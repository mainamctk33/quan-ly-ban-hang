﻿using App.Database.Pay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class Debit
    {
        public Debit()
        {

        }
        private tb_Provider provider;

        public double Payment { get; set; }
        public string Code { get; set; }
        public int BillId { get; set; }

        public DateTime CreatedDate;

        public string UserFullName { get; set; }

        public string Note { get; set; }

        public int ID { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public bool isFamiliar { get; set; }
        public int Quantity { get; set; }
        public double Money { get; set; }
        public String MoneyString
        {
            get
            {
                return Money.ToString("N0")+" đ";
            }
        }
        public String PaymentString
        {
            get
            {
                return Payment.ToString("N0") + " đ";
            }
        }
        public String CreatedDateString
        {
            get
            {
                if (this.CreatedDate != null)
                    return CreatedDate.ToString("dd/MM/yyyyy");
                return "";
            }
        }
        public Debit(tb_Customer customer, double money)
        {
            this.Money = money;
            this.ID = customer.ID;
            this.FullName = customer.FullName;
            this.Phone = customer.Phone;
            this.Address = customer.Address;
            this.isFamiliar = customer.isFamiliar;
        }

        public Debit(tb_Pay pay)
        {
            this.Money = pay.Value;
            this.Payment = pay.Value;
            this.CreatedDate = pay.CreatedDate;
            this.UserFullName = pay.tb_User.FullName;
            this.Note = pay.Note;
            this.ID = pay.ID;
        }

        public Debit(tb_BillItem billItem)
        {
            this.Quantity = billItem.Quantily;
            this.Money = billItem.Price;
            this.Payment = billItem.Sum;
            this.Code = billItem.ProductId;
            this.Name = billItem.ProductName;
            this.ID = billItem.ID;
        }

        public Debit(tb_Provider provider, double money)
        {
            this.ID = provider.ID;
            this.Name = provider.Name;
            this.Phone = provider.Phone;
            this.Address = provider.Address;
            this.Money = money;
        }

        public Debit(int BillId, tb_Customer customer, double money, double paymented, DateTime createdDate, tb_User tb_User, string note) : this(customer, money)
        {
            this.Payment = paymented;
            this.BillId = BillId;
            this.CreatedDate = createdDate;
            this.UserFullName = tb_User.FullName;
            this.Note = note;
        }

        public Debit(int BillId, tb_Provider provider, double money, double paymented, DateTime createdDate, tb_User tb_User, string note) : this(provider, money)
        {
            this.Payment = paymented;
            this.BillId = BillId;
            this.CreatedDate = createdDate;
            this.UserFullName = tb_User.FullName;
            this.Note = note;
        }
    }
}
