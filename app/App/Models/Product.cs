﻿using App.Database.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class Product
    {
        public Product()
        {

        }
        public Product(tb_Product x)
        {
            ProductCode = x.ProductCode;
            Name = x.Name;
            NewPrice = x.NewPrice;
            NewPriceFromDate = x.NewPriceFromDate;
            NewPriceToDate = x.NewPriceToDate;
            Price = x.Price;
            ProductTypeId = x.ProductTypeId;
            ApplyNewPrice = x.ApplyNewPrice;
            ProviderId = x.ProviderId;
            QuantityInStock = x.QuantityInStock;
            UpdatedDate = x.UpdatedDate;
            ProductTypeName = x.tb_ProductType.Name;
            ProviderName = x.tb_Provider.Name;
            CreatedBy = x.tb_User.FullName;
            Active = x.Active;
            Description = x.Description;
            NewPriceString = this.ApplyNewPrice ? this.NewPrice.HasValue ? (this.NewPrice.Value.ToString("N0") + " đ") : "" : "";
            FromDate = this.ApplyNewPrice ? NewPriceFromDate.HasValue ? NewPriceFromDate.Value.ToString("dd/MM/yyyy") : "" : "";
            ToDate = this.ApplyNewPrice ? NewPriceToDate.HasValue ? NewPriceToDate.Value.ToString("dd/MM/yyyy") : "" : "";
            ActualPrice = GetActualPrice(x);
        }

        private double GetActualPrice(tb_Product x)
        {
            if (!x.ApplyNewPrice)
                return x.Price;
            if (!x.NewPrice.HasValue)
                return x.Price;
            if (x.NewPriceToDate.HasValue)
            {
                DateTime date = x.NewPriceToDate.Value;
                if ((DateTime.Now.Date - date.Date).TotalDays > 0)
                    return x.Price;
            }
            if (x.NewPriceFromDate.HasValue)
            {
                DateTime date = x.NewPriceFromDate.Value;
                if ((DateTime.Now.Date - date.Date).TotalDays < 0)
                    return x.Price;
                return x.NewPrice.Value;
            }
            return x.Price;
        }

        public String ProductCode { get; set; }
        public string Name { get; set; }
        public double? NewPrice { get; set; }
        public DateTime? NewPriceFromDate { get; set; }
        public String NewPriceString { get; set; }
        public String FromDate { get; set; }
        public String ToDate { get; set; }
        public double ActualPrice { get; set; }
        public DateTime? NewPriceToDate { get; set; }
        public double Price { get; set; }
        public String Price15String
        {
            get
            {
                return (Price * 115 / 100).ToString("N0") + " đ";
            }
        }
        public String Price20String
        {
            get
            {
                return (Price * 120 / 100).ToString("N0") + " đ";
            }
        }
        public String Price30String
        {
            get
            {
                return (Price * 130 / 100).ToString("N0") + " đ";
            }
        }
        public String Price40String
        {
            get
            {
                return (Price * 140 / 100).ToString("N0") + " đ";
            }
        }
        public int ProductTypeId { get; set; }
        public bool ApplyNewPrice { get; set; }
        public int ProviderId { get; set; }
        public int QuantityInStock { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ProductTypeName { get; set; }
        public string ProviderName { get; set; }
        public string CreatedBy { get; set; }
        public bool Active { get; set; }
        public string Description { get; private set; }

        public String PriceString
        {
            get
            {
                return Price.ToString("N0") + " đ";
            }
        }

        public String ActualPriceString
        {
            get
            {
                return ActualPrice.ToString("N0") + " đ";
            }
        }
        public double Sum
        {
            get
            {
                return ActualPrice * QuantityInStock;
            }
        }
        public String SumString
        {
            get
            {
                return (ActualPrice * QuantityInStock).ToString("N0") + " đ";
            }
        }
    }
}
