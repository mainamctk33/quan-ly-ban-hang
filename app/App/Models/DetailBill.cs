﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Database.Pay;

namespace App.Models
{
    public class DetailBill
    {
        public tb_Bill tb_Bill;
        public tb_Customer tb_Customer;
        public tb_Provider tb_Provider;
        public List<Debit> listPayments;
        public List<Debit> listProducts;
        public DetailBill()
        {

        }
        public DetailBill(Database.Pay.tb_Customer tb_Customer, Database.Pay.tb_Provider tb_Provider, tb_Bill result, List<Debit> listPayments,List<Debit> listProducts)
        {
            this.tb_Bill = result;
            this.tb_Customer = tb_Customer;
            this.tb_Provider = tb_Provider;
            this.listPayments = listPayments;
            this.listProducts = listProducts;
        }
    }
}
