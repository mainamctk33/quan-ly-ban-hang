﻿using App.DataAccess;
using App.Database.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class User
    {
        public User(tb_User user)
        {
            Active = user.Active;
            FullName = user.FullName;
            UserName = user.UserName;
            CreatedDateString = user.CreatedDate.ToString("dd/MM/yyyy");
            Role = user.Role;
            ACCOUNT = RoleInfo.HasPermission(Role, RoleInfo.Role.mgrAccount);
            CUSTOMER = RoleInfo.HasPermission(Role, RoleInfo.Role.mgrCustomer);
            PRODUCT = RoleInfo.HasPermission(Role, RoleInfo.Role.mgrProduct);
            PRODUCT_TYPE = RoleInfo.HasPermission(Role, RoleInfo.Role.mgrProductType);
            PROVIDER = RoleInfo.HasPermission(Role, RoleInfo.Role.mgrProvider);
            SALE = RoleInfo.HasPermission(Role, RoleInfo.Role.allowSale);

        }

        public bool Active { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string CreatedDateString { get; set; }
        public int Role { get; set; }

        public bool ACCOUNT { get; set; }

        public bool CUSTOMER { get; set; }
        public bool PRODUCT { get; set; }
        public bool PRODUCT_TYPE { get; set; }
        public bool PROVIDER { get; set; }
        public bool SALE { get; set; }
    }
}
