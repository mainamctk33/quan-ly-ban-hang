﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    public delegate T delExecute<T>(params Object[] a);
    public delegate ConstructorInfo delGetContructor();
    public delegate MethodInfo delGetMethod();
    public delegate T delExecute3<T>();
    public delegate void delExecute2();
    public delegate void delEndExecute<T>(T t, Form ctrol);
    public partial class frmLoading : DevComponents.DotNetBar.Office2007Form
    {
        private static frmLoading _frmLoading;
        Boolean isClose = false;

        public bool IsShow { get; private set; }

        public frmLoading()
        {
            InitializeComponent();
        }
        public void ShowFrorm()
        {
            this.IsShow = true;
            this.ShowDialog();
        }
        public static void Run<T>(delExecute<T> delExecute, delEndExecute<T> delEndExecute, Form ctrl, params Object[] param) where T : new()
        {
            frmLoading xl = null;
            if (ctrl != null)
                xl = new frmLoading();
            new Thread(() =>
            {
                test2(xl, ctrl, delExecute, delEndExecute, param);
            }).Start();
            if (ctrl != null)
                xl.Show();
        }

        //public static void RunMethod<T>(Func<T> action, delEndExecute<T> delEndExecute, Form ctrl, params object[] param) where T : new()
        //{
        //    frmLoading xl = null;
        //    if (ctrl != null)
        //        xl = new frmLoading();
        //    new Thread(() =>
        //    {
        //        test3(xl, ctrl, () =>
        //        {
        //            return action.Method;
        //        }, delEndExecute, param);
        //    }).Start();
        //    if (ctrl != null)
        //        xl.Show();
        //}
        public static void RunMethod<T>(Action action, delEndExecute<T> delEndExecute, Form ctrl, params object[] param) where T : new()
        {
            frmLoading xl = null;
            if (ctrl != null)
                xl = new frmLoading();
            new Thread(() =>
            {
                test3(xl, ctrl, () =>
                {
                    return action.Method;
                }, delEndExecute, param);
            }).Start();
            if (ctrl != null)
                xl.Show();
        }

        public static void RunMethod<T>(Type magicType, String methodName, delEndExecute<T> delEndExecute, Form ctrl, bool showLoading, params object[] param) where T : new()
        {
            frmLoading xl = null;
            if (showLoading && ctrl != null)
                xl = new frmLoading();

            new Thread(() =>
            {
                test3(xl, ctrl, () =>
                {
                    return magicType.GetConstructor(Type.EmptyTypes);
                }, () =>
                {
                    return magicType.GetMethod(methodName);
                }, delEndExecute, param);
            }).Start();
            if (ctrl != null && xl != null)
                xl.Show();
        }
        public static void RunMethod<T>(Type magicType, String methodName, delEndExecute<T> delEndExecute, Form ctrl, params object[] param) where T : new()
        {
            RunMethod<T>(magicType, methodName, delEndExecute, ctrl, true, param);
        }
        public static void RunMethod<T>(String className, String methodName, delEndExecute<T> delEndExecute, Form ctrl, params object[] param) where T : new()
        {
            frmLoading xl = null;
            if (ctrl != null)
                xl = new frmLoading();
            new Thread(() =>
            {
                Type magicType = Type.GetType(className);
                test3(xl, ctrl, () =>
                {
                    return magicType.GetConstructor(Type.EmptyTypes);
                }, () =>
                {
                    return magicType.GetMethod(methodName);
                }, delEndExecute, param);
            }).Start();
            if (ctrl != null)
                xl.Show();
        }

        public static void RunMethod<T>(delGetContructor getContructor, delGetMethod getMethod, delEndExecute<T> delEndExecute, Form ctrl, params object[] param) where T : new()
        {
            frmLoading xl = null;
            if (ctrl != null)
                xl = new frmLoading();
            new Thread(() =>
            {

                test3(xl, ctrl, getContructor, getMethod, delEndExecute, param);
            }).Start();
            if (ctrl != null)
                xl.Show();
        }

        public static void Run<T>(delExecute3<T> delExecute, delEndExecute<T> delEndExecute, Form ctrl) where T : new()
        {
            frmLoading xl = null;
            if (ctrl != null)
                xl = new frmLoading();
            new Thread(() =>
            {
                test2(xl, ctrl, delExecute, delEndExecute);
            }).Start();
            if (ctrl != null)
                xl.Show();
        }
        private static void test3<T>(frmLoading xl, Form ctrl, delGetMethod method, delEndExecute<T> delEndExecute, params object[] param) where T : new()
        {
            Thread.Sleep(1000);
            //object magicClassObject = contructor().Invoke(new object[] { });
            var value = method().Invoke(null, param);
            try
            {
                ctrl.Invoke(new EventHandler(delegate
                {
                    try
                    {
                        delEndExecute((T)value, ctrl);
                        xl.isClose = true;
                        xl.Close();
                    }
                    catch (Exception)
                    {

                    }
                }));
            }
            catch (Exception e)
            {
            }
        }
        private static void test3<T>(frmLoading xl, Form ctrl, delGetContructor contructor, delGetMethod method, delEndExecute<T> delEndExecute, params object[] param) where T : new()
        {
            Thread.Sleep(1000);
            //object magicClassObject = contructor().Invoke(new object[] { });
            var value = method().Invoke(null, param);
            try
            {
                ctrl.Invoke(new EventHandler(delegate
                {
                    try
                    {
                        delEndExecute((T)value, ctrl);
                        if (xl != null)
                        {
                            xl.isClose = true;
                            xl.Close();
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }));
            }
            catch (Exception e)
            {
            }
        }
        private static void test2<T>(frmLoading xl, Form ctrl, delExecute<T> delExecute, delEndExecute<T> delEndExecute, params Object[] param) where T : new()
        {
            Thread.Sleep(1000);
            var value = delExecute(param);
            try
            {
                ctrl.Invoke(new EventHandler(delegate
                {
                    try
                    {
                        delEndExecute(value, ctrl);
                        xl.isClose = true;
                        xl.Close();
                    }
                    catch (Exception)
                    {

                    }
                }));
            }
            catch (Exception e)
            {
            }
        }
        private static void test2<T>(frmLoading xl, Form ctrl, delExecute3<T> delExecute, delEndExecute<T> delEndExecute) where T : new()
        {
            Thread.Sleep(1000);
            var value = delExecute();
            try
            {
                ctrl.Invoke(new EventHandler(delegate
                {
                    try
                    {
                        delEndExecute(value, ctrl);
                        xl.isClose = true;
                        xl.Close();
                    }
                    catch (Exception)
                    {

                    }
                }));
            }
            catch (Exception e)
            {
            }
        }

        static void test(frmLoading xl, Form ctrl, delExecute2 delExecute)
        {
            Thread.Sleep(1000);
            delExecute();
            try
            {
                ctrl.Invoke(new EventHandler(delegate
                {
                    try
                    {
                        xl.isClose = true;
                        xl.Close();
                    }
                    catch (Exception)
                    {

                    }
                }));
            }
            catch (Exception e)
            {
            }
        }
        public static void Run(delExecute2 delExecute, Form ctrl)
        {
            frmLoading xl = null;
            if (ctrl != null)
                xl = new frmLoading();
            new Thread(() =>
            {
                test(xl, ctrl, delExecute);
            }).Start();
            if (ctrl != null)
                xl.Show();
        }

        private static void showProgress()
        {
            try
            {
            }
            catch (ThreadAbortException)
            {
                // possibly do something here
                Thread.ResetAbort();
            }
            catch (Exception)
            {

            }
        }

        private void frmLoading_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isClose)
                e.Cancel = true;
        }

        private void frmLoading_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsShow = false;
        }
    }
}
