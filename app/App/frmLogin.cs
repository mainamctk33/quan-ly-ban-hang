﻿using App.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Common;
using App.Database.User;
using App.Utils;

namespace App
{
    public delegate void delLogin(object obj);
    public partial class frmLogin : DevComponents.DotNetBar.Office2007Form
    {
        private delLogin _delLogin;

        public frmLogin()
        {
            InitializeComponent();
            FormUtils.ConfigDialog(this);
        }

        public frmLogin(delLogin _delLogin)
        {
            this._delLogin = _delLogin;
            InitializeComponent();
            FormUtils.ConfigDialog(this);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            String username = txtUserName.Text;
            String password = txtPassword.Text;
            if (String.IsNullOrWhiteSpace(username))
            {
                frmDialog.ShowOkDialog("Thông báo", "Vui lòng nhập tên đăng nhập", this);
                return;
            }
            if (String.IsNullOrWhiteSpace(password))
            {
                frmDialog.ShowOkDialog("Thông báo", "Vui lòng nhập mật khẩu", this);
                return;
            }
            frmLoading.Run(() =>
              {
                  return new UserInfo().Login(username, password);
              }, (result, ctrl) =>
              {
                  if (result.IsTrue)
                  {
                      this.Close();
                      _delLogin(result.Data);
                  }
                  else
                  {
                      frmDialog.ShowOkDialog(result.Message, this);
                  }
              }, this);
        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin.PerformClick();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin.PerformClick();
            }
        }
    }
}
