﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common
{
    class Constants
    {
        public static string SERVER = "SERVER";
        public static string DATABASE = "DATABASE";
        public static string PASSWORD = "PASSWORD";

        public static string USERNAME = "USERNAME";
    }
}
