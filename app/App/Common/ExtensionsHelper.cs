﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Dynamic;
using App.Utils;

namespace App.Common
{
    public static class ExtensionsHelper
    {
      
        //Function get unix time stamp for date
        public static long GetUnixTimeStamp(this DateTime dateTime)
        {
            return (long)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        //Function convert unix time stamp to date
        public static DateTime UnixTimeStampToDateTime(this double timeMilisecond)
        {
            return ConvertUtils.TimeMilisecondToDateTime(timeMilisecond);
        }

        //Function get time post status
        public static StringBuilder GetTimePostStatus(this double timeMilisecond)
        {
            //Define date time
            long date10Day = 86400 * 10;
            long dateOneDay = 86400;
            long dateOneHour = 3600;
            long dateOneMinute = 60;
            string dateDayName = " ngày";
            string dateHourName = " giờ";
            string dateMinuteName = " phút";
            string dateHourAgoName = " trước";
            string dateJustNowName = " vừa xong";

            StringBuilder time = new StringBuilder();
            try
            {
                var temp = App.Utils.ConvertUtils.TimeMilisecondToDateTime(timeMilisecond);

                double totalTimeDiff = (DateTime.Now - temp).TotalSeconds;

                if (totalTimeDiff > date10Day)
                {
                    time.Append(temp.ToString("dd-MM-yyyy HH:mm:ss"));
                }
                else
                {
                    if (totalTimeDiff > dateOneDay)
                    {
                        int day = (int)(totalTimeDiff / dateOneDay);
                        time.Append(day).Append(" ").Append(dateDayName).Append(dateHourAgoName);
                    }
                    else if (totalTimeDiff > dateOneHour)
                    {
                        int hour = (int)(totalTimeDiff / dateOneHour);
                        time.Append(hour).Append(" ").Append(dateHourName).Append(dateHourAgoName);
                    }
                    else if (totalTimeDiff > dateOneMinute)
                    {
                        int minute = (int)(totalTimeDiff / dateOneMinute);
                        time.Append(minute).Append(" ").Append(dateMinuteName).Append(dateHourAgoName);
                    }
                    else
                        time.Append(dateJustNowName);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return time;
        }

        //Function generate string to md5
        public static String GenerateStringToMD5(this string value)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(value);
            MemoryStream stream = new MemoryStream(byteArray);
            string hexString = String.Empty;
            using (var md5 = MD5.Create())
            {
                hexString = ToHex(md5.ComputeHash(stream), false);
            }
            return hexString;
        }

        private static string ToHex(byte[] bytes, bool upperCase)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

            return result.ToString();
        }
        //End Function generate string to md5

        //Function convert string to unsign
        public static string ConvertToUnSign(this string value)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = value.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        
        #region price format
        public static String PriceFormat(this long val)
        {
            return String.Format("{0:N0}", val).Replace(".00", "").Replace(",", ".");
        }
        public static String PriceFormat(this double val)
        {
            return String.Format("{0:N0}", val).Replace(".00", "").Replace(",", ".");
        }
        public static String PriceFormat(this float val)
        {
            return String.Format("{0:N0}", val).Replace(".00", "").Replace(",", ".");
        }

        public static String PriceFormat(this int val)
        {
            return String.Format("{0:N0}", val).Replace(".00", "").Replace(",", ".");
        }
        #endregion        
    }
}