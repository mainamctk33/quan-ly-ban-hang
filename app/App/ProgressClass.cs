﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    public class ProgressClass
    {
        static void funShowProgress()
        {
            try
            {
                frmLoading xl = new frmLoading();
                xl.ShowDialog();
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {

            }
        }
        static Thread thrProgress;
        public static Thread ShowProgress()
        {
            try
            {
                Thread thrProgress2 = new Thread(funShowProgress);
                thrProgress2.Start();
                return thrProgress2;
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {
            }
            return null;
        }

        public static Thread ShowProgress(Form frm)
        {
            try
            {
                Thread thrProgress2 = new Thread(funShowProgress);
                thrProgress2.Start();
                frm.Visible = false;
                return thrProgress2;
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {
            }
            return null;
        }
        public static Thread ShowProgress(Control ctr)
        {
            try
            {
                Thread thrProgress2 = new Thread(funShowProgress);
                thrProgress2.Start();
                ctr.Visible = false;
                return thrProgress2;
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {
            }
            return null;
        }
        public static void HideProgress(Thread thrProgress2)
        {
            try
            {
                thrProgress2.Abort();
                thrProgress2.Join();
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {

            }
        }
        public static void HideProgress(Thread thrProgress2, Form frm)
        {
            try
            {
                frm.Visible = true;
                thrProgress2.Abort();
                thrProgress2.Join();
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {

            }
        }
        public static void HideProgress(Thread thrProgress2, Control ctr)
        {
            try
            {
                ctr.Visible = true;
                thrProgress2.Abort();
                thrProgress.Join();
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {

            }
        }

        public delegate void Show(ToolStripStatusLabel lb, string Text);
        public delegate void Hide(ToolStripStatusLabel lb);
        static ToolStripStatusLabel lb;
        static Form frm2;
        internal static ToolStripStatusLabel toolStripStatusLabel;

        static void ShowText(string Text)
        {
            try
            {
                if (toolStripStatusLabel != null)
                {
                    toolStripStatusLabel.Text = Text;
                    toolStripStatusLabel.Visible = true;
                }
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {

            }
        }
        static void ShowText(ToolStripStatusLabel lbStatus, string Text)
        {
            try
            {
                lbStatus.Text = Text;
                lbStatus.Visible = true;
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {

            }
        }
        static void HideText(ToolStripStatusLabel lbStatus)
        {
            try
            {
                lbStatus.Visible = false;

            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {

                throw;
            }
        }

        static void HideStatus()
        {
            try
            {
                Thread.Sleep(2000);
                frm2.Invoke(new Hide(HideText), lb);
                thrProgress.Abort();
                thrProgress.Join();
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception)
            {
            }
        }
        public static void ShowStatus(Form frm, string Text)
        {
            try
            {
                if (toolStripStatusLabel != null)
                {
                    lb = toolStripStatusLabel;
                    frm2 = frm;
                    frm.Invoke(new Show(ShowText), new object[] { toolStripStatusLabel, Text });
                    thrProgress = new Thread(HideStatus);
                    thrProgress.Start();
                }
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception e)
            {
            }
        }
        public static void ShowStatus(Form frm, ToolStripStatusLabel lbStatus, string Text)
        {
            try
            {
                lb = lbStatus;
                frm2 = frm;
                frm.Invoke(new Show(ShowText), new object[] { lbStatus, Text });
                thrProgress = new Thread(HideStatus);
                thrProgress.Start();
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread - caught ThreadAbortException - resetting.");
                Console.WriteLine("Exception message: {0}", e.Message);
                Thread.ResetAbort();
            }
            catch (Exception e)
            {
            }
        }
    }
}
