﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Database.Product;
using App.Models;

namespace App.DataAccess
{
    public class ProductInfo
    {

        public static BaseReturnFunction<List<Product>> SearchByProvider(int providerId, String code, int page, int size)
        {
            try
            {
                using (var context = new ProductDbDataContext())
                {
                    IQueryable<tb_Product> value = context.tb_Products.Where(x=>x.ProviderId==providerId && x.Active);
                    if (!String.IsNullOrEmpty(code))
                    {
                        code = code.ToLower().Trim();
                        value = context.tb_Products.Where(x => x.ProductCode.ToLower().Contains(code) || x.Name.ToLower().Contains(code));
                    }
                    var count = value.Count();
                    var list2 = value.Skip((page - 1) * size).Take(size).ToList();

                    List<Product> list = list2.Select(x => new Product(x)).ToList();

                    return new BaseReturnFunction<List<Product>>(StatusFunction.TRUE, list, count);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<Product>>(StatusFunction.TRUE, new List<Product>());
            }
        }

        public static BaseReturnFunction<List<Product>> Search2(String code,int page, int size)
        {
            try
            {
                using (var context = new ProductDbDataContext())
                {
                    IQueryable<tb_Product> value;
                    if (!String.IsNullOrEmpty(code))
                    {
                        code = code.ToLower().Trim();
                        value = context.tb_Products.Where(x =>x.Active && x.QuantityInStock!=0&&  (x.ProductCode.ToLower().Contains(code)) || x.Name.ToLower().Contains(code));
                    }
                    else
                    {
                        value = context.tb_Products;
                    }
                    var count = value.Count();
                    var list2 = value.Skip((page - 1) * size).Take(size).ToList();

                    List<Product> list = list2.Select(x => new Product(x)).ToList();

                    return new BaseReturnFunction<List<Product>>(StatusFunction.TRUE, list, count);
                }
            }
            catch (Exception)
             {
                return new BaseReturnFunction<List<Product>>(StatusFunction.TRUE, new List<Product>());
            }
        }

        public static BaseReturnFunction<List<Product>> Search(String code, String name, int page, int size)
        {
            try
            {
                using (var context = new ProductDbDataContext())
                {
                    IQueryable<tb_Product> value;
                    if (name == null)
                        name = "";
                    if (!String.IsNullOrEmpty(name) || !String.IsNullOrEmpty(code))
                    {
                        code = code.ToLower().Trim();
                        name = name.ToLower().Trim();
                        value = context.tb_Products.Where(x => (code == "" || x.ProductCode.ToLower().Contains(code)) && (name == "" || x.Name.ToLower().Contains(name)));
                    }
                    else
                    {
                        value = context.tb_Products;
                    }
                    var count = value.Count();
                    var list2 = value.Skip((page - 1) * size).Take(size).ToList();

                    List<Product> list = list2.Select(x => new Product(x)).ToList();

                    return new BaseReturnFunction<List<Product>>(StatusFunction.TRUE, list, count);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<Product>>(StatusFunction.TRUE, new List<Product>());
            }
        }
        public static BaseReturnFunction<tb_Product> GetById(ProductDbDataContext context, String id)
        {
            try
            {
                var customer = context.tb_Products.SingleOrDefault(x => x.ProductCode == id);
                if (customer != null)
                    return new BaseReturnFunction<tb_Product>(StatusFunction.TRUE, customer);
                return new BaseReturnFunction<tb_Product>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Product>(StatusFunction.EXCEPTION, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
        }
        public static BaseReturnFunction<bool> Delete(String productCode)
        {
            try
            {
                using (var context = new ProductDbDataContext())
                {
                    var temp = GetById(context, productCode);
                    if (temp.IsTrue)
                    {
                        context.tb_Products.DeleteOnSubmit(temp.Data);
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn nhập");

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Xóa sản phẩm không thành công");
            }

        }

        public static BaseReturnFunction<tb_Product> GetByName(ProductDbDataContext context, String name)
        {
            try
            {
                name = name.Trim().ToLower();
                var product = context.tb_Products.SingleOrDefault(x => x.Name.ToLower() == name);
                if (product != null)
                    return new BaseReturnFunction<tb_Product>(StatusFunction.TRUE, product);
                return new BaseReturnFunction<tb_Product>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Product>(StatusFunction.EXCEPTION, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
        }
        public static BaseReturnFunction<App.Models.Product> Create(String productCode, String username, String name, int productTypeId, int providerId, int quantityInStock, double price, double newPrice, DateTime? applyFrom, DateTime? applyTo, bool applyNewPrice, String description, bool active)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<App.Models.Product>(StatusFunction.FALSE, "Vui lòng nhập tên sản phẩm");
                using (var context = new ProductDbDataContext())
                {
                    var temp = GetByName(context, name);
                    if (temp.IsTrue)
                        return new BaseReturnFunction<App.Models.Product>(StatusFunction.FALSE, "Tồn tại sản phẩm với tên bạn nhập");
                    var product = new tb_Product()
                    {
                        ProductCode = productCode,
                        CreatedBy = username,
                        CreatedDate = DateTime.Now,
                        Name = name,
                        Price = price,
                        ApplyNewPrice = applyNewPrice,
                        NewPriceFromDate = applyFrom,
                        NewPriceToDate = applyTo,
                        NewPrice = newPrice,
                        ProductTypeId = productTypeId,
                        ProviderId = providerId,
                        QuantityInStock = quantityInStock,
                        UpdatedDate = DateTime.Now,
                        Active = active,
                        Description = description
                    };
                    context.tb_Products.InsertOnSubmit(product);
                    context.SubmitChanges();
                    return new BaseReturnFunction<App.Models.Product>(StatusFunction.TRUE, new App.Models.Product(product));
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<App.Models.Product>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static BaseReturnFunction<App.Models.Product> Update(String productCode, String username, String name, int productTypeId, int providerId, int quantityInStock, double price, double newPrice, DateTime? applyFrom, DateTime? applyTo, bool applyNewPrice, String description, bool active)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<App.Models.Product>(StatusFunction.FALSE, "Vui lòng nhập tên sản phẩm");
                using (var context = new ProductDbDataContext())
                {
                    var temp = GetById(context, productCode);
                    if (temp.IsTrue)
                    {
                        if (name.Trim().ToLower() != temp.Data.Name.Trim().ToLower())
                        {
                            var temp2 = GetByName(context, name);
                            if (temp2.IsTrue)
                            {
                                return new BaseReturnFunction<Models.Product>(StatusFunction.FALSE, "Tồn tại sản phẩm với với thông tin bạn nhập");
                            }
                        }
                        var customer = temp.Data;
                        customer.CreatedBy = username;
                        customer.CreatedDate = DateTime.Now;
                        customer.Name = name;
                        customer.Price = price;
                        customer.ApplyNewPrice = applyNewPrice;
                        customer.NewPriceFromDate = applyFrom;
                        customer.NewPriceToDate = applyTo;
                        customer.NewPrice = newPrice;
                        customer.ProductTypeId = productTypeId;
                        customer.ProviderId = providerId;
                        customer.QuantityInStock = quantityInStock;
                        customer.UpdatedDate = DateTime.Now;
                        customer.Active = active;
                        customer.UpdatedDate = DateTime.Now;
                        customer.Description = description;
                        context.SubmitChanges();
                        return new BaseReturnFunction<Models.Product>(StatusFunction.TRUE, new Models.Product(customer));
                    }
                    return new BaseReturnFunction<Models.Product>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Models.Product>(StatusFunction.EXCEPTION, e.Message);
            }
        }

    }
}
