﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Database.Bill;

namespace App.DataAccess
{
    public class BillInfo
    {
        public static BaseReturnFunction<List<tb_Bill>> GetAll()
        {
            try
            {
                using (var context = new BillDbDataContext())
                {
                    return new BaseReturnFunction<List<tb_Bill>>(StatusFunction.TRUE, context.tb_Bills.ToList());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Bill>>(StatusFunction.TRUE, new List<tb_Bill>());
            }
        }
        public static BaseReturnFunction<List<tb_Bill>> Search(int page, int size)
        {
            try
            {
                using (var context = new BillDbDataContext())
                {
                    IQueryable<tb_Bill> value = context.tb_Bills.Skip((page - 1) * size).Take(size);

                    return new BaseReturnFunction<List<tb_Bill>>(StatusFunction.TRUE, value.ToList());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Bill>>(StatusFunction.TRUE, new List<tb_Bill>());
            }
        }
        public static BaseReturnFunction<tb_Bill> GetById(BillDbDataContext context, int id)
        {
            try
            {
                var customer = context.tb_Bills.SingleOrDefault(x => x.ID == id);
                if (customer != null)
                    return new BaseReturnFunction<tb_Bill>(StatusFunction.TRUE, customer);
                return new BaseReturnFunction<tb_Bill>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Bill>(StatusFunction.EXCEPTION, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
        }
        public static BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new BillDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        context.tb_Bills.DeleteOnSubmit(temp.Data);
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn nhập");

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Xóa khách hàng không thành công");
            }

        }

        public static BaseReturnFunction<tb_Bill> Create(Nullable<int>providerId, Nullable<int>customerId, decimal price, String createdBy, String note)
        {
            try
            {
                using (var context = new BillDbDataContext())
                {
                    var bill = new tb_Bill()
                    {
                        CreatedBy=createdBy,
                        CreatedDate=DateTime.Now,
                        CustomerId=customerId,
                        Note=note,
                        Price=price,
                        ProviderId=providerId,
                        UpdatedDate=DateTime.Now
                    };
                    context.tb_Bills.InsertOnSubmit(bill);
                    context.SubmitChanges();
                    return new BaseReturnFunction<tb_Bill>(StatusFunction.TRUE, bill);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Bill>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static BaseReturnFunction<tb_Bill> Update(int id, Nullable<int> providerId, Nullable<int> customerId, decimal price, String note)
        {
            try
            {
                using (var context = new BillDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        var bill = temp.Data;
                        bill.Price = price;
                        bill.Note = note;
                        bill.ProviderId= providerId;
                        bill.CustomerId = customerId;
                        bill.UpdatedDate = DateTime.Now;
                        context.SubmitChanges();
                        return new BaseReturnFunction<tb_Bill>(StatusFunction.TRUE, bill);
                    }
                    return new BaseReturnFunction<tb_Bill>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Bill>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

    }
}