﻿using App.Database.Pay;
using App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;

namespace App.DataAccess
{
    public class DebtInfo
    {
        public static BaseReturnFunction<List<Debit>> Search(String keyword, int page, int size)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    IQueryable<tb_Bill> result;
                    if (String.IsNullOrWhiteSpace(keyword))
                    {
                        result = context.tb_Bills.Where(x => x.CustomerId.HasValue);
                    }
                    else
                    {
                        keyword = keyword.ToLower().Trim();
                        result = context.tb_Bills.Where(x => x.CustomerId.HasValue && (x.tb_Customer.FullName.ToLower().Contains(keyword) || x.tb_Customer.Phone.ToLower().Contains(keyword) || x.tb_Customer.Address.ToLower().Contains(keyword)));
                    }
                    var result2 = result.GroupBy(x => x.CustomerId).Select(x => new { Customer = x.First().tb_Customer, mua = x.Sum(y => y.Price), tra = x.SelectMany(y => y.tb_Pays).Count() > 0 ? x.SelectMany(y => y.tb_Pays).Select(t => t.Value).Sum() : 0 }).ToList();
                    var result3 = result2.Select(x => new Debit(x.Customer, x.mua - x.tra)).Where(x => x.Money != 0).OrderBy(x => x.Money);
                    int total = result3.Count();
                    return new BaseReturnFunction<List<Debit>>(StatusFunction.TRUE, result3.Skip((page - 1) * size).Take(size).ToList(), total);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Debit>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public static BaseReturnFunction<List<Debit>> SearchProvider(String keyword, int page, int size)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    IQueryable<tb_Bill> result;
                    if (String.IsNullOrWhiteSpace(keyword))
                    {
                        result = context.tb_Bills.Where(x => x.ProviderId.HasValue);
                    }
                    else
                    {
                        keyword = keyword.ToLower().Trim();
                        result = context.tb_Bills.Where(x => x.ProviderId.HasValue && (x.tb_Provider.Name.ToLower().Contains(keyword) || x.tb_Provider.Phone.ToLower().Contains(keyword) || x.tb_Provider.Address.ToLower().Contains(keyword)));
                    }
                    var result2 = result.GroupBy(x => x.ProviderId).Select(x => new { Provider = x.First().tb_Provider, mua = x.Sum(y => y.Price), tra = x.SelectMany(y => y.tb_Pays).Count() > 0 ? x.SelectMany(y => y.tb_Pays).Select(t => t.Value).Sum() : 0 }).ToList();
                    var result3 = result2.Select(x => new Debit(x.Provider, x.mua - x.tra)).Where(x => x.Money != 0).OrderBy(x => x.Money);
                    int total = result3.Count();
                    return new BaseReturnFunction<List<Debit>>(StatusFunction.TRUE, result3.Skip((page - 1) * size).Take(size).ToList(), total);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Debit>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public static BaseReturnFunction<Boolean> CustomerPaymentAll(String userName, int customerId, String note)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    IQueryable<tb_Bill> result;

                    result = context.tb_Bills.Where(x => x.CustomerId.HasValue && x.tb_Customer.ID == customerId);
                    foreach (var item in result)
                    {
                        var no = item.Price - item.tb_Pays.Sum(x => x.Value);
                        if (no != 0)
                        {
                            context.tb_Pays.InsertOnSubmit(new tb_Pay()
                            {
                                BillId = item.ID,
                                Note = note,
                                Value = no,
                                CreatedBy = userName,
                                CreatedDate = DateTime.Now
                            });
                        }
                    }
                    context.SubmitChanges();
                    return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public static BaseReturnFunction<Boolean> ProviderPaymentAll(String userName, int providerId, String note)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    IQueryable<tb_Bill> result;

                    result = context.tb_Bills.Where(x => x.ProviderId.HasValue && x.tb_Provider.ID == providerId);
                    foreach (var item in result)
                    {
                        var no = item.Price - item.tb_Pays.Sum(x => x.Value);
                        if (no != 0)
                        {
                            context.tb_Pays.InsertOnSubmit(new tb_Pay()
                            {
                                BillId = item.ID,
                                Note = note,
                                Value = no,
                                CreatedBy = userName,
                                CreatedDate = DateTime.Now
                            });
                        }
                    }
                    context.SubmitChanges();
                    return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public static BaseReturnFunction<Boolean> PaymentBill(String userName, int billId, String note)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    tb_Bill result = context.tb_Bills.SingleOrDefault(x => x.ID == billId);
                    if (result != null)
                    {
                        var no = result.Price - result.tb_Pays.Sum(x => x.Value);
                        if (no != 0)
                        {
                            context.tb_Pays.InsertOnSubmit(new tb_Pay()
                            {
                                BillId = billId,
                                Note = note,
                                Value = no,
                                CreatedBy = userName,
                                CreatedDate = DateTime.Now
                            });
                        }
                    }
                    context.SubmitChanges();
                    return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public static BaseReturnFunction<Debit> AddPayment(String userName, int billId, double value, String note)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    tb_Bill result = context.tb_Bills.SingleOrDefault(x => x.ID == billId);
                    if (result != null)
                    {
                        var no = result.Price - result.tb_Pays.Sum(x => x.Value);
                        if (no != 0 && value <= no)
                        {
                            var pay = new tb_Pay()
                            {
                                BillId = billId,
                                Note = note,
                                Value = value,
                                CreatedBy = userName,
                                CreatedDate = DateTime.Now
                            };
                            context.tb_Pays.InsertOnSubmit(pay);
                            context.SubmitChanges();
                            var debit = new Debit(pay);
                            return new BaseReturnFunction<Debit>(StatusFunction.TRUE, debit);
                        }
                        else
                        {
                            return new BaseReturnFunction<Debit>(StatusFunction.FALSE, "Số tiền thanh toán lớn hơn số tiền nợ");
                        }
                    }
                    return new BaseReturnFunction<Debit>(StatusFunction.FALSE, "Không tồn tại hóa đơn");

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Debit>(StatusFunction.EXCEPTION, e.Message);
            }
        }
        public static BaseReturnFunction<Debit> EditPayment(int paymentId, String userId, DateTime createdDate, double value, String note)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    var payment = context.tb_Pays.SingleOrDefault(x => x.ID == paymentId);
                    if (payment == null)
                    {
                        return new BaseReturnFunction<Debit>(StatusFunction.FALSE, "Không tìm thấy thanh toán này");
                    }
                    var bill = payment.tb_Bill;
                    var listpayment = bill.tb_Pays.Where(x => x.ID != paymentId);
                    var sum = listpayment.Sum(x => x.Value);
                    var temp = bill.Price - sum;
                    if (temp < value)
                    {
                        return new BaseReturnFunction<Debit>(StatusFunction.FALSE, "Giá trị thanh toán vượt quá số nợ của hóa đơn này (Vui lòng thanh toán không quá " + temp.PriceFormat() + ")");
                    }
                    payment.Value = value;
                    payment.CreatedBy = userId;
                    payment.Note = note;
                    payment.CreatedDate = createdDate;
                    context.SubmitChanges();
                    return new BaseReturnFunction<Debit>(StatusFunction.TRUE, "Chỉnh sửa thanh toán thành công");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Debit>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public static BaseReturnFunction<Boolean> CreatePayment(String userName, int billId, double value, String note)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    var bill = context.tb_Bills.SingleOrDefault(x => x.ID == billId);
                    if (bill == null)
                        return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Không tìm thấy thông tin của khoản nợ này");
                    var datra = bill.tb_BillItems.Sum(x => x.Sum);
                    var no = bill.Price - datra;
                    if (value > no)
                        return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Số tiền bạn nhập vượt quá số nợ của hóa đơn này (" + no.ToString("N0") + " đ)");
                    context.tb_Pays.InsertOnSubmit(new tb_Pay()
                    {
                        BillId = billId,
                        CreatedBy = userName,
                        Note = note,
                        Value = value,
                        CreatedDate = DateTime.Now
                    });
                    context.SubmitChanges();

                    context.SubmitChanges();
                    return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }


        }

        public static BaseReturnFunction<List<Debit>> GetByCustomer(int customerId, String keyword, int page, int size)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    IQueryable<tb_Bill> result;
                    result = context.tb_Bills.Where(x => x.CustomerId.HasValue && x.CustomerId == customerId);
                    if (!String.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = keyword.ToLower().Trim().ToLower();
                        result = context.tb_Bills.Where(x => x.CustomerId.HasValue && x.CustomerId == customerId && x.Note != null && x.Note.ToLower().Contains(keyword));
                    }
                    var result2 = result.ToList();
                    var result3 = result2.Select(x => new Debit(x.ID, x.tb_Customer, x.Price, (x.tb_Pays.Count() > 0 ? x.tb_Pays.Sum(z => z.Value) : 0), x.CreatedDate, x.tb_User, x.Note)).OrderBy(x => x.Money).ToList();
                    int total = result2.Count();
                    return new BaseReturnFunction<List<Debit>>(StatusFunction.TRUE, result3.Skip((page - 1) * size).Take(size).ToList(), total);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Debit>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public static BaseReturnFunction<List<Debit>> GetByProvider(int providerId, String keyword, int page, int size)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    IQueryable<tb_Bill> result;
                    result = context.tb_Bills.Where(x => x.ProviderId.HasValue && x.ProviderId.Value == providerId);
                    if (!String.IsNullOrWhiteSpace(keyword))
                    {
                        keyword = keyword.ToLower().Trim().ToLower();
                        result = context.tb_Bills.Where(x => x.Note != null && x.Note.ToLower().Contains(keyword));
                    }
                    var result2 = result.ToList();
                    var result3 = result2.Select(x => new Debit(x.ID, x.tb_Provider, x.Price, (x.tb_Pays.Count() > 0 ? x.tb_Pays.Sum(z => z.Value) : 0), x.CreatedDate, x.tb_User, x.Note)).OrderBy(x => x.Money).ToList();
                    int total = result2.Count();
                    return new BaseReturnFunction<List<Debit>>(StatusFunction.TRUE, result3.Skip((page - 1) * size).Take(size).ToList(), total);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<List<Debit>>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public static BaseReturnFunction<DetailBill> GetDetailBill(int billId)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    var result = context.tb_Bills.SingleOrDefault(x => x.ID == billId);
                    DetailBill detailBill = null;
                    if (result != null)
                    {
                        detailBill = new Models.DetailBill(result.tb_Customer, result.tb_Provider, result, result.tb_Pays.ToList().Select(x => new Debit(x)).ToList(), result.tb_BillItems.Select(x => new Debit(x)).ToList());
                        return new BaseReturnFunction<DetailBill>(StatusFunction.TRUE, detailBill);
                    }
                    return new BaseReturnFunction<DetailBill>(StatusFunction.FALSE, "Không tìm thấy thông tin");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<DetailBill>(StatusFunction.EXCEPTION, e.Message);
            }
        }


        public static BaseReturnFunction<Boolean> DeletePayment(int paymentId)
        {
            try
            {
                using (var context = new PayDbDataContext())
                {
                    var payment = context.tb_Pays.SingleOrDefault(x => x.ID == paymentId);
                    if (payment != null)
                    {
                        context.tb_Pays.DeleteOnSubmit(payment);
                        context.SubmitChanges();
                        return new BaseReturnFunction<Boolean>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<Boolean>(StatusFunction.FALSE, "Không tồn tại thanh toán này");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<Boolean>(StatusFunction.EXCEPTION, e.Message);
            }
        }
    }
}
