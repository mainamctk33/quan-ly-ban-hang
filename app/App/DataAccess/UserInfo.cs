﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common;
using App.Database.User;
using App.Models;

namespace App.DataAccess
{
    public class UserInfo
    {
        public static event EventHandler eventUserUpdate;
        public static void NotifiAccountChange()
        {
            if (eventUserUpdate != null)
                eventUserUpdate(GetCurrentUser(), null);
        }

        private static tb_User currentUser;
        public static tb_User GetCurrentUser()
        {
            return currentUser;
        }
        public static User GetCurrentUser2()
        {
            if (!isLogin())
                return null;
            return new User(GetCurrentUser());
        }
        public static bool isLogin()
        {
            return GetCurrentUser() != null;
        }

        public static BaseReturnFunction<tb_User> GetById(String username, UserDbDataContext context)
        {
            try
            {
                var user = context.tb_Users.SingleOrDefault(x => x.UserName == username);
                if (user != null)
                    return new BaseReturnFunction<tb_User>(StatusFunction.TRUE, user);
                return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Không tìm thấy tài khoản với thông tin bạn nhập");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_User>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static void SetLogin(tb_User tb_User)
        {
            currentUser = tb_User;
        }

        public BaseReturnFunction<tb_User> Login(String username, String password)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(username) || String.IsNullOrWhiteSpace(password))
                    return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Vui lòng nhập tên đăng nhập và mật khẩu");
                using (var context = new UserDbDataContext())
                {
                    password = password.GenerateStringToMD5();
                    var user = context.tb_Users.SingleOrDefault(x => x.UserName == username && x.Password == password);
                    if (user != null)
                    {
                        if (user.Active)
                            return new BaseReturnFunction<tb_User>(StatusFunction.TRUE, user);
                        return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Tài khoản đã bị khóa");
                    }
                    return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Tài khoản hoặc tên đăng nhập không đúng");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<tb_User>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        internal static BaseReturnFunction<bool> ResetPassword(string userName)
        {
            try
            {
                using (var context = new UserDbDataContext())
                {
                    var user = context.tb_Users.SingleOrDefault(x => x.UserName == userName);
                    if (user != null)
                    {
                        user.Password = "123456".GenerateStringToMD5();
                        context.SubmitChanges();
                        if (UserInfo.GetCurrentUser().UserName == userName)
                        {
                            UserInfo.GetCurrentUser().Password = "123456".GenerateStringToMD5();
                        }
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy thông tin tài khoản " + userName, false);
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public BaseReturnFunction<bool> ChangePassword(String username, String currentPassowrd, String newPassword)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Vui lòng nhập tên đăng nhập");
                if (String.IsNullOrWhiteSpace(currentPassowrd))
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Vui lòng nhập mật khẩu hiện tại");
                if (String.IsNullOrWhiteSpace(newPassword))
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Vui lòng nhập mật khẩu mới");
                if (newPassword.Length < 6)
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Mật khẩu mới phải lớn hơn 6 ký tự");

                using (var context = new UserDbDataContext())
                {
                    currentPassowrd = currentPassowrd.GenerateStringToMD5();
                    newPassword = newPassword.GenerateStringToMD5();
                    var temp = GetById(username, context);
                    if (temp.IsTrue)
                    {
                        var user = temp.Data;
                        if (user.Password == currentPassowrd)
                        {
                            user.Password = newPassword;
                            context.SubmitChanges();
                            return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                        }
                        return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Mật khẩu bạn nhập hiện tại không đúng");

                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy tài khoản với thông tin bạn nhập");
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        internal static BaseReturnFunction<bool> Remove(string userName)
        {
            if (userName == UserInfo.GetCurrentUser().UserName)
            {
                return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Bạn không thể xóa chính mình", false);
            }
            if (userName == "admin")
            {
                return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Bạn không có quyền xóa tài khoản này");
            }
            try
            {
                using (var context = new UserDbDataContext())
                {
                    var user = context.tb_Users.SingleOrDefault(x => x.UserName == userName);
                    if (user != null)
                    {
                        user.Deleted = true;
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy tài khoản này");
                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, e.Message);
            }
        }

        public static BaseReturnFunction<tb_User> Update(String username, String fullname, int role, bool active)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(username))
                    return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Vui lòng nhập tên đăng nhập");
                if (String.IsNullOrWhiteSpace(fullname))
                    return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Vui lòng nhập họ tên");

                using (var context = new UserDbDataContext())
                {
                    var temp = GetById(username, context);
                    if (temp.IsTrue)
                    {
                        var user = temp.Data;
                        user.FullName = fullname;
                        user.Active = active;
                        user.Role = role;
                        context.SubmitChanges();
                        return new BaseReturnFunction<tb_User>(StatusFunction.TRUE, user);

                    }
                    return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Không tìm thấy tài khoản với thông tin bạn nhập");
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_User>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static BaseReturnFunction<tb_User> Create(String username, String fullname, String password, int role, bool active)
        {
            using (var context = new UserDbDataContext())
            {
                var temp = GetById(username, context);

                if (temp.IsTrue)
                {
                    return new BaseReturnFunction<tb_User>(StatusFunction.FALSE, "Tồn tại tài khoản với thông tin bạn nhập");
                }
                var user = new tb_User()
                {
                    Active = active,
                    FullName = fullname,
                    CreatedDate = DateTime.Now,
                    Password = password.GenerateStringToMD5(),
                    Role = role,
                    UserName = username
                };
                context.tb_Users.InsertOnSubmit(user);
                context.SubmitChanges();
                return new BaseReturnFunction<tb_User>(StatusFunction.TRUE, user);
            }
        }

        public static BaseReturnFunction<List<User>> Search(String name, int page, int size, int role)
        {
            try
            {
                using (var context = new UserDbDataContext())
                {
                    IQueryable<tb_User> value = context.tb_Users.Where(x => !x.Deleted);
                    if (name == null)
                        name = "";

                    if (!String.IsNullOrEmpty(name))
                    {
                        name = name.ToLower().Trim();
                        value = value.Where(x => x.UserName.ToLower().Contains(name)
                        || x.FullName.ToLower().Contains(name));
                    }
                    List<tb_User> list2 = value.ToList();
                    if (role != 0)
                    {
                        list2 = list2.FindAll(x => (x.Role & role) == role);
                    }
                    int total = list2.Count();
                    list2 = list2.Skip((page - 1) * size).Take(size).ToList();

                    return new BaseReturnFunction<List<User>>(StatusFunction.TRUE, list2.Select(x => new User(x)).ToList(), total);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<User>>(StatusFunction.TRUE, new List<User>());
            }
        }
    }
}
