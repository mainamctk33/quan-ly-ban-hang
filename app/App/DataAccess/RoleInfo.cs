﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DataAccess
{
    public class RoleInfo
    {
        public enum Role
        {
            mgrCustomer = 1,
            mgrProvider = 2,
            mgrAccount = 4,
            mgrProductType = 8,
            mgrProduct = 16,
            allowSale = 32,
        }

        public static bool HasPermission(int currentPermission, Role role)
        {
            return (currentPermission & (int)role) == (int)role;
        }
    }
}
