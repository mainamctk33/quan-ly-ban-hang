﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Database.ProductType;

namespace App.DataAccess
{
    public class ProductTypeInfo
    {
        public static BaseReturnFunction<List<tb_ProductType>> GetAll()
        {
            try
            {
                using (var context = new ProductTypeDbDataContext())
                {
                    return new BaseReturnFunction<List<tb_ProductType>>(StatusFunction.TRUE, context.tb_ProductTypes.ToList());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_ProductType>>(StatusFunction.TRUE, new List<tb_ProductType>());
            }
        }
        public static BaseReturnFunction<List<tb_ProductType>> Search(String name, int page, int size)
        {
            try
            {
                using (var context = new ProductTypeDbDataContext())
                {
                    IQueryable<tb_ProductType> value;
                    if (name == null)
                        name = "";
                    if (!String.IsNullOrEmpty(name))
                    {
                        name = name.ToLower().Trim();
                        value = context.tb_ProductTypes.Where(x => x.Name.ToLower().Contains(name)).Skip((page - 1) * size).Take(size);
                    }
                    else
                    {
                        value = context.tb_ProductTypes.Skip((page - 1) * size).Take(size);
                    }

                    return new BaseReturnFunction<List<tb_ProductType>>(StatusFunction.TRUE, value.ToList());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_ProductType>>(StatusFunction.TRUE, new List<tb_ProductType>());
            }
        }
        public static BaseReturnFunction<tb_ProductType> GetById(ProductTypeDbDataContext context, int id)
        {
            try
            {
                var provider = context.tb_ProductTypes.SingleOrDefault(x => x.ID == id);
                if (provider != null)
                    return new BaseReturnFunction<tb_ProductType>(StatusFunction.TRUE, provider);
                return new BaseReturnFunction<tb_ProductType>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_ProductType>(StatusFunction.EXCEPTION, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
        }
        public static BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new ProductTypeDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        context.tb_ProductTypes.DeleteOnSubmit(temp.Data);
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn nhập");

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Xóa loại sản phẩm không thành công");
            }

        }

        public static BaseReturnFunction<tb_ProductType> GetByName(ProductTypeDbDataContext context, String name)
        {
            try
            {
                name = name.Trim().ToLower();
                var product = context.tb_ProductTypes.SingleOrDefault(x => x.Name.ToLower() == name);
                if (product != null)
                    return new BaseReturnFunction<tb_ProductType>(StatusFunction.TRUE, product);
                return new BaseReturnFunction<tb_ProductType>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_ProductType>(StatusFunction.EXCEPTION, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
        }
        public static BaseReturnFunction<tb_ProductType> Create(String name, bool active)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<tb_ProductType>(StatusFunction.FALSE, "Vui lòng nhập tên loại sản phẩm");
                using (var context = new ProductTypeDbDataContext())
                {
                    var temp = GetByName(context, name);
                    if (temp.IsTrue)
                        return new BaseReturnFunction<tb_ProductType>(StatusFunction.FALSE, "Tồn tại loại sản phẩm với tên bạn nhập");
                    var provider = new tb_ProductType()
                    {
                        Active = active,
                        Name = name.Trim(),
                    };
                    context.tb_ProductTypes.InsertOnSubmit(provider);
                    context.SubmitChanges();
                    return new BaseReturnFunction<tb_ProductType>(StatusFunction.TRUE, provider);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_ProductType>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static BaseReturnFunction<tb_ProductType> Update(int id, String name, bool active)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<tb_ProductType>(StatusFunction.FALSE, "Vui lòng nhập tên loại sản phẩm");
                using (var context = new ProductTypeDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        if (name.Trim().ToLower() != temp.Data.Name.Trim().ToLower())
                        {
                            var temp2 = GetByName(context, name);
                            if (temp2.IsTrue)
                            {
                                return new BaseReturnFunction<tb_ProductType>(StatusFunction.FALSE, "Tồn tại loại sản phẩm với với thông tin bạn nhập");
                            }
                        }
                        var provider = temp.Data;
                        provider.Active = active;
                        provider.Name = name.Trim().ToLower();
                        context.SubmitChanges();
                        return new BaseReturnFunction<tb_ProductType>(StatusFunction.TRUE, provider);
                    }
                    return new BaseReturnFunction<tb_ProductType>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_ProductType>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

    }
}
