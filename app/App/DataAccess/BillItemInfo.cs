﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Database.BillItem;

namespace App.DataAccess
{
    public class BillItemInfo
    {
        public static BaseReturnFunction<List<tb_BillItem>> GetByBillId(int billId)
        {
            try
            {
                using (var context = new BillItemDbDataContext())
                {
                    IQueryable<tb_BillItem> value = context.tb_BillItems.Where(x => x.BillId == billId);
                    return new BaseReturnFunction<List<tb_BillItem>>(StatusFunction.TRUE, value.ToList());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_BillItem>>(StatusFunction.TRUE, new List<tb_BillItem>());
            }
        }
        public static BaseReturnFunction<tb_BillItem> GetById(BillItemDbDataContext context, int id)
        {
            try
            {
                var customer = context.tb_BillItems.SingleOrDefault(x => x.ID == id);
                if (customer != null)
                    return new BaseReturnFunction<tb_BillItem>(StatusFunction.TRUE, customer);
                return new BaseReturnFunction<tb_BillItem>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_BillItem>(StatusFunction.EXCEPTION, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
        }
        public static BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new BillItemDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        context.tb_BillItems.DeleteOnSubmit(temp.Data);
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn nhập");

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Xóa sản phẩm không thành công");
            }

        }
        public static BaseReturnFunction<tb_BillItem> Create(int billId, decimal price, int productId, String productName, int quantity)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(productName))
                    return new BaseReturnFunction<tb_BillItem>(StatusFunction.FALSE, "Vui lòng nhập tên sản phẩm");
                using (var context = new BillItemDbDataContext())
                {
                    var billItem = new tb_BillItem()
                    {
                        BillId = billId,
                        Price = price,
                        ProductId = productId,
                        ProductName = productName,
                        Quantily = quantity
                    };
                    context.tb_BillItems.InsertOnSubmit(billItem);
                    context.SubmitChanges();
                    return new BaseReturnFunction<tb_BillItem>(StatusFunction.TRUE, billItem);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_BillItem>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static BaseReturnFunction<tb_BillItem> Update(int billItemId, int billId, decimal price, int productId, String productName, int quantity)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(productName))
                    return new BaseReturnFunction<tb_BillItem>(StatusFunction.FALSE, "Vui lòng nhập tên sản phẩm");
                using (var context = new BillItemDbDataContext())
                {
                    var temp = GetById(context, billItemId);
                    if (temp.IsTrue)
                    {
                        var billItem = temp.Data;
                        billItem.Price = price;
                        billItem.ProductId = productId;
                        billItem.ProductName = productName;
                        billItem.Quantily = quantity;
                        context.SubmitChanges();
                        return new BaseReturnFunction<tb_BillItem>(StatusFunction.TRUE, billItem);
                    }
                    return new BaseReturnFunction<tb_BillItem>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_BillItem>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

    }
}
