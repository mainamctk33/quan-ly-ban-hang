﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Database.Provider;
namespace App.DataAccess
{
    public class ProviderInfo
    {
        public static BaseReturnFunction<List<tb_Provider>> GetAll()
        {
            try
            {
                using (var context = new ProviderDbDataContext())
                {
                    return new BaseReturnFunction<List<tb_Provider>>(StatusFunction.TRUE, context.tb_Providers.ToList());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Provider>>(StatusFunction.TRUE, new List<tb_Provider>());
            }
        }
        public static BaseReturnFunction<List<tb_Provider>> Search2(String name, int page, int size)
        {
            try
            {
                using (var context = new ProviderDbDataContext())
                {
                    IQueryable<tb_Provider> value;
                    if (name == null)
                        name = "";

                    if (!String.IsNullOrEmpty(name))
                    {
                        name = name.ToLower().Trim();
                        value = context.tb_Providers.Where(x => x.Name.ToLower().Contains(name));
                    }
                    else
                    {
                        value = context.tb_Providers;
                    }

                    return new BaseReturnFunction<List<tb_Provider>>(StatusFunction.TRUE, value.Skip((page - 1) * size).Take(size).ToList(), value.Count());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Provider>>(StatusFunction.TRUE, new List<tb_Provider>());
            }
        }
        public static BaseReturnFunction<List<tb_Provider>> Search(String name, int page, int size, bool active)
        {
            try
            {
                using (var context = new ProviderDbDataContext())
                {
                    IQueryable<tb_Provider> value;
                    if (name == null)
                        name = "";
                    value = context.tb_Providers.Where(x => x.Active == active);
                    if (!String.IsNullOrEmpty(name))
                    {
                        name = name.ToLower().Trim();
                        value = context.tb_Providers.Where(x => x.Name.ToLower().Contains(name));
                    }

                    return new BaseReturnFunction<List<tb_Provider>>(StatusFunction.TRUE, value.Skip((page - 1) * size).Take(size).ToList(), value.Count());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Provider>>(StatusFunction.TRUE, new List<tb_Provider>());
            }
        }

        public static BaseReturnFunction<tb_Provider> GetById1(int id)
        {
            using (var context = new ProviderDbDataContext())
            {
                return GetById(context, id);
            }
        }
        public static BaseReturnFunction<tb_Provider> GetById(ProviderDbDataContext context, int id)
        {
            try
            {
                var provider = context.tb_Providers.SingleOrDefault(x => x.ID == id);
                if (provider != null)
                    return new BaseReturnFunction<tb_Provider>(StatusFunction.TRUE, provider);
                return new BaseReturnFunction<tb_Provider>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Provider>(StatusFunction.EXCEPTION, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
        }
        public static BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new ProviderDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        context.tb_Providers.DeleteOnSubmit(temp.Data);
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn nhập");

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Xóa nhà cung cấp không thành công");
            }

        }

        public static BaseReturnFunction<tb_Provider> GetByName(ProviderDbDataContext context, String name)
        {
            try
            {
                name = name.Trim().ToLower();
                var product = context.tb_Providers.SingleOrDefault(x => x.Name.ToLower() == name);
                if (product != null)
                    return new BaseReturnFunction<tb_Provider>(StatusFunction.TRUE, product);
                return new BaseReturnFunction<tb_Provider>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Provider>(StatusFunction.EXCEPTION, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
        }
        public static BaseReturnFunction<tb_Provider> Create(String name, String address, String phone, String note, bool active)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<tb_Provider>(StatusFunction.FALSE, "Vui lòng nhập tên nhà cung cấp");
                using (var context = new ProviderDbDataContext())
                {
                    var temp = GetByName(context, name);
                    if (temp.IsTrue)
                        return new BaseReturnFunction<tb_Provider>(StatusFunction.FALSE, "Tồn tại nhà cung cấp với tên bạn nhập");
                    var provider = new tb_Provider()
                    {
                        Active = active,
                        Address = address,
                        Name = name.Trim(),
                        Note = note,
                        Phone = phone
                    };
                    context.tb_Providers.InsertOnSubmit(provider);
                    context.SubmitChanges();
                    return new BaseReturnFunction<tb_Provider>(StatusFunction.TRUE, provider);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Provider>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static BaseReturnFunction<tb_Provider> Update(int id, String name, String note, String address, String phone, bool active)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<tb_Provider>(StatusFunction.FALSE, "Vui lòng nhập tên nhà cung cấp");
                using (var context = new ProviderDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        if (name.Trim().ToLower() != temp.Data.Name.Trim().ToLower())
                        {
                            var temp2 = GetByName(context, name);
                            if (temp2.IsTrue)
                            {
                                return new BaseReturnFunction<tb_Provider>(StatusFunction.FALSE, "Tồn tại nhà cung cấp với với thông tin bạn nhập");
                            }
                        }
                        var provider = temp.Data;
                        provider.Active = active;
                        provider.Note = note;
                        provider.Phone = phone;
                        provider.Name = name.Trim().ToLower();
                        context.SubmitChanges();
                        return new BaseReturnFunction<tb_Provider>(StatusFunction.TRUE, provider);
                    }
                    return new BaseReturnFunction<tb_Provider>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Provider>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

    }
}
