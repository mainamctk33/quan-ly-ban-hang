﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Database.Customer;

namespace App.DataAccess
{
    public class CustomerInfo
    {
        public static BaseReturnFunction<List<tb_Customer>> GetAll()
        {
            try
            {
                using (var context = new CustomerDbDataContext())
                {
                    return new BaseReturnFunction<List<tb_Customer>>(StatusFunction.TRUE, context.tb_Customers.ToList());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Customer>>(StatusFunction.TRUE, new List<tb_Customer>());
            }
        }
        public static BaseReturnFunction<List<tb_Customer>> Search2(String name, int page, int size)
        {
            try
            {
                using (var context = new CustomerDbDataContext())
                {
                    IQueryable<tb_Customer> value;
                    if (name == null)
                        name = "";

                    if (!String.IsNullOrEmpty(name))
                    {
                        name = name.ToLower().Trim();
                        value = context.tb_Customers.Where(x => x.FullName.ToLower().Contains(name));
                    }
                    else
                    {
                        value = context.tb_Customers;
                    }

                    return new BaseReturnFunction<List<tb_Customer>>(StatusFunction.TRUE, value.Skip((page - 1) * size).Take(size).ToList(), value.Count());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Customer>>(StatusFunction.TRUE, new List<tb_Customer>());
            }
        }
        public static BaseReturnFunction<List<tb_Customer>> Search(String name, int page, int size, bool active)
        {
            try
            {
                using (var context = new CustomerDbDataContext())
                {
                    IQueryable<tb_Customer> value;
                    if (name == null)
                        name = "";
                    value = context.tb_Customers.Where(x =>x.Active == active);
                    if (!String.IsNullOrEmpty(name))
                    {
                        name = name.ToLower().Trim();
                        value = value.Where(x =>x.FullName.ToLower().Contains(name));
                    }
                    return new BaseReturnFunction<List<tb_Customer>>(StatusFunction.TRUE, value.Skip((page - 1) * size).Take(size).ToList(), value.Count());
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<List<tb_Customer>>(StatusFunction.TRUE, new List<tb_Customer>());
            }
        }
        public static BaseReturnFunction<tb_Customer> GetById1(int id)
        {
            try
            {
                using (var context = new CustomerDbDataContext())
                {
                    return GetById(context, id);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Customer>(StatusFunction.EXCEPTION, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
        }
        public static BaseReturnFunction<tb_Customer> GetById(CustomerDbDataContext context, int id)
        {
            try
            {
                var customer = context.tb_Customers.SingleOrDefault(x => x.ID == id);
                if (customer != null)
                    return new BaseReturnFunction<tb_Customer>(StatusFunction.TRUE, customer);
                return new BaseReturnFunction<tb_Customer>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Customer>(StatusFunction.EXCEPTION, "Không tìm thấy dữ liệu với thông tin bạn cung cấp");
            }
        }
        public static BaseReturnFunction<bool> Delete(int id)
        {
            try
            {
                using (var context = new CustomerDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        context.tb_Customers.DeleteOnSubmit(temp.Data);
                        context.SubmitChanges();
                        return new BaseReturnFunction<bool>(StatusFunction.TRUE, true);
                    }
                    return new BaseReturnFunction<bool>(StatusFunction.FALSE, "Không tìm thấy dữ liệu với thông tin bạn nhập");

                }
            }
            catch (Exception e)
            {
                return new BaseReturnFunction<bool>(StatusFunction.EXCEPTION, "Xóa khách hàng không thành công");
            }

        }

        public static BaseReturnFunction<tb_Customer> GetByName(CustomerDbDataContext context, String name)
        {
            try
            {
                name = name.Trim().ToLower();
                var product = context.tb_Customers.SingleOrDefault(x => x.FullName.ToLower() == name);
                if (product != null)
                    return new BaseReturnFunction<tb_Customer>(StatusFunction.TRUE, product);
                return new BaseReturnFunction<tb_Customer>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Customer>(StatusFunction.EXCEPTION, "Không tìm thấy thông tin với dữ liệu bạn nhập");
            }
        }
        public static BaseReturnFunction<tb_Customer> Create(String name, String address, String phone, bool active, bool isFamiliar)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<tb_Customer>(StatusFunction.FALSE, "Vui lòng nhập tên khách hàng");
                using (var context = new CustomerDbDataContext())
                {
                    var temp = GetByName(context, name);
                    if (temp.IsTrue)
                        return new BaseReturnFunction<tb_Customer>(StatusFunction.FALSE, "Tồn tại khách hàng với tên bạn nhập");
                    var customer = new tb_Customer()
                    {
                        Active = active,
                        Address = address,
                        FullName = name.Trim(),
                        isFamiliar = isFamiliar,
                        Phone = phone
                    };
                    context.tb_Customers.InsertOnSubmit(customer);
                    context.SubmitChanges();
                    return new BaseReturnFunction<tb_Customer>(StatusFunction.TRUE, customer);
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Customer>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

        public static BaseReturnFunction<tb_Customer> Update(int id, String name, String note, String address, String phone, bool active, bool isFamiliar)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(name))
                    return new BaseReturnFunction<tb_Customer>(StatusFunction.FALSE, "Vui lòng nhập tên khách hàng");
                using (var context = new CustomerDbDataContext())
                {
                    var temp = GetById(context, id);
                    if (temp.IsTrue)
                    {
                        if (name.Trim().ToLower() != temp.Data.FullName.Trim().ToLower())
                        {
                            var temp2 = GetByName(context, name);
                            if (temp2.IsTrue)
                            {
                                return new BaseReturnFunction<tb_Customer>(StatusFunction.FALSE, "Tồn tại khách hàng với với thông tin bạn nhập");
                            }
                        }
                        var customer = temp.Data;
                        customer.Active = active;
                        customer.Phone = phone;
                        customer.FullName = name.Trim().ToLower();
                        customer.isFamiliar = isFamiliar;
                        context.SubmitChanges();
                        return new BaseReturnFunction<tb_Customer>(StatusFunction.TRUE, customer);
                    }
                    return new BaseReturnFunction<tb_Customer>(StatusFunction.FALSE, "Không tìm thấy thông tin với dữ liệu bạn nhập");
                }
            }
            catch (Exception)
            {
                return new BaseReturnFunction<tb_Customer>(StatusFunction.EXCEPTION, "Lỗi xảy ra vui lòng thử lại sau");
            }
        }

    }
}
