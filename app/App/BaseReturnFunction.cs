﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App
{
    public class BaseReturnFunction<T> where T: new()
    {
        public StatusFunction Status { get; set; }
        public T Data { get; set; }
        public int Total { get; set; }
        public String Message { get; set; }

        public BaseReturnFunction()
        {
            Status = StatusFunction.TRUE;
            Total = 0;
        }

        public BaseReturnFunction(StatusFunction status, String message, int total)
        {
            this.Total = total;
            this.Status = status;
            this.Message = message;
        }

        public BaseReturnFunction(StatusFunction status, String message, T data) : this(status, message, 0)
        {
            Data = data;
        }
        public BaseReturnFunction(StatusFunction status, String message, T data, int total) : this(status, message, total)
        {
            Data = data;
        }
        public BaseReturnFunction(StatusFunction status, T data) : this(status, "", data)
        {
        }
        public BaseReturnFunction(StatusFunction status, T data, int total) : this(status, "", data, total)
        {
        }
        public BaseReturnFunction(StatusFunction status, String message) : this(status, message, default(T), 0)
        {
        }

        public bool IsTrue => Status == StatusFunction.TRUE;
    }

    public enum StatusFunction
    {
        TRUE,
        FALSE,
        EXCEPTION
    }
}