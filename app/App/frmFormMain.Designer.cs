﻿namespace App
{
    partial class frmFormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFormMain));
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.ribbonPanel4 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar8 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonPanel5 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar10 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonBar9 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonPanel3 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
            this.tbManage = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar1 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonBar6 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonBar5 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonBar4 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonBar3 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonPanel2 = new DevComponents.DotNetBar.RibbonPanel();
            this.ribbonBar7 = new DevComponents.DotNetBar.RibbonBar();
            this.ribbonPanel1 = new DevComponents.DotNetBar.RibbonPanel();
            this.rbInfo = new DevComponents.DotNetBar.RibbonBar();
            this.lbTime = new DevComponents.DotNetBar.LabelItem();
            this.lbFullName = new DevComponents.DotNetBar.LabelItem();
            this.rbAccount = new DevComponents.DotNetBar.RibbonBar();
            this.tabAccount = new DevComponents.DotNetBar.RibbonTabItem();
            this.tabManage = new DevComponents.DotNetBar.RibbonTabItem();
            this.tabSale = new DevComponents.DotNetBar.RibbonTabItem();
            this.tabConfig = new DevComponents.DotNetBar.RibbonTabItem();
            this.tabSoNo = new DevComponents.DotNetBar.RibbonTabItem();
            this.tabThongKe = new DevComponents.DotNetBar.RibbonTabItem();
            this.btnInfo = new DevComponents.DotNetBar.ButtonItem();
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.TabFunction = new DevComponents.DotNetBar.SuperTabControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnDebtBook = new DevComponents.DotNetBar.ButtonItem();
            this.btnSoNoNhaCungCap = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
            this.btnConfig = new DevComponents.DotNetBar.ButtonItem();
            this.btnMgrAccount = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddNewAccount = new DevComponents.DotNetBar.ButtonItem();
            this.btnMgrCustomer = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddNewCustomer = new DevComponents.DotNetBar.ButtonItem();
            this.btnMgrProvider = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddNewProvider = new DevComponents.DotNetBar.ButtonItem();
            this.btnMgrProductType = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddNewProductType = new DevComponents.DotNetBar.ButtonItem();
            this.btnMgrProduct = new DevComponents.DotNetBar.ButtonItem();
            this.btnAddNewProduct = new DevComponents.DotNetBar.ButtonItem();
            this.btnSale = new DevComponents.DotNetBar.ButtonItem();
            this.btnLogin = new DevComponents.DotNetBar.ButtonItem();
            this.btnLogout = new DevComponents.DotNetBar.ButtonItem();
            this.btnChangePassword = new DevComponents.DotNetBar.ButtonItem();
            this.btnMyAccount = new DevComponents.DotNetBar.ButtonItem();
            this.applicationButton1 = new DevComponents.DotNetBar.ApplicationButton();
            this.btnNhapHang = new DevComponents.DotNetBar.ButtonItem();
            this.ribbonControl1.SuspendLayout();
            this.ribbonPanel4.SuspendLayout();
            this.ribbonPanel5.SuspendLayout();
            this.ribbonPanel3.SuspendLayout();
            this.tbManage.SuspendLayout();
            this.ribbonPanel2.SuspendLayout();
            this.ribbonPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabFunction)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ribbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Controls.Add(this.ribbonPanel2);
            this.ribbonControl1.Controls.Add(this.ribbonPanel4);
            this.ribbonControl1.Controls.Add(this.ribbonPanel5);
            this.ribbonControl1.Controls.Add(this.ribbonPanel3);
            this.ribbonControl1.Controls.Add(this.tbManage);
            this.ribbonControl1.Controls.Add(this.ribbonPanel1);
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.ForeColor = System.Drawing.Color.Black;
            this.ribbonControl1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.tabAccount,
            this.tabManage,
            this.tabSale,
            this.tabConfig,
            this.tabSoNo,
            this.tabThongKe});
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(5, 1);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.applicationButton1,
            this.btnInfo});
            this.ribbonControl1.Size = new System.Drawing.Size(1053, 175);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.ribbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.ribbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.ribbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.ribbonControl1.SystemText.QatDialogAddButton = "&Add >>";
            this.ribbonControl1.SystemText.QatDialogCancelButton = "Cancel";
            this.ribbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.ribbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.ribbonControl1.SystemText.QatDialogOkButton = "OK";
            this.ribbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatDialogRemoveButton = "&Remove";
            this.ribbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.ribbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.ribbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 0;
            this.ribbonControl1.Text = "PHẦN MỀM QUẢN LÝ CỬA HÀNG";
            this.ribbonControl1.TitleText = "PHẦN MỀM QUẢN LÝ CỬA HÀNG";
            this.ribbonControl1.SelectedRibbonTabChanged += new System.EventHandler(this.ribbonControl1_SelectedRibbonTabChanged);
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel4.Controls.Add(this.ribbonBar8);
            this.ribbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel4.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel4.Name = "ribbonPanel4";
            this.ribbonPanel4.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel4.Size = new System.Drawing.Size(1053, 116);
            // 
            // 
            // 
            this.ribbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel4.TabIndex = 5;
            this.ribbonPanel4.Visible = false;
            // 
            // ribbonBar8
            // 
            this.ribbonBar8.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar8.ContainerControlProcessDialogKey = true;
            this.ribbonBar8.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar8.DragDropSupport = true;
            this.ribbonBar8.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnDebtBook,
            this.btnSoNoNhaCungCap});
            this.ribbonBar8.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar8.Name = "ribbonBar8";
            this.ribbonBar8.Size = new System.Drawing.Size(209, 113);
            this.ribbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar8.TabIndex = 3;
            this.ribbonBar8.Text = "Sổ nợ";
            // 
            // 
            // 
            this.ribbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel5.Controls.Add(this.ribbonBar10);
            this.ribbonPanel5.Controls.Add(this.ribbonBar9);
            this.ribbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel5.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel5.Name = "ribbonPanel5";
            this.ribbonPanel5.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel5.Size = new System.Drawing.Size(1053, 116);
            // 
            // 
            // 
            this.ribbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel5.TabIndex = 6;
            this.ribbonPanel5.Visible = false;
            // 
            // ribbonBar10
            // 
            this.ribbonBar10.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar10.ContainerControlProcessDialogKey = true;
            this.ribbonBar10.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar10.DragDropSupport = true;
            this.ribbonBar10.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem1});
            this.ribbonBar10.Location = new System.Drawing.Point(212, 0);
            this.ribbonBar10.Name = "ribbonBar10";
            this.ribbonBar10.Size = new System.Drawing.Size(309, 113);
            this.ribbonBar10.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar10.TabIndex = 5;
            this.ribbonBar10.Text = "Hàng Hóa";
            // 
            // 
            // 
            this.ribbonBar10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar10.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonBar9
            // 
            this.ribbonBar9.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar9.ContainerControlProcessDialogKey = true;
            this.ribbonBar9.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar9.DragDropSupport = true;
            this.ribbonBar9.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem2,
            this.buttonItem3});
            this.ribbonBar9.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar9.Name = "ribbonBar9";
            this.ribbonBar9.Size = new System.Drawing.Size(209, 113);
            this.ribbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar9.TabIndex = 4;
            this.ribbonBar9.Text = "Thu Nhập";
            // 
            // 
            // 
            this.ribbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel3.Controls.Add(this.ribbonBar2);
            this.ribbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel3.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel3.Name = "ribbonPanel3";
            this.ribbonPanel3.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel3.Size = new System.Drawing.Size(1053, 116);
            // 
            // 
            // 
            this.ribbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel3.TabIndex = 4;
            this.ribbonPanel3.Visible = false;
            // 
            // ribbonBar2
            // 
            this.ribbonBar2.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar2.ContainerControlProcessDialogKey = true;
            this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar2.DragDropSupport = true;
            this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnConfig});
            this.ribbonBar2.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar2.Name = "ribbonBar2";
            this.ribbonBar2.Size = new System.Drawing.Size(112, 113);
            this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar2.TabIndex = 0;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // tbManage
            // 
            this.tbManage.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.tbManage.Controls.Add(this.ribbonBar1);
            this.tbManage.Controls.Add(this.ribbonBar6);
            this.tbManage.Controls.Add(this.ribbonBar5);
            this.tbManage.Controls.Add(this.ribbonBar4);
            this.tbManage.Controls.Add(this.ribbonBar3);
            this.tbManage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbManage.Location = new System.Drawing.Point(0, 56);
            this.tbManage.Name = "tbManage";
            this.tbManage.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.tbManage.Size = new System.Drawing.Size(1053, 116);
            // 
            // 
            // 
            this.tbManage.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.tbManage.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.tbManage.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tbManage.TabIndex = 2;
            this.tbManage.Visible = false;
            // 
            // ribbonBar1
            // 
            this.ribbonBar1.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar1.ContainerControlProcessDialogKey = true;
            this.ribbonBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar1.DragDropSupport = true;
            this.ribbonBar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMgrAccount,
            this.btnAddNewAccount});
            this.ribbonBar1.Location = new System.Drawing.Point(839, 0);
            this.ribbonBar1.Name = "ribbonBar1";
            this.ribbonBar1.Size = new System.Drawing.Size(209, 113);
            this.ribbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar1.TabIndex = 4;
            this.ribbonBar1.Text = "Tài khoản";
            // 
            // 
            // 
            this.ribbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonBar6
            // 
            this.ribbonBar6.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar6.ContainerControlProcessDialogKey = true;
            this.ribbonBar6.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar6.DragDropSupport = true;
            this.ribbonBar6.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMgrCustomer,
            this.btnAddNewCustomer});
            this.ribbonBar6.Location = new System.Drawing.Point(630, 0);
            this.ribbonBar6.Name = "ribbonBar6";
            this.ribbonBar6.Size = new System.Drawing.Size(209, 113);
            this.ribbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar6.TabIndex = 3;
            this.ribbonBar6.Text = "Khách hàng";
            // 
            // 
            // 
            this.ribbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonBar5
            // 
            this.ribbonBar5.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar5.ContainerControlProcessDialogKey = true;
            this.ribbonBar5.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar5.DragDropSupport = true;
            this.ribbonBar5.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMgrProvider,
            this.btnAddNewProvider});
            this.ribbonBar5.Location = new System.Drawing.Point(421, 0);
            this.ribbonBar5.Name = "ribbonBar5";
            this.ribbonBar5.Size = new System.Drawing.Size(209, 113);
            this.ribbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar5.TabIndex = 2;
            this.ribbonBar5.Text = "Nhà sản xuất";
            // 
            // 
            // 
            this.ribbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonBar4
            // 
            this.ribbonBar4.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar4.ContainerControlProcessDialogKey = true;
            this.ribbonBar4.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar4.DragDropSupport = true;
            this.ribbonBar4.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMgrProductType,
            this.btnAddNewProductType});
            this.ribbonBar4.Location = new System.Drawing.Point(212, 0);
            this.ribbonBar4.Name = "ribbonBar4";
            this.ribbonBar4.Size = new System.Drawing.Size(209, 113);
            this.ribbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar4.TabIndex = 1;
            this.ribbonBar4.Text = "Loại sản phẩm";
            // 
            // 
            // 
            this.ribbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonBar3
            // 
            this.ribbonBar3.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.ContainerControlProcessDialogKey = true;
            this.ribbonBar3.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar3.DragDropSupport = true;
            this.ribbonBar3.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnMgrProduct,
            this.btnAddNewProduct});
            this.ribbonBar3.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar3.Name = "ribbonBar3";
            this.ribbonBar3.Size = new System.Drawing.Size(209, 113);
            this.ribbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar3.TabIndex = 0;
            this.ribbonBar3.Text = "Sản phẩm";
            // 
            // 
            // 
            this.ribbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar3.ItemClick += new System.EventHandler(this.ribbonBar3_ItemClick);
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel2.Controls.Add(this.ribbonBar7);
            this.ribbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel2.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel2.Name = "ribbonPanel2";
            this.ribbonPanel2.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel2.Size = new System.Drawing.Size(1053, 116);
            // 
            // 
            // 
            this.ribbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel2.TabIndex = 3;
            // 
            // ribbonBar7
            // 
            this.ribbonBar7.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonBar7.ContainerControlProcessDialogKey = true;
            this.ribbonBar7.Dock = System.Windows.Forms.DockStyle.Left;
            this.ribbonBar7.DragDropSupport = true;
            this.ribbonBar7.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnSale,
            this.btnNhapHang});
            this.ribbonBar7.Location = new System.Drawing.Point(3, 0);
            this.ribbonBar7.Name = "ribbonBar7";
            this.ribbonBar7.Size = new System.Drawing.Size(202, 113);
            this.ribbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonBar7.TabIndex = 1;
            this.ribbonBar7.Text = "Sản phẩm";
            // 
            // 
            // 
            this.ribbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ribbonPanel1.Controls.Add(this.rbInfo);
            this.ribbonPanel1.Controls.Add(this.rbAccount);
            this.ribbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ribbonPanel1.Location = new System.Drawing.Point(0, 56);
            this.ribbonPanel1.Name = "ribbonPanel1";
            this.ribbonPanel1.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ribbonPanel1.Size = new System.Drawing.Size(1053, 116);
            // 
            // 
            // 
            this.ribbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.ribbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ribbonPanel1.TabIndex = 1;
            this.ribbonPanel1.Visible = false;
            // 
            // rbInfo
            // 
            this.rbInfo.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rbInfo.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbInfo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbInfo.ContainerControlProcessDialogKey = true;
            this.rbInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbInfo.DragDropSupport = true;
            this.rbInfo.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lbTime,
            this.lbFullName});
            this.rbInfo.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.rbInfo.Location = new System.Drawing.Point(323, 0);
            this.rbInfo.MinimumSize = new System.Drawing.Size(295, 113);
            this.rbInfo.Name = "rbInfo";
            this.rbInfo.Size = new System.Drawing.Size(295, 113);
            this.rbInfo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.rbInfo.TabIndex = 1;
            // 
            // 
            // 
            this.rbInfo.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbInfo.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // lbTime
            // 
            this.lbTime.Name = "lbTime";
            this.lbTime.PaddingLeft = 20;
            this.lbTime.PaddingTop = 30;
            this.lbTime.Text = "labelItem1";
            // 
            // lbFullName
            // 
            this.lbFullName.Name = "lbFullName";
            this.lbFullName.PaddingLeft = 20;
            this.lbFullName.PaddingTop = 15;
            this.lbFullName.Text = "labelItem1";
            // 
            // rbAccount
            // 
            this.rbAccount.AutoOverflowEnabled = false;
            // 
            // 
            // 
            this.rbAccount.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbAccount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rbAccount.ContainerControlProcessDialogKey = true;
            this.rbAccount.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbAccount.DragDropSupport = true;
            this.rbAccount.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnLogin,
            this.btnLogout,
            this.btnChangePassword,
            this.btnMyAccount});
            this.rbAccount.Location = new System.Drawing.Point(3, 0);
            this.rbAccount.Name = "rbAccount";
            this.rbAccount.Size = new System.Drawing.Size(320, 113);
            this.rbAccount.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.rbAccount.TabIndex = 0;
            this.rbAccount.Text = "Tài khoản";
            // 
            // 
            // 
            this.rbAccount.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rbAccount.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // tabAccount
            // 
            this.tabAccount.Name = "tabAccount";
            this.tabAccount.Panel = this.ribbonPanel1;
            this.tabAccount.Text = "Tài khoản";
            // 
            // tabManage
            // 
            this.tabManage.Name = "tabManage";
            this.tabManage.Panel = this.tbManage;
            this.tabManage.Text = "Quản lý";
            // 
            // tabSale
            // 
            this.tabSale.Checked = true;
            this.tabSale.Name = "tabSale";
            this.tabSale.Panel = this.ribbonPanel2;
            this.tabSale.Text = "Bán hàng";
            // 
            // tabConfig
            // 
            this.tabConfig.Name = "tabConfig";
            this.tabConfig.Panel = this.ribbonPanel3;
            this.tabConfig.Text = "Cấu hình";
            // 
            // tabSoNo
            // 
            this.tabSoNo.Name = "tabSoNo";
            this.tabSoNo.Panel = this.ribbonPanel4;
            this.tabSoNo.Text = "Sổ nợ";
            // 
            // tabThongKe
            // 
            this.tabThongKe.Name = "tabThongKe";
            this.tabThongKe.Panel = this.ribbonPanel5;
            this.tabThongKe.Text = "Thống kê";
            // 
            // btnInfo
            // 
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Text = "Thông tin";
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2007Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(163)))), ((int)(((byte)(26))))));
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbStatus});
            this.statusStrip1.Location = new System.Drawing.Point(5, 384);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1053, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbStatus
            // 
            this.lbStatus.BackColor = System.Drawing.Color.Transparent;
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(173, 17);
            this.lbStatus.Text = "Vui lòng đăng nhập để sử dụng";
            // 
            // TabFunction
            // 
            this.TabFunction.CloseButtonOnTabsVisible = true;
            // 
            // 
            // 
            // 
            // 
            // 
            this.TabFunction.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.TabFunction.ControlBox.MenuBox.Name = "";
            this.TabFunction.ControlBox.Name = "";
            this.TabFunction.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.TabFunction.ControlBox.MenuBox,
            this.TabFunction.ControlBox.CloseBox});
            this.TabFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabFunction.Location = new System.Drawing.Point(5, 176);
            this.TabFunction.Name = "TabFunction";
            this.TabFunction.ReorderTabsEnabled = true;
            this.TabFunction.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.TabFunction.SelectedTabIndex = -1;
            this.TabFunction.Size = new System.Drawing.Size(1053, 208);
            this.TabFunction.TabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.TabFunction.TabIndex = 3;
            this.TabFunction.Text = "superTabControl1";
            this.TabFunction.TabItemClose += new System.EventHandler<DevComponents.DotNetBar.SuperTabStripTabItemCloseEventArgs>(this.TabFunction_TabItemClose);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnDebtBook
            // 
            this.btnDebtBook.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDebtBook.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnDebtBook.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDebtBook.FixedSize = new System.Drawing.Size(100, 90);
            this.btnDebtBook.Image = global::App.Properties.Resources.parchment;
            this.btnDebtBook.ImagePaddingHorizontal = 10;
            this.btnDebtBook.ImagePaddingVertical = 10;
            this.btnDebtBook.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnDebtBook.Name = "btnDebtBook";
            this.btnDebtBook.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnDebtBook.SubItemsExpandWidth = 14;
            this.btnDebtBook.Text = "Khách hàng";
            this.btnDebtBook.Click += new System.EventHandler(this.btnDebtBook_Click);
            // 
            // btnSoNoNhaCungCap
            // 
            this.btnSoNoNhaCungCap.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSoNoNhaCungCap.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnSoNoNhaCungCap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSoNoNhaCungCap.FixedSize = new System.Drawing.Size(100, 90);
            this.btnSoNoNhaCungCap.Image = global::App.Properties.Resources.parchment;
            this.btnSoNoNhaCungCap.ImagePaddingHorizontal = 10;
            this.btnSoNoNhaCungCap.ImagePaddingVertical = 10;
            this.btnSoNoNhaCungCap.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSoNoNhaCungCap.Name = "btnSoNoNhaCungCap";
            this.btnSoNoNhaCungCap.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnSoNoNhaCungCap.SubItemsExpandWidth = 14;
            this.btnSoNoNhaCungCap.Text = "Nhà cung cấp";
            this.btnSoNoNhaCungCap.Click += new System.EventHandler(this.btnSoNoNhaCungCap_Click);
            // 
            // buttonItem4
            // 
            this.buttonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem4.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonItem4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonItem4.FixedSize = new System.Drawing.Size(100, 90);
            this.buttonItem4.Image = global::App.Properties.Resources.parchment;
            this.buttonItem4.ImagePaddingHorizontal = 10;
            this.buttonItem4.ImagePaddingVertical = 10;
            this.buttonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem4.Name = "buttonItem4";
            this.buttonItem4.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.buttonItem4.SubItemsExpandWidth = 14;
            this.buttonItem4.Text = "Đã bán";
            // 
            // buttonItem5
            // 
            this.buttonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem5.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonItem5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonItem5.FixedSize = new System.Drawing.Size(100, 90);
            this.buttonItem5.Image = global::App.Properties.Resources.parchment;
            this.buttonItem5.ImagePaddingHorizontal = 10;
            this.buttonItem5.ImagePaddingVertical = 10;
            this.buttonItem5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem5.Name = "buttonItem5";
            this.buttonItem5.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.buttonItem5.SubItemsExpandWidth = 14;
            this.buttonItem5.Text = "Đã Nhập";
            // 
            // buttonItem1
            // 
            this.buttonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem1.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonItem1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonItem1.FixedSize = new System.Drawing.Size(100, 90);
            this.buttonItem1.Image = global::App.Properties.Resources.parchment;
            this.buttonItem1.ImagePaddingHorizontal = 10;
            this.buttonItem1.ImagePaddingVertical = 10;
            this.buttonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.buttonItem1.SubItemsExpandWidth = 14;
            this.buttonItem1.Text = "Đã Nhập";
            // 
            // buttonItem2
            // 
            this.buttonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem2.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonItem2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonItem2.FixedSize = new System.Drawing.Size(100, 90);
            this.buttonItem2.Image = global::App.Properties.Resources.parchment;
            this.buttonItem2.ImagePaddingHorizontal = 10;
            this.buttonItem2.ImagePaddingVertical = 10;
            this.buttonItem2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem2.Name = "buttonItem2";
            this.buttonItem2.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.buttonItem2.SubItemsExpandWidth = 14;
            this.buttonItem2.Text = "Thu";
            // 
            // buttonItem3
            // 
            this.buttonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem3.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.buttonItem3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonItem3.FixedSize = new System.Drawing.Size(100, 90);
            this.buttonItem3.Image = global::App.Properties.Resources.parchment;
            this.buttonItem3.ImagePaddingHorizontal = 10;
            this.buttonItem3.ImagePaddingVertical = 10;
            this.buttonItem3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.buttonItem3.Name = "buttonItem3";
            this.buttonItem3.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.buttonItem3.SubItemsExpandWidth = 14;
            this.buttonItem3.Text = "Chi";
            // 
            // btnConfig
            // 
            this.btnConfig.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnConfig.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnConfig.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfig.FixedSize = new System.Drawing.Size(100, 90);
            this.btnConfig.Image = global::App.Properties.Resources.settings;
            this.btnConfig.ImagePaddingHorizontal = 10;
            this.btnConfig.ImagePaddingVertical = 10;
            this.btnConfig.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnConfig.SubItemsExpandWidth = 14;
            this.btnConfig.Text = "Cấu hình";
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // btnMgrAccount
            // 
            this.btnMgrAccount.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMgrAccount.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnMgrAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMgrAccount.FixedSize = new System.Drawing.Size(100, 90);
            this.btnMgrAccount.Image = global::App.Properties.Resources.smartphone;
            this.btnMgrAccount.ImagePaddingHorizontal = 10;
            this.btnMgrAccount.ImagePaddingVertical = 10;
            this.btnMgrAccount.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMgrAccount.Name = "btnMgrAccount";
            this.btnMgrAccount.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnMgrAccount.SubItemsExpandWidth = 14;
            this.btnMgrAccount.Text = "Danh sách";
            this.btnMgrAccount.Click += new System.EventHandler(this.btnMgrAccount_Click);
            // 
            // btnAddNewAccount
            // 
            this.btnAddNewAccount.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddNewAccount.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddNewAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewAccount.FixedSize = new System.Drawing.Size(100, 90);
            this.btnAddNewAccount.Image = global::App.Properties.Resources.follower;
            this.btnAddNewAccount.ImagePaddingHorizontal = 10;
            this.btnAddNewAccount.ImagePaddingVertical = 10;
            this.btnAddNewAccount.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAddNewAccount.Name = "btnAddNewAccount";
            this.btnAddNewAccount.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnAddNewAccount.SubItemsExpandWidth = 14;
            this.btnAddNewAccount.Text = "Thêm mới";
            this.btnAddNewAccount.Click += new System.EventHandler(this.btnAddNewAccount_Click);
            // 
            // btnMgrCustomer
            // 
            this.btnMgrCustomer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMgrCustomer.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnMgrCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMgrCustomer.FixedSize = new System.Drawing.Size(100, 90);
            this.btnMgrCustomer.Image = global::App.Properties.Resources.student;
            this.btnMgrCustomer.ImagePaddingHorizontal = 10;
            this.btnMgrCustomer.ImagePaddingVertical = 10;
            this.btnMgrCustomer.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMgrCustomer.Name = "btnMgrCustomer";
            this.btnMgrCustomer.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnMgrCustomer.SubItemsExpandWidth = 14;
            this.btnMgrCustomer.Text = "Danh sách";
            this.btnMgrCustomer.Click += new System.EventHandler(this.btnMgrCustomer_Click);
            // 
            // btnAddNewCustomer
            // 
            this.btnAddNewCustomer.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddNewCustomer.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddNewCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewCustomer.FixedSize = new System.Drawing.Size(100, 90);
            this.btnAddNewCustomer.Image = global::App.Properties.Resources.button;
            this.btnAddNewCustomer.ImagePaddingHorizontal = 10;
            this.btnAddNewCustomer.ImagePaddingVertical = 10;
            this.btnAddNewCustomer.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAddNewCustomer.Name = "btnAddNewCustomer";
            this.btnAddNewCustomer.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnAddNewCustomer.SubItemsExpandWidth = 14;
            this.btnAddNewCustomer.Text = "Thêm mới";
            this.btnAddNewCustomer.Click += new System.EventHandler(this.btnAddNewCustomer_Click);
            // 
            // btnMgrProvider
            // 
            this.btnMgrProvider.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMgrProvider.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnMgrProvider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMgrProvider.FixedSize = new System.Drawing.Size(100, 90);
            this.btnMgrProvider.Image = global::App.Properties.Resources.atm;
            this.btnMgrProvider.ImagePaddingHorizontal = 10;
            this.btnMgrProvider.ImagePaddingVertical = 10;
            this.btnMgrProvider.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMgrProvider.Name = "btnMgrProvider";
            this.btnMgrProvider.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnMgrProvider.SubItemsExpandWidth = 14;
            this.btnMgrProvider.Text = "Danh sách";
            this.btnMgrProvider.Click += new System.EventHandler(this.btnMgrProvider_Click);
            // 
            // btnAddNewProvider
            // 
            this.btnAddNewProvider.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddNewProvider.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddNewProvider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewProvider.FixedSize = new System.Drawing.Size(100, 90);
            this.btnAddNewProvider.Image = global::App.Properties.Resources.button;
            this.btnAddNewProvider.ImagePaddingHorizontal = 10;
            this.btnAddNewProvider.ImagePaddingVertical = 10;
            this.btnAddNewProvider.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAddNewProvider.Name = "btnAddNewProvider";
            this.btnAddNewProvider.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnAddNewProvider.SubItemsExpandWidth = 14;
            this.btnAddNewProvider.Text = "Thêm mới";
            this.btnAddNewProvider.Click += new System.EventHandler(this.btnAddNewProvider_Click);
            // 
            // btnMgrProductType
            // 
            this.btnMgrProductType.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMgrProductType.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnMgrProductType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMgrProductType.FixedSize = new System.Drawing.Size(100, 90);
            this.btnMgrProductType.Image = global::App.Properties.Resources.inbox;
            this.btnMgrProductType.ImagePaddingHorizontal = 10;
            this.btnMgrProductType.ImagePaddingVertical = 10;
            this.btnMgrProductType.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMgrProductType.Name = "btnMgrProductType";
            this.btnMgrProductType.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnMgrProductType.SubItemsExpandWidth = 14;
            this.btnMgrProductType.Text = "Danh sách";
            this.btnMgrProductType.Click += new System.EventHandler(this.btnMgrProductType_Click);
            // 
            // btnAddNewProductType
            // 
            this.btnAddNewProductType.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddNewProductType.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddNewProductType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewProductType.FixedSize = new System.Drawing.Size(100, 90);
            this.btnAddNewProductType.Image = global::App.Properties.Resources.button;
            this.btnAddNewProductType.ImagePaddingHorizontal = 10;
            this.btnAddNewProductType.ImagePaddingVertical = 10;
            this.btnAddNewProductType.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAddNewProductType.Name = "btnAddNewProductType";
            this.btnAddNewProductType.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnAddNewProductType.SubItemsExpandWidth = 14;
            this.btnAddNewProductType.Text = "Thêm mới";
            this.btnAddNewProductType.Click += new System.EventHandler(this.btnAddNewProductType_Click);
            // 
            // btnMgrProduct
            // 
            this.btnMgrProduct.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMgrProduct.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnMgrProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMgrProduct.FixedSize = new System.Drawing.Size(100, 90);
            this.btnMgrProduct.Image = global::App.Properties.Resources.dashboard;
            this.btnMgrProduct.ImagePaddingHorizontal = 10;
            this.btnMgrProduct.ImagePaddingVertical = 10;
            this.btnMgrProduct.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMgrProduct.Name = "btnMgrProduct";
            this.btnMgrProduct.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnMgrProduct.SubItemsExpandWidth = 14;
            this.btnMgrProduct.Text = "Danh sách";
            this.btnMgrProduct.Click += new System.EventHandler(this.btnMgrProduct_Click);
            // 
            // btnAddNewProduct
            // 
            this.btnAddNewProduct.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnAddNewProduct.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnAddNewProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddNewProduct.FixedSize = new System.Drawing.Size(100, 90);
            this.btnAddNewProduct.Image = global::App.Properties.Resources.button;
            this.btnAddNewProduct.ImagePaddingHorizontal = 10;
            this.btnAddNewProduct.ImagePaddingVertical = 10;
            this.btnAddNewProduct.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnAddNewProduct.Name = "btnAddNewProduct";
            this.btnAddNewProduct.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnAddNewProduct.SubItemsExpandWidth = 14;
            this.btnAddNewProduct.Text = "Thêm mới";
            this.btnAddNewProduct.Click += new System.EventHandler(this.btnAddNewProduct_Click);
            // 
            // btnSale
            // 
            this.btnSale.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnSale.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnSale.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSale.FixedSize = new System.Drawing.Size(100, 90);
            this.btnSale.Image = global::App.Properties.Resources.document;
            this.btnSale.ImagePaddingHorizontal = 10;
            this.btnSale.ImagePaddingVertical = 10;
            this.btnSale.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnSale.Name = "btnSale";
            this.btnSale.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnSale.SubItemsExpandWidth = 14;
            this.btnSale.Text = "Bán hàng";
            this.btnSale.Click += new System.EventHandler(this.btnSale_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLogin.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.FixedSize = new System.Drawing.Size(100, 90);
            this.btnLogin.Image = global::App.Properties.Resources.iclogin;
            this.btnLogin.ImagePaddingHorizontal = 10;
            this.btnLogin.ImagePaddingVertical = 10;
            this.btnLogin.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnLogin.SubItemsExpandWidth = 14;
            this.btnLogin.Text = "Đăng nhập";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLogout.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.FixedSize = new System.Drawing.Size(100, 90);
            this.btnLogout.Image = global::App.Properties.Resources.logout;
            this.btnLogout.ImagePaddingHorizontal = 10;
            this.btnLogout.ImagePaddingVertical = 10;
            this.btnLogout.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnLogout.SubItemsExpandWidth = 14;
            this.btnLogout.Text = "Đăng xuất";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnChangePassword.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnChangePassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnChangePassword.FixedSize = new System.Drawing.Size(100, 90);
            this.btnChangePassword.Image = global::App.Properties.Resources.air_filter;
            this.btnChangePassword.ImagePaddingHorizontal = 10;
            this.btnChangePassword.ImagePaddingVertical = 10;
            this.btnChangePassword.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnChangePassword.SubItemsExpandWidth = 14;
            this.btnChangePassword.Text = "Đổi mật khẩu";
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // btnMyAccount
            // 
            this.btnMyAccount.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnMyAccount.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnMyAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMyAccount.FixedSize = new System.Drawing.Size(100, 90);
            this.btnMyAccount.Image = global::App.Properties.Resources.youtuber;
            this.btnMyAccount.ImagePaddingHorizontal = 10;
            this.btnMyAccount.ImagePaddingVertical = 10;
            this.btnMyAccount.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnMyAccount.Name = "btnMyAccount";
            this.btnMyAccount.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnMyAccount.SubItemsExpandWidth = 14;
            this.btnMyAccount.Text = "Tài khoản của tôi";
            this.btnMyAccount.Click += new System.EventHandler(this.btnMyAccount_Click);
            // 
            // applicationButton1
            // 
            this.applicationButton1.AutoExpandOnClick = true;
            this.applicationButton1.CanCustomize = false;
            this.applicationButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.applicationButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("applicationButton1.Icon")));
            this.applicationButton1.Image = ((System.Drawing.Image)(resources.GetObject("applicationButton1.Image")));
            this.applicationButton1.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.applicationButton1.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Default;
            this.applicationButton1.ImagePaddingHorizontal = 2;
            this.applicationButton1.ImagePaddingVertical = 2;
            this.applicationButton1.Name = "applicationButton1";
            this.applicationButton1.ShowSubItems = false;
            // 
            // btnNhapHang
            // 
            this.btnNhapHang.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnNhapHang.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat;
            this.btnNhapHang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNhapHang.FixedSize = new System.Drawing.Size(100, 90);
            this.btnNhapHang.Image = global::App.Properties.Resources.document;
            this.btnNhapHang.ImagePaddingHorizontal = 10;
            this.btnNhapHang.ImagePaddingVertical = 10;
            this.btnNhapHang.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnNhapHang.Name = "btnNhapHang";
            this.btnNhapHang.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(10);
            this.btnNhapHang.SubItemsExpandWidth = 14;
            this.btnNhapHang.Text = "Nhập hàng";
            this.btnNhapHang.Click += new System.EventHandler(this.btnNhapHang_Click);
            // 
            // frmFormMain
            // 
            this.ClientSize = new System.Drawing.Size(1063, 408);
            this.Controls.Add(this.TabFunction);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ribbonControl1);
            this.EnableGlass = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "frmFormMain";
            this.Text = "PHẦN MỀM QUẢN LÝ BÁN HÀNG";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.SizeChanged += new System.EventHandler(this.frmFormMain_SizeChanged);
            this.ribbonControl1.ResumeLayout(false);
            this.ribbonControl1.PerformLayout();
            this.ribbonPanel4.ResumeLayout(false);
            this.ribbonPanel5.ResumeLayout(false);
            this.ribbonPanel3.ResumeLayout(false);
            this.tbManage.ResumeLayout(false);
            this.ribbonPanel2.ResumeLayout(false);
            this.ribbonPanel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabFunction)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel1;
        private DevComponents.DotNetBar.RibbonBar rbAccount;
        private DevComponents.DotNetBar.RibbonPanel tbManage;
        private DevComponents.DotNetBar.RibbonTabItem tabAccount;
        private DevComponents.DotNetBar.RibbonTabItem tabManage;
        private DevComponents.DotNetBar.ButtonItem btnInfo;
        private DevComponents.DotNetBar.StyleManager styleManager1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lbStatus;
        private DevComponents.DotNetBar.SuperTabControl TabFunction;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel2;
        private DevComponents.DotNetBar.RibbonTabItem tabSale;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel3;
        private DevComponents.DotNetBar.RibbonTabItem tabConfig;
        private DevComponents.DotNetBar.RibbonBar ribbonBar2;
        private DevComponents.DotNetBar.ButtonItem btnConfig;
        private DevComponents.DotNetBar.RibbonBar ribbonBar3;
        private DevComponents.DotNetBar.ButtonItem btnMgrProduct;
        private DevComponents.DotNetBar.ApplicationButton applicationButton1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar4;
        private DevComponents.DotNetBar.ButtonItem btnMgrProductType;
        private DevComponents.DotNetBar.RibbonBar ribbonBar5;
        private DevComponents.DotNetBar.ButtonItem btnMgrProvider;
        private DevComponents.DotNetBar.RibbonBar ribbonBar6;
        private DevComponents.DotNetBar.ButtonItem btnMgrCustomer;
        private DevComponents.DotNetBar.ButtonItem btnAddNewProduct;
        private DevComponents.DotNetBar.ButtonItem btnAddNewProductType;
        private DevComponents.DotNetBar.ButtonItem btnAddNewProvider;
        private DevComponents.DotNetBar.ButtonItem btnAddNewCustomer;
        private DevComponents.DotNetBar.ButtonItem btnLogin;
        private DevComponents.DotNetBar.ButtonItem btnLogout;
        private DevComponents.DotNetBar.ButtonItem btnChangePassword;
        private DevComponents.DotNetBar.ButtonItem btnMyAccount;
        private DevComponents.DotNetBar.RibbonBar rbInfo;
        private DevComponents.DotNetBar.LabelItem lbFullName;
        private DevComponents.DotNetBar.RibbonBar ribbonBar1;
        private DevComponents.DotNetBar.ButtonItem btnMgrAccount;
        private DevComponents.DotNetBar.ButtonItem btnAddNewAccount;
        private DevComponents.DotNetBar.LabelItem lbTime;
        private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar7;
        private DevComponents.DotNetBar.ButtonItem btnSale;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel4;
        private DevComponents.DotNetBar.RibbonTabItem tabSoNo;
        private DevComponents.DotNetBar.RibbonBar ribbonBar8;
        private DevComponents.DotNetBar.ButtonItem btnDebtBook;
        private DevComponents.DotNetBar.ButtonItem btnSoNoNhaCungCap;
        private DevComponents.DotNetBar.RibbonPanel ribbonPanel5;
        private DevComponents.DotNetBar.RibbonTabItem tabThongKe;
        private DevComponents.DotNetBar.RibbonBar ribbonBar9;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.RibbonBar ribbonBar10;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem btnNhapHang;
    }
}