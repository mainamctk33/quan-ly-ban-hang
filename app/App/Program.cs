﻿using App.FormApp.Product;
using App.FormApp.Sell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmFormMain());
            }
            catch (ThreadAbortException e)
            {
                Thread.ResetAbort();
            }
        }
    }
}
