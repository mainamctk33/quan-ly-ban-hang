﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Common;
using App.DataAccess;
using App.Utils;

namespace App
{
    public partial class frmChangePassword : DevComponents.DotNetBar.Office2007Form
    {
        private _del1Param del;

        public frmChangePassword(_del1Param e)
        {
            InitializeComponent();
            this.del = e;
            FormUtils.ConfigDialog(this);
        }

        private void txtConfirmPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnChange.PerformClick();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (UserInfo.isLogin())
            {
                var currentPassword = txtPassword.Text;
                var newPassword = txtNewPassword.Text;
                var confirmNewPassword = txtConfirmPassword.Text;
                if (String.IsNullOrWhiteSpace(currentPassword))
                {
                    frmDialog.ShowCancelDialog("Thông báo", "Vui lòng nhập mật khẩu hiện tại", this);
                    return;
                }
                if (String.IsNullOrWhiteSpace(newPassword))
                {
                    frmDialog.ShowCancelDialog("Thông báo", "Vui lòng nhập mật khẩu mới", this);
                    return;
                }
                if (String.IsNullOrWhiteSpace(confirmNewPassword))
                {
                    frmDialog.ShowCancelDialog("Thông báo", "Vui lòng xác nhận mật khẩu mới", this);
                    return;
                }
                if (newPassword != confirmNewPassword)
                {
                    frmDialog.ShowCancelDialog("Thông báo", "Xác nhận mật khẩu không trùng khớp", this);
                    return;
                }
                if (currentPassword.GenerateStringToMD5() != UserInfo.GetCurrentUser().Password)
                {
                    frmDialog.ShowCancelDialog("Thông báo", "Mật khẩu hiện tại không đúng", this);
                    return;
                }
                frmLoading.Run(() =>
                {
                    return new UserInfo().ChangePassword(UserInfo.GetCurrentUser().UserName, currentPassword, newPassword);
                }, (result, ctrl) =>
                {
                    if (result.IsTrue)
                    {
                        del(newPassword.GenerateStringToMD5());
                        this.Close();
                    }
                    else
                        frmDialog.ShowCancelDialog("Thông báo", result.Message, ctrl);
                }, this);

            }


        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
