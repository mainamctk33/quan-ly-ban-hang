﻿namespace App
{
    partial class ucPaging
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNext = new DevComponents.DotNetBar.ButtonX();
            this.lbTotalPage = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.btnPreview = new DevComponents.DotNetBar.ButtonX();
            this.lbTotalItem = new DevComponents.DotNetBar.LabelX();
            this.txtPage = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnNext.Location = new System.Drawing.Point(626, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(27, 24);
            this.btnNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2013;
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = ">";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lbTotalPage
            // 
            // 
            // 
            // 
            this.lbTotalPage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbTotalPage.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbTotalPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalPage.Location = new System.Drawing.Point(599, 3);
            this.lbTotalPage.Name = "lbTotalPage";
            this.lbTotalPage.Size = new System.Drawing.Size(27, 24);
            this.lbTotalPage.TabIndex = 2;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelX1.Location = new System.Drawing.Point(586, 3);
            this.labelX1.Name = "labelX1";
            this.labelX1.PaddingLeft = 5;
            this.labelX1.PaddingRight = 5;
            this.labelX1.Size = new System.Drawing.Size(13, 24);
            this.labelX1.TabIndex = 3;
            this.labelX1.Text = "/";
            // 
            // btnPreview
            // 
            this.btnPreview.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPreview.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground;
            this.btnPreview.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPreview.Location = new System.Drawing.Point(526, 3);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(27, 24);
            this.btnPreview.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2013;
            this.btnPreview.TabIndex = 5;
            this.btnPreview.Text = "<";
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // lbTotalItem
            // 
            // 
            // 
            // 
            this.lbTotalItem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbTotalItem.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbTotalItem.Location = new System.Drawing.Point(3, 3);
            this.lbTotalItem.Name = "lbTotalItem";
            this.lbTotalItem.Size = new System.Drawing.Size(110, 24);
            this.lbTotalItem.TabIndex = 6;
            // 
            // txtPage
            // 
            this.txtPage.Dock = System.Windows.Forms.DockStyle.Right;
            this.txtPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPage.Location = new System.Drawing.Point(553, 3);
            this.txtPage.Name = "txtPage";
            this.txtPage.Size = new System.Drawing.Size(33, 24);
            this.txtPage.TabIndex = 7;
            this.txtPage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPage_KeyDown);
            this.txtPage.Leave += new System.EventHandler(this.txtPage_Leave);
            // 
            // ucPaging
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.txtPage);
            this.Controls.Add(this.lbTotalItem);
            this.Controls.Add(this.labelX1);
            this.Controls.Add(this.lbTotalPage);
            this.Controls.Add(this.btnNext);
            this.MinimumSize = new System.Drawing.Size(0, 30);
            this.Name = "ucPaging";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Size = new System.Drawing.Size(656, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnNext;
        private DevComponents.DotNetBar.LabelX lbTotalPage;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.ButtonX btnPreview;
        private DevComponents.DotNetBar.LabelX lbTotalItem;
        private System.Windows.Forms.TextBox txtPage;
    }
}
