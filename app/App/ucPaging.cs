﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Utils;

namespace App
{
    public partial class ucPaging : UserControl
    {
        int currentPage = 1;
        int totalPage = 1;
        int totalItem = 0;
        public void setData(int currentPage, int totalPage, int totalItem)
        {
            this.currentPage = currentPage;
            this.totalItem = totalItem;
            this.totalPage = totalPage;
            txtPage.Text = currentPage.ToString();
            lbTotalPage.Text = totalPage.ToString();
            lbTotalItem.Text = "Tìm thấy: " + totalItem.ToString() + " item(s)";
        }
        public ucPaging()
        {
            InitializeComponent();
            txtPage.Text = currentPage.ToString();
            lbTotalPage.Text = totalPage.ToString();
            lbTotalItem.Text = "Tìm thấy: " + totalItem.ToString() + " item(s)";
        }

        public event EventHandler pageChange;

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (currentPage < totalPage)
            {
                currentPage++;
                if (pageChange != null)
                    pageChange(currentPage, e);
                txtPage.Text = currentPage.ToString();
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage--;
                if (pageChange != null)
                    pageChange(currentPage, e);
                txtPage.Text = currentPage.ToString();
            }
        }

        private void txtPage_Leave(object sender, EventArgs e)
        {
            textChange();
        }

        private void txtPage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textChange();
            }
        }

        void textChange()
        {
            int page = ConvertUtils.ToInt(txtPage.Text);
            if (page != currentPage)
            {
                if (page <= 0)
                {
                    page = 1;
                }
                if (page > totalPage)
                {
                    page = 1;
                }

                currentPage = page;
                txtPage.Text = page.ToString();
                if (pageChange != null)
                    pageChange(page, null);
            }
        }
    }
}
