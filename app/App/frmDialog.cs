﻿using App.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App
{
    public partial class frmDialog : DevComponents.DotNetBar.Office2007Form
    {
        public frmDialog()
        {
            InitializeComponent();
            FormUtils.ConfigDialog(this);
        }

        internal static bool ShowCancelDialog(string title, string message, Form form, string cancelText)
        {
            if (form != null)
                form.Opacity = 0;
            frmDialog frm = new frmDialog();
            frm.Text = title;
            frm.btnOk.Hide();
            frm.lnCancel.Visible = true;
            frm.btnCancel.Text = cancelText;
            frm.tvMessage.Text = message;
            var result = frm.ShowDialog();
            if (form != null)
                form.Opacity = 100;
            return true;
        }

        internal static bool ShowCancelDialog(string title, string message, Form form)
        {
            return ShowCancelDialog(title, message, form, "Hủy bỏ");
        }
        internal static bool ShowCancelDialog(string message, Form form)
        {
            return ShowCancelDialog("Thông báo", message, form, "Hủy bỏ");
        }
        internal static bool ShowCancelDialog(string title, string message)
        {
            return ShowCancelDialog(title, message, null);
        }
        internal static bool ShowCancelDialog(string message)
        {
            return ShowCancelDialog("Thông báo", message, null);
        }

        internal static bool ShowOkDialog(string title, string message, Form form, string okText)
        {
            if (form != null)
                form.Opacity = 0;
            frmDialog frm = new frmDialog();
            frm.Text = title;
            frm.btnOk.Show();
            frm.lnCancel.Visible = false;
            frm.btnOk.Text = okText;
            frm.tvMessage.Text = message;
            var result = frm.ShowDialog();
            if (form != null)
                form.Opacity = 100;
            return true;
        }

        internal static bool ShowOkDialog(string message, Form form)
        {
            return ShowOkDialog("Thông báo", message, form);
        }

        internal static bool ShowOkDialog(string title, string message)
        {
            return ShowOkDialog(title, message, null);
        }
        internal static bool ShowOkDialog(string title, string message, Form form)
        {
            return ShowOkDialog(title, message, form, "Chấp nhận");
        }
        internal static bool ShowOkCancelDialog(string title, string message, string okText, string cancelText)
        {
            return ShowOkCancelDialog(title, message, null, okText, cancelText);
        }
        internal static bool ShowOkCancelDialog(string title, string message)
        {
            return ShowOkCancelDialog(title, message, null);
        }



        internal static bool ShowOkCancelDialog(string title, string message, Form form, string okText, string cancelText)
        {
            if (form != null)

                form.Opacity = 0;
            frmDialog frm = new frmDialog();
            frm.Text = title;
            frm.btnOk.Text = okText;
            frm.btnCancel.Text = cancelText;
            frm.tvMessage.Text = message;
            var result = frm.ShowDialog();
            if (form != null)

                form.Opacity = 100;
            return result == DialogResult.OK;
        }

        internal static bool ShowOkCancelDialog(string title, string message, Form form)
        {
            return ShowOkCancelDialog(title, message, form, "Chấp nhận", "Hủy bỏ");
        }

    }
}
