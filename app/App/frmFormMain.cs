﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using App.FormApp.Setting;
using App.FormApp.Customer;
using App.FormApp.Provider;
using App.FormApp.ProductType;
using App.FormApp.Product;
using App.DataAccess;
using App.Database.User;
using App.FormApp.Account;
using App.Models;
using App.FormApp.Sell;
using App.FormApp.DebtBook;
using App.FormApp.About;
using App.FormApp.Money.Loan;

namespace App
{
    public delegate void _del1Param(Object o);

    public partial class frmFormMain : DevComponents.DotNetBar.RibbonForm
    {
        private frmMgrCustomer _frmCustomer;
        private frmMgrProvider _frmProvider;
        private frmMgrProductType _frmMgrProductType;
        private frmMgrProduct _frmMgrProduct;
        private frmMgrAccount _frmMgrAccount;
        private frmSoNoKhachHang _frmDebtBook;
        private frmSoNoNhaCungCap _frmLoan;

        public frmFormMain()
        {
            InitializeComponent();
            UpdateUILogin();
            ProgressClass.toolStripStatusLabel = lbStatus;
            UserInfo.eventUserUpdate += UserInfo_eventUserUpdate;
        }

        private void UserInfo_eventUserUpdate(object sender, EventArgs e)
        {
            UpdateUILogin();
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            frmConfig frm = new frmConfig((a) =>
            {
                Application.Restart();
                Environment.Exit(0);
            });
            frm.ShowDialog();
        }

        private void frmFormMain_SizeChanged(object sender, EventArgs e)
        {

        }

        private void ribbonBar3_ItemClick(object sender, EventArgs e)
        {

        }

        private void btnMgrProduct_Click(object sender, EventArgs e)
        {
            if (_frmMgrProduct == null)
                _frmMgrProduct = new frmMgrProduct();
            ShowFeature(_frmMgrProduct);
        }

        private void btnMgrCustomer_Click(object sender, EventArgs e)
        {
            if (_frmCustomer == null)
                _frmCustomer = new frmMgrCustomer();
            ShowFeature(_frmCustomer);
        }
        public bool FormIsInactive(string Title)
        {
            foreach (SuperTabItem it in TabFunction.Tabs)
                if (it.Text == Title)
                {
                    if (TabFunction.SelectedTab != it)
                    {
                        TabFunction.SelectedTab = it;
                    }
                    return false;
                }
            return true;

            //foreach (Form frm in MdiChildren)
            //    if (frm.GetType().Name == FormName)
            //    {
            //        frm.BringToFront();
            //        return false;
            //    }
            //return true;
        }
        public void ShowFeature(Form frm)
        {
            if (FormIsInactive(frm.Text))
            {
                frm.TopLevel = false;
                SuperTabItem tab = new DevComponents.DotNetBar.SuperTabItem();
                tab.Text = frm.Text;
                tab.GlobalItem = false;

                SuperTabControlPanel pn = new SuperTabControlPanel();
                tab.AttachedControl = pn;
                TabFunction.SelectedTab = tab;


                pn.Dock = System.Windows.Forms.DockStyle.Fill;
                pn.Location = new System.Drawing.Point(0, 33);
                pn.Name = "superTabControlPanel2";
                pn.Size = new System.Drawing.Size(636, 144);
                pn.TabIndex = 0;
                pn.TabItem = tab;
                TabFunction.Tabs.Add(tab);
                TabFunction.Controls.Add(pn);
                frm.Parent = this;
                pn.Controls.Add(frm);
                frm.Dock = DockStyle.Fill;
                frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                frm.BackColor = this.BackColor;
                frm.Size = pn.Size;
                frm.Show();



                //if (!this.IsMdiContainer)
                //    IsMdiContainer = true;
                //frm.MdiParent = this;
                //frm.LocationChanged += new EventHandler(frm_LocationChanged);
                //frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
                //frm.ShowIcon = false;
                //frm.ShowInTaskbar = false;
                //frm.Text = "";
                //frm.ControlBox = false;
                //frm.BackColor = this.BackColor;
                //frm.WindowState = FormWindowState.Maximized;
                //frm.MinimizeBox = false;
                //frm.MaximizeBox = false;
                //frm.Show();
            }
        }

        private void btnMgrProvider_Click(object sender, EventArgs e)
        {
            if (_frmProvider == null)
                _frmProvider = new frmMgrProvider();
            ShowFeature(_frmProvider);
        }

        private void TabFunction_TabItemClose(object sender, SuperTabStripTabItemCloseEventArgs e)
        {
            if (_frmCustomer != null && e.Tab.Text == _frmCustomer.Text)
                _frmCustomer = null;
            else
            if (_frmProvider != null && e.Tab.Text == _frmProvider.Text)
                _frmProvider = null;
            else
            if (_frmMgrProduct != null && e.Tab.Text == _frmMgrProduct.Text)
                _frmMgrProduct = null;
            else
            if (_frmMgrProductType != null && e.Tab.Text == _frmMgrProductType.Text)
                _frmMgrProductType = null;
            else
            if (_frmMgrAccount != null && e.Tab.Text == _frmMgrAccount.Text)
                _frmMgrAccount = null;

        }

        private void btnMgrProductType_Click(object sender, EventArgs e)
        {
            if (_frmMgrProductType == null)
                _frmMgrProductType = new frmMgrProductType();
            ShowFeature(_frmMgrProductType);
        }

        private void btnAddNewProduct_Click(object sender, EventArgs e)
        {
            frmAddNewProduct frm = new frmAddNewProduct((a) =>
            {
                ProgressClass.ShowStatus(this, "Thêm mới sản phẩm thành công");
                if (_frmMgrProduct != null)
                {
                    _frmMgrProduct.addNew(a);
                }
            });
            frm.ShowDialog();
        }

        private void btnAddNewProductType_Click(object sender, EventArgs e)
        {
            frmAddNewProductType frm = new frmAddNewProductType((a) =>
            {
                ProgressClass.ShowStatus(this, "Thêm mới loại sản phẩm thành công");
                if (_frmMgrProductType != null)
                {
                    _frmMgrProductType.addNew(a);
                }
            });
            frm.ShowDialog();
        }

        private void btnAddNewCustomer_Click(object sender, EventArgs e)
        {
            frmAddNewCustomer frm = new frmAddNewCustomer((a) =>
            {
                ProgressClass.ShowStatus(this, "Thêm mới khách hàng thành công");
                if (_frmCustomer != null)
                {
                    _frmCustomer.addNew(a);
                }
            });
            frm.ShowDialog();
        }

        private void btnAddNewProvider_Click(object sender, EventArgs e)
        {
            frmAddNewProvider frm = new frmAddNewProvider((a) =>
            {
                ProgressClass.ShowStatus(this, "Thêm mới nhà cung cấp thành công");
                if (_frmProvider != null)
                {
                    _frmProvider.addNew(a);
                }
            });
            frm.ShowDialog();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            frmLogin frm = new frmLogin((a) =>
            {
                if (a is tb_User)
                {
                    ProgressClass.ShowStatus(this, lbStatus, "Đăng nhập thành công");
                    UserInfo.SetLogin(a as tb_User);
                    UpdateUILogin();
                }
            });
            frm.ShowDialog();
        }
        private void UpdateUILogin()
        {
            var isLogin = UserInfo.isLogin();
            btnLogin.Visible = !isLogin;
            btnLogout.Visible = isLogin;
            btnChangePassword.Visible = isLogin;
            btnMyAccount.Visible = isLogin;
            rbInfo.Visible = isLogin;
            if (isLogin)
                rbAccount.MinimumSize = new Size(320, 113);
            else
                rbAccount.MinimumSize = new Size(106, 113);
            if (isLogin)
            {
                lbFullName.Text = "Xin chào, " + UserInfo.GetCurrentUser().FullName;
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            TabFunction.Tabs.Clear();
            TabFunction.Controls.Clear();
            _frmCustomer = null;
            _frmMgrAccount = null;
            _frmProvider = null;
            _frmMgrProductType = null;
            _frmMgrProduct = null;

            UserInfo.SetLogin(null);
            UpdateUILogin();
            ProgressClass.ShowStatus(this, lbStatus, "Bạn đã đăng xuất");
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            frmChangePassword frm = new frmChangePassword((a) =>
            {
                UserInfo.GetCurrentUser().Password = a as String;
                ProgressClass.ShowStatus(this, lbStatus, "Đổi mật khẩu thành công");
            });
            frm.ShowDialog();
        }

        private void btnMyAccount_Click(object sender, EventArgs e)
        {

        }

        private void ribbonControl1_SelectedRibbonTabChanged(object sender, EventArgs e)
        {
            try
            {
                if (ribbonControl1.SelectedRibbonTabItem.Name == "")
                {
                    ribbonControl1.SelectFirstVisibleRibbonTab();
                    return;
                }
                if (ribbonControl1.SelectedRibbonTabItem.Name == "tabAccount" || ribbonControl1.SelectedRibbonTabItem.Name == "tabConfig")
                    return;
                else
                {
                    if (UserInfo.isLogin())
                        return;
                    ribbonControl1.SelectFirstVisibleRibbonTab();
                    ProgressClass.ShowStatus(this, "Vui lòng đăng nhập để thực hiện");
                    frmDialog.ShowCancelDialog("Vui lòng đăng nhập để thực hiện");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnMgrAccount_Click(object sender, EventArgs e)
        {
            if (_frmMgrAccount == null)
                _frmMgrAccount = new frmMgrAccount();
            ShowFeature(_frmMgrAccount);
        }

        private void btnAddNewAccount_Click(object sender, EventArgs e)
        {
            frmAddNewAccount frm = new frmAddNewAccount((a) =>
            {
                ProgressClass.ShowStatus(this, "Thêm mới tài khoản thành công");
                if (_frmMgrAccount != null)
                {
                    _frmMgrAccount.addNew(a);
                }
            });
            frm.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime time = DateTime.Now;

            lbTime.Text = String.Format("{0:F}", time); ;
        }

        private void btnSale_Click(object sender, EventArgs e)
        {
            frmFormSell sle = new frmFormSell();
            sle.ShowDialog();
            if (_frmCustomer != null)
                _frmCustomer.Reload();
        }

        private void btnDebtBook_Click(object sender, EventArgs e)
        {
            if (_frmDebtBook == null)
                _frmDebtBook = new frmSoNoKhachHang();
            ShowFeature(_frmDebtBook);
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            frmAbout frm = new frmAbout();
            frm.ShowDialog();
        }

        private void btnSoNoNhaCungCap_Click(object sender, EventArgs e)
        {
            if (_frmLoan == null)
                _frmLoan = new frmSoNoNhaCungCap();
            ShowFeature(_frmLoan);
        }

        private void btnNhapHang_Click(object sender, EventArgs e)
        {
            frmSelectProvider dialog = new frmSelectProvider();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                frmNhapHang frm = new frmNhapHang(dialog.Value);
                frm.ShowDialog();
            }
        }
    }
}