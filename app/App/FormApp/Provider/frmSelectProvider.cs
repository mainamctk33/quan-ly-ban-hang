using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using App.Utils;
using App.Database.Customer;
using App.DataAccess;
using App.Database.Provider;

namespace App.FormApp.Provider
{
    public partial class frmSelectProvider : DevComponents.DotNetBar.Office2007Form
    {
        List<tb_Provider> data = new List<tb_Provider>();
        private int size = 20;
        private int page = 1;

        public tb_Provider Value { get; private set; }

        public frmSelectProvider()
        {
            InitializeComponent();
            FormUtils.ConfigDialog(this);
            dgrList.AutoGenerateColumns = false;
            dgrList.DataSource = data;
            paging.pageChange += Paging_pageChange;
        }
        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void LoadData(int page, int size)
        {
            this.page = page;
            frmLoading.RunMethod<BaseReturnFunction<List<tb_Provider>>>(typeof(ProviderInfo), "Search", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    dgrList.DataSource = result.Data;
                    data = result.Data;
                    double totalPage = result.Total * 1.0 / size;
                    int _totalPage = (int)totalPage;
                    if (totalPage > _totalPage)
                        _totalPage++;
                    paging.setData(page, _totalPage, result.Total);
                }
                else
                {
                    data = new List<tb_Provider>();
                    dgrList.DataSource = data;
                    paging.setData(1, 1, 0);
                }
            }, this, txtSearch.Text, page, size, true);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void frmSelectCustomer_Load(object sender, EventArgs e)
        {
            LoadData(page, size);
        }

        private void dgrList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 3)
                {
                    this.Value = data[e.RowIndex];
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }
    }
}