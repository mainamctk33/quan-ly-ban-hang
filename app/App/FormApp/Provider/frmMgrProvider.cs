﻿using App.DataAccess;
using App.Database.Customer;
using App.Database.Provider;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App.FormApp.Provider
{
    public partial class frmMgrProvider : DevComponents.DotNetBar.Office2007Form
    {
        List<tb_Provider> data = new List<tb_Provider>();
        private int size = 20;
        private int page = 1;

        public frmMgrProvider()
        {
            InitializeComponent();
            dgrList.AutoGenerateColumns = false;
            dgrList.DataSource = data;
            LoadData(page, size);
            paging.pageChange += Paging_pageChange;
        }

        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void LoadData(int page, int size)
        {
            this.page = page;
            frmLoading.RunMethod<BaseReturnFunction<List<tb_Provider>>>(typeof(ProviderInfo), "Search2", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    dgrList.DataSource = result.Data;
                    data = result.Data;
                    double totalPage = result.Total * 1.0 / size;
                    int _totalPage = (int)totalPage;
                    if (totalPage > _totalPage)
                        _totalPage++;
                    paging.setData(page, _totalPage, result.Total);
                }
                else
                {
                    data = new List<tb_Provider>();
                    dgrList.DataSource = data;
                    paging.setData(1, 1, 0);
                }
            }, this, txtSearch.Text, page, size);
        }

        private void dgrList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < data.Count)
            {
                switch (e.ColumnIndex)
                {
                    case 5:
                        frmAddNewProvider frm = new frmAddNewProvider(data[e.RowIndex], (a) =>
                        {
                            ProgressClass.ShowStatus(this, "Chỉnh sửa thành công");
                            dgrList.DataSource = null;
                            data[e.RowIndex] = a;
                            dgrList.DataSource = data;
                        });
                        frm.ShowDialog();
                        break;
                    case 6:
                        var result = frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn xóa nhà cung cấp này không");
                        if (result)
                        {
                            var result2 = ProviderInfo.Delete(data[e.RowIndex].ID);
                            if (result2.IsTrue)
                            {
                                dgrList.DataSource = null;
                                data.RemoveAt(e.RowIndex);
                                dgrList.DataSource = data;
                                ProgressClass.ShowStatus(this, "Xóa nhà cung cấp thành công");
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog(result2.Message);
                            }
                        }

                        break;
                }

            }
        }

        internal void addNew(tb_Provider a)
        {
            dgrList.DataSource = null;
            data.Insert(0, a);
            dgrList.DataSource = data;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(1, size);
        }
    }
}
