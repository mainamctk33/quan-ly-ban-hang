﻿using App.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Database.Customer;
using App.Database.Provider;
using App.Utils;

namespace App.FormApp.Provider
{
    public delegate void delUpdate(tb_Provider value);
    public partial class frmAddNewProvider : DevComponents.DotNetBar.Office2007Form
    {
        private delUpdate _delUpdate;
        private tb_Provider tb_Provider;

        public frmAddNewProvider(delUpdate _delUpdate)
        {
            InitializeComponent();
            this._delUpdate = _delUpdate;
            FormUtils.ConfigDialog(this);
        }

        public frmAddNewProvider(tb_Provider tb_Provider, delUpdate _delUpdate) : this(_delUpdate)
        {
            this.tb_Provider = tb_Provider;
            this.Text = "Chỉnh sửa thông tin nhà cung cấp";
            this.btnOk.Text = "Lưu";
            this.txtName.Text = tb_Provider.Name;
            this.txtAddress.Text = tb_Provider.Address;
            this.txtPhone.Text = tb_Provider.Phone;
            this.txtNote.Text = tb_Provider.Note;
            this.chkActive.Checked = tb_Provider.Active;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (tb_Provider != null)
            {
                var result = ProviderInfo.Update(tb_Provider.ID, txtName.Text,txtNote.Text, txtAddress.Text, txtPhone.Text, chkActive.Checked);
                if(result.IsTrue)
                {
                    tb_Provider.Phone = txtPhone.Text;
                    tb_Provider.Active = chkActive.Checked;
                    tb_Provider.Name= txtName.Text;
                    tb_Provider.Note= txtNote.Text;
                    tb_Provider.Address = txtAddress.Text;
                    if (_delUpdate != null)
                        _delUpdate(tb_Provider);
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message, this);
                }
                return;
            }
            else
            {
                var result = ProviderInfo.Create(txtName.Text, txtAddress.Text, txtPhone.Text, txtNote.Text, chkActive.Checked);
                if (result.IsTrue)
                {
                    if (_delUpdate != null)
                        _delUpdate(result.Data);
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message, this);
                }
            }
        }
    }
}
