﻿using App.DataAccess;
using App.Database.Customer;
using App.FormApp.Customer;
using App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App.FormApp.Account
{
    public partial class frmMgrAccount : DevComponents.DotNetBar.Office2007Form
    {
        List<User> data = new List<User>();
        private int size = 20;
        private int page = 1;

        public frmMgrAccount()
        {
            InitializeComponent();
            dgrList.AutoGenerateColumns = false;
            dgrList.DataSource = data;
            LoadData(page, size);
            paging.pageChange += Paging_pageChange;
        }

        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void LoadData(int page, int size)
        {
            int role = 0;
            if (chkAdminAccount.Checked)
                role = role | (int)RoleInfo.Role.mgrAccount;
            if (chkAdminProduct.Checked)
                role = role | (int)RoleInfo.Role.mgrProduct;
            if (chkAdminProductType.Checked)
                role = role | (int)RoleInfo.Role.mgrProductType;
            if (chkAdminProvider.Checked)
                role = role | (int)RoleInfo.Role.mgrProvider;
            if (chkAdminSale.Checked)
                role = role | (int)RoleInfo.Role.allowSale;
            if (chkAdminCustomer.Checked)
                role = role | (int)RoleInfo.Role.mgrCustomer;
            this.page = page;
            frmLoading.RunMethod<BaseReturnFunction< List<User>>>(typeof(UserInfo),"Search",(result, ctrl) =>
            {
                                if (result.IsTrue)
                {
                    dgrList.DataSource = result.Data;
                    data = result.Data;
                    double totalPage = result.Total * 1.0 / size;
                    int _totalPage = (int)totalPage;
                    if (totalPage > _totalPage)
                        _totalPage++;
                    paging.setData(page, _totalPage, result.Total);
                }
                else
                {
                    data = new List<User>();
                    dgrList.DataSource = data;
                    paging.setData(1, 1, 0);
                }
            }, this, txtSearch.Text, page, size, role);
        }

        private void dgrList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < data.Count)
            {
                switch (e.ColumnIndex)
                {
                    case 9:
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn reset mật khẩu của tài khoản này", "Reset", "Hủy"))
                        {
                            var result2 = UserInfo.ResetPassword(data[e.RowIndex].UserName);
                            if (result2.IsTrue)
                            {
                                dgrList.DataSource = null;
                                dgrList.DataSource = data;
                                ProgressClass.ShowStatus(this, "Đã reset mật khẩu của người dùng này về \"123456\"");
                                frmDialog.ShowOkDialog("Thông báo", "Đã reset mật khẩu của người dùng này về \"123456\"", null);
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog(result2.Message);
                            }

                        }
                        break;
                    case 10:
                        frmAddNewAccount frm = new frmAddNewAccount(data[e.RowIndex], (a) =>
                        {
                            ProgressClass.ShowStatus(this, "Chỉnh sửa thành công");
                            dgrList.DataSource = null;
                            data[e.RowIndex] = a;
                            dgrList.DataSource = data;
                        });
                        frm.ShowDialog();
                        break;
                    case 11:
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn xóa tài khoản này", "Xóa", "Hủy"))
                        {
                            var result2 = UserInfo.Remove(data[e.RowIndex].UserName);
                            if (result2.IsTrue)
                            {
                                data.RemoveAt(e.RowIndex);
                                dgrList.DataSource = null;
                                dgrList.DataSource = data;
                                ProgressClass.ShowStatus(this, "Đã xóa thành công");
                                frmDialog.ShowOkDialog("Thông báo", "Đã xóa tài khoản thành công", null);
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog(result2.Message);
                            }

                        }
                        break;
                }

            }
        }

        internal void addNew(User a)
        {
            dgrList.DataSource = null;
            data.Insert(0, a);
            dgrList.DataSource = data;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void chkAdminAccount_CheckedChanged(object sender, EventArgs e)
        {
            LoadData(1, size);
        }
    }
}
