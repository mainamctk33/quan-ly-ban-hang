﻿using App.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Database.Customer;
using App.Models;
using App.Database.User;
using App.Utils;

namespace App.FormApp.Account
{
    public delegate void delUpdate(User value);
    public partial class frmAddNewAccount : DevComponents.DotNetBar.Office2007Form
    {
        private delUpdate _delUpdate;
        private User tb_User;

        public frmAddNewAccount(delUpdate _delUpdate)
        {
            InitializeComponent();
            this._delUpdate = _delUpdate;
            FormUtils.ConfigDialog(this);
        }

        public frmAddNewAccount(User tb_User, delUpdate _delUpdate) : this(_delUpdate)
        {
            this.tb_User = tb_User;
            this.Text = "Chỉnh sửa thông tin tài khoản";
            this.btnOk.Text = "Lưu";
            this.txtUserName.Text = tb_User.UserName;
            this.txtFullName.Text = tb_User.FullName;
            this.chkAdminAccount.Checked = tb_User.ACCOUNT;
            this.chkAdminCustomer.Checked = tb_User.CUSTOMER;
            this.chkAdminProvider.Checked = tb_User.PROVIDER;
            this.chkAdminProduct.Checked = tb_User.PRODUCT;
            this.chkAdminProductType.Checked = tb_User.PRODUCT_TYPE;
            this.chkAdminSale.Checked = tb_User.SALE;

            this.chkActive.Checked = tb_User.Active;
            this.txtUserName.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                frmDialog.ShowCancelDialog("Thông báo", "Vui lòng nhập tên tài khoản", this);
                return;
            }
            if (String.IsNullOrEmpty(txtFullName.Text))
            {
                frmDialog.ShowCancelDialog("Thông báo", "Vui lòng nhập họ và tên", this);
                return;
            }
            int role = 0;
            if (chkAdminAccount.Checked)
                role = role | (int)RoleInfo.Role.mgrAccount;
            if (chkAdminProduct.Checked)
                role = role | (int)RoleInfo.Role.mgrProduct;
            if (chkAdminProductType.Checked)
                role = role | (int)RoleInfo.Role.mgrProductType;
            if (chkAdminProvider.Checked)
                role = role | (int)RoleInfo.Role.mgrProvider;
            if (chkAdminSale.Checked)
                role = role | (int)RoleInfo.Role.allowSale;
            if (chkAdminCustomer.Checked)
                role = role | (int)RoleInfo.Role.mgrCustomer;

            if (tb_User != null)
            {
                if (tb_User.UserName == UserInfo.GetCurrentUser().UserName && !chkActive.Checked)
                {
                    frmDialog.ShowCancelDialog("Bạn không thể hủy kích hoạt tài khoản của chính mình", this);
                    return;
                }

                if (tb_User.UserName == UserInfo.GetCurrentUser().UserName && !chkAdminAccount.Checked)
                {
                    frmDialog.ShowCancelDialog("Bạn không thể hủy quyền quản lý tài khoản của chính mình", this);
                    return;
                }


                var result = UserInfo.Update(tb_User.UserName, txtFullName.Text, role, chkActive.Checked);
                if (result.IsTrue)
                {
                    if (UserInfo.GetCurrentUser().UserName == tb_User.UserName)
                    {
                        UserInfo.GetCurrentUser().Role = role;
                        UserInfo.GetCurrentUser().FullName = txtFullName.Text;
                        UserInfo.NotifiAccountChange();
                    }
                    if (_delUpdate != null)
                        _delUpdate(new User(result.Data));
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message, this);
                }
                return;
            }
            else
            {

                var result = UserInfo.Create(txtUserName.Text, txtFullName.Text, "123456", role, chkActive.Checked);
                if (result.IsTrue)
                {
                    if (_delUpdate != null)
                        _delUpdate(new User(result.Data));
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message, this);
                }
            }
        }

        private void chkAdminProvider_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
