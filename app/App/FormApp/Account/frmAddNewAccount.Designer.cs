﻿namespace App.FormApp.Account
{
    partial class frmAddNewAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.txtUserName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFullName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label2 = new System.Windows.Forms.Label();
            this.chkAdminCustomer = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkActive = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkAdminProvider = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.chkAdminProductType = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkAdminAccount = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkAdminProduct = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.chkAdminSale = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.groupPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Location = new System.Drawing.Point(229, 286);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(79, 23);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnOk.Location = new System.Drawing.Point(144, 286);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(79, 23);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "Thêm";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFullName.Border.Class = "TextBoxBorder";
            this.txtFullName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtUserName.DisabledBackColor = System.Drawing.Color.White;
            this.txtUserName.ForeColor = System.Drawing.Color.Black;
            this.txtUserName.Location = new System.Drawing.Point(15, 34);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.PreventEnterBeep = true;
            this.txtUserName.Size = new System.Drawing.Size(292, 20);
            this.txtUserName.TabIndex = 0;
            this.txtUserName.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(317, 1);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Tài khoản";
            // 
            // txtFullName
            // 
            this.txtFullName.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtFullName.Border.Class = "TextBoxBorder";
            this.txtFullName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtFullName.DisabledBackColor = System.Drawing.Color.White;
            this.txtFullName.ForeColor = System.Drawing.Color.Black;
            this.txtFullName.Location = new System.Drawing.Point(15, 81);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.PreventEnterBeep = true;
            this.txtFullName.Size = new System.Drawing.Size(292, 20);
            this.txtFullName.TabIndex = 1;
            this.txtFullName.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Họ tên";
            // 
            // chkAdminCustomer
            // 
            this.chkAdminCustomer.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkAdminCustomer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAdminCustomer.ForeColor = System.Drawing.Color.Black;
            this.chkAdminCustomer.Location = new System.Drawing.Point(3, 12);
            this.chkAdminCustomer.Name = "chkAdminCustomer";
            this.chkAdminCustomer.Size = new System.Drawing.Size(133, 23);
            this.chkAdminCustomer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAdminCustomer.TabIndex = 3;
            this.chkAdminCustomer.Text = "Quản lý khách hàng";
            // 
            // chkActive
            // 
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkActive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.CheckValue = "Y";
            this.chkActive.ForeColor = System.Drawing.Color.Black;
            this.chkActive.Location = new System.Drawing.Point(15, 257);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(133, 23);
            this.chkActive.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkActive.TabIndex = 4;
            this.chkActive.Text = "Kích hoạt";
            // 
            // chkAdminProvider
            // 
            this.chkAdminProvider.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkAdminProvider.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAdminProvider.ForeColor = System.Drawing.Color.Black;
            this.chkAdminProvider.Location = new System.Drawing.Point(3, 41);
            this.chkAdminProvider.Name = "chkAdminProvider";
            this.chkAdminProvider.Size = new System.Drawing.Size(133, 23);
            this.chkAdminProvider.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAdminProvider.TabIndex = 3;
            this.chkAdminProvider.Text = "Quản lý nhà cung cấp";
            this.chkAdminProvider.CheckedChanged += new System.EventHandler(this.chkAdminProvider_CheckedChanged);
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.chkAdminCustomer);
            this.groupPanel1.Controls.Add(this.chkAdminSale);
            this.groupPanel1.Controls.Add(this.chkAdminProduct);
            this.groupPanel1.Controls.Add(this.chkAdminAccount);
            this.groupPanel1.Controls.Add(this.chkAdminProductType);
            this.groupPanel1.Controls.Add(this.chkAdminProvider);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(15, 117);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(292, 122);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 13;
            this.groupPanel1.Text = "Quyền";
            // 
            // chkAdminProductType
            // 
            this.chkAdminProductType.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkAdminProductType.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAdminProductType.ForeColor = System.Drawing.Color.Black;
            this.chkAdminProductType.Location = new System.Drawing.Point(3, 70);
            this.chkAdminProductType.Name = "chkAdminProductType";
            this.chkAdminProductType.Size = new System.Drawing.Size(133, 23);
            this.chkAdminProductType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAdminProductType.TabIndex = 3;
            this.chkAdminProductType.Text = "Quản lý loại sản phẩm";
            // 
            // chkAdminAccount
            // 
            this.chkAdminAccount.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkAdminAccount.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAdminAccount.ForeColor = System.Drawing.Color.Black;
            this.chkAdminAccount.Location = new System.Drawing.Point(142, 12);
            this.chkAdminAccount.Name = "chkAdminAccount";
            this.chkAdminAccount.Size = new System.Drawing.Size(133, 23);
            this.chkAdminAccount.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAdminAccount.TabIndex = 3;
            this.chkAdminAccount.Text = "Quản lý tài khoản";
            // 
            // chkAdminProduct
            // 
            this.chkAdminProduct.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkAdminProduct.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAdminProduct.ForeColor = System.Drawing.Color.Black;
            this.chkAdminProduct.Location = new System.Drawing.Point(142, 41);
            this.chkAdminProduct.Name = "chkAdminProduct";
            this.chkAdminProduct.Size = new System.Drawing.Size(133, 23);
            this.chkAdminProduct.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAdminProduct.TabIndex = 3;
            this.chkAdminProduct.Text = "Quản lý Sản phẩm";
            // 
            // chkAdminSale
            // 
            this.chkAdminSale.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkAdminSale.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkAdminSale.ForeColor = System.Drawing.Color.Black;
            this.chkAdminSale.Location = new System.Drawing.Point(142, 70);
            this.chkAdminSale.Name = "chkAdminSale";
            this.chkAdminSale.Size = new System.Drawing.Size(133, 23);
            this.chkAdminSale.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkAdminSale.TabIndex = 3;
            this.chkAdminSale.Text = "Bán hàng";
            // 
            // frmAddNewAccount
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 320);
            this.ControlBox = false;
            this.Controls.Add(this.groupPanel1);
            this.Controls.Add(this.chkActive);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtFullName);
            this.Controls.Add(this.txtUserName);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmAddNewAccount";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thêm mới tài khoản";
            this.groupPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnOk;
        private DevComponents.DotNetBar.Controls.TextBoxX txtUserName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtFullName;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAdminCustomer;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkActive;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAdminProvider;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAdminSale;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAdminProduct;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAdminAccount;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkAdminProductType;
    }
}