﻿using App.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Database.Customer;
using App.Database.ProductType;
using App.Utils;

namespace App.FormApp.ProductType
{
    public delegate void delUpdate(tb_ProductType value);
    public partial class frmAddNewProductType : DevComponents.DotNetBar.Office2007Form
    {
        private delUpdate _delUpdate;
        private tb_ProductType tb_ProductType;

        public frmAddNewProductType(delUpdate _delUpdate)
        {
            InitializeComponent();
            this._delUpdate = _delUpdate;
            FormUtils.ConfigDialog(this);
        }

        public frmAddNewProductType(tb_ProductType tb_ProductType, delUpdate _delUpdate) : this(_delUpdate)
        {
            this.tb_ProductType = tb_ProductType;
            this.Text = "Chỉnh sửa thông tin loại sản phẩm";
            this.btnOk.Text = "Lưu";
            this.txtName.Text = tb_ProductType.Name;
            this.chkActive.Checked = tb_ProductType.Active;
            FormUtils.ConfigDialog(this);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (tb_ProductType != null)
            {
                frmLoading.RunMethod<BaseReturnFunction<tb_ProductType>>(typeof(ProductTypeInfo), "Update", (result, ctrl) =>
                {
                    if (result.IsTrue)
                    {
                        tb_ProductType.Active = chkActive.Checked;
                        tb_ProductType.Name = txtName.Text;
                        if (_delUpdate != null)
                            _delUpdate(tb_ProductType);
                        this.Close();
                    }
                    else
                    {
                        frmDialog.ShowCancelDialog(result.Message, this);
                    }

                }, this, tb_ProductType.ID, txtName.Text, chkActive.Checked);
            }
            else
            {
                frmLoading.RunMethod<BaseReturnFunction<tb_ProductType>>(typeof(ProductTypeInfo), "Create", (result, ctrl) =>
                {
                    if (result.IsTrue)
                    {
                        if (_delUpdate != null)
                            _delUpdate(result.Data);
                        this.Close();
                    }
                    else
                    {
                        frmDialog.ShowCancelDialog(result.Message, this);
                    }

                }, this, txtName.Text, chkActive.Checked);
            }
        }
    }
}
