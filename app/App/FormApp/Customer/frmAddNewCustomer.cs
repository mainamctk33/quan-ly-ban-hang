﻿using App.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Database.Customer;
using App.Utils;

namespace App.FormApp.Customer
{
    public delegate void delUpdate(tb_Customer value);
    public partial class frmAddNewCustomer : DevComponents.DotNetBar.Office2007Form
    {
        private delUpdate _delUpdate;
        private tb_Customer tb_Customer;

        public frmAddNewCustomer(delUpdate _delUpdate)
        {
            InitializeComponent();
            this._delUpdate = _delUpdate;
            FormUtils.ConfigDialog(this);
        }

        public frmAddNewCustomer(tb_Customer tb_Customer, delUpdate _delUpdate) : this(_delUpdate)
        {
            this.tb_Customer = tb_Customer;
            this.Text = "Chỉnh sửa thông tin khách hàng";
            this.btnOk.Text = "Lưu";
            this.txtFullName.Text = tb_Customer.FullName;
            this.txtAddress.Text = tb_Customer.Address;
            this.txtPhone.Text = tb_Customer.Phone;
            this.chkActive.Checked = tb_Customer.Active;
            this.chkIsFamiliar.Checked = tb_Customer.isFamiliar;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtFullName.Text))
            {
                frmDialog.ShowCancelDialog("Thông báo", "Vui lòng nhập tên khách hàng", this);
                return;
            }
            if (tb_Customer != null)
            {
                var result = CustomerInfo.Update(tb_Customer.ID, txtFullName.Text, "", txtAddress.Text, txtPhone.Text, chkActive.Checked, chkIsFamiliar.Checked);
                if(result.IsTrue)
                {
                    tb_Customer.Phone = txtPhone.Text;
                    tb_Customer.isFamiliar = chkIsFamiliar.Checked;
                    tb_Customer.Active = chkActive.Checked;
                    tb_Customer.FullName = txtFullName.Text;
                    tb_Customer.Address = txtAddress.Text;
                    if (_delUpdate != null)
                        _delUpdate(tb_Customer);
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message, this);
                }
                return;
            }
            else
            {
                var result = CustomerInfo.Create(txtFullName.Text, txtAddress.Text, txtPhone.Text, chkActive.Checked, chkIsFamiliar.Checked);
                if (result.IsTrue)
                {
                    if (_delUpdate != null)
                        _delUpdate(result.Data);
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message, this);
                }
            }
        }
    }
}
