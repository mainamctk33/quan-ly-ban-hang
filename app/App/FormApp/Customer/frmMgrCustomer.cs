﻿using App.DataAccess;
using App.Database.Customer;
using App.FormApp.Customer;
using App.FormApp.DebtBook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App.FormApp.Customer
{
    public partial class frmMgrCustomer : DevComponents.DotNetBar.Office2007Form
    {
        List<tb_Customer> data = new List<tb_Customer>();
        private int size = 20;
        private int page = 1;

        public frmMgrCustomer()
        {
            InitializeComponent();
            dgrList.AutoGenerateColumns = false;
            dgrList.DataSource = data;
            LoadData(page, size);
            paging.pageChange += Paging_pageChange;
        }

        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void LoadData(int page, int size)
        {
            this.page = page;
            frmLoading.RunMethod<BaseReturnFunction<List<tb_Customer>>>(typeof(CustomerInfo), "Search2", (result, ctrl) =>
              {
                  if (result.IsTrue)
                  {
                      dgrList.DataSource = result.Data;
                      data = result.Data;
                      double totalPage = result.Total * 1.0 / size;
                      int _totalPage = (int)totalPage;
                      if (totalPage > _totalPage)
                          _totalPage++;
                      paging.setData(page, _totalPage, result.Total);
                  }
                  else
                  {
                      data = new List<tb_Customer>();
                      dgrList.DataSource = data;
                      paging.setData(1, 1, 0);
                  }
              }, this, txtSearch.Text, page, size);
        }

        private void dgrList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < data.Count)
            {
                switch (e.ColumnIndex)
                {
                    case 5:
                        frmAddNewCustomer frm = new frmAddNewCustomer(data[e.RowIndex], (a) =>
                        {
                            ProgressClass.ShowStatus(this, "Chỉnh sửa thành công");
                            dgrList.DataSource = null;
                            data[e.RowIndex] = a;
                            dgrList.DataSource = data;
                        });
                        frm.ShowDialog();
                        break;
                    case 6:
                        var result = frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn xóa khách hàng này không");
                        if (result)
                        {
                            var result2 = CustomerInfo.Delete(data[e.RowIndex].ID);
                            if (result2.IsTrue)
                            {
                                dgrList.DataSource = null;
                                data.RemoveAt(e.RowIndex);
                                dgrList.DataSource = data;
                                ProgressClass.ShowStatus(this, "Xóa khách hàng thành công");
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog(result2.Message);
                            }
                        }

                        break;
                    case 7:
                        frmDanhSachHoaDon ds = new frmDanhSachHoaDon(data[e.RowIndex]);
                        ds.ShowDialog();
                        LoadData(page, size);
                        break;
                }

            }
        }

        internal void addNew(tb_Customer a)
        {
            dgrList.DataSource = null;
            data.Insert(0, a);
            dgrList.DataSource = data;
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        internal void Reload()
        {
            LoadData(1, size);
        }
    }
}
