﻿using App.Common;
using App.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App.FormApp.Setting
{
    public partial class frmConfig : DevComponents.DotNetBar.Office2007Form
    {
        private _del1Param del1Param;

        public frmConfig(_del1Param del1Param)
        {
            InitializeComponent();
            FormUtils.ConfigDialog(this);
            this.del1Param = del1Param;
            String server = RegistryUtils.GetStringValue(Constants.SERVER);
            String database = RegistryUtils.GetStringValue(Constants.DATABASE);
            String password = RegistryUtils.GetStringValue(Constants.PASSWORD);
            String username = RegistryUtils.GetStringValue(Constants.USERNAME);

            txtUserName.Text = username;
            txtDatabase.Text = database;
            txtPassword.Text = password;
            txtServer.Text = server;
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtServer.Text))
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập địa chỉ server", this);
                return;
            }
            if (String.IsNullOrWhiteSpace(txtDatabase.Text))
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập database", this);
                return;
            }
            String userName = txtUserName.Text;
            if (String.IsNullOrWhiteSpace(userName))
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập username", this);
                return;
            }
            RegistryUtils.SetValue(Constants.SERVER, txtServer.Text);
            RegistryUtils.SetValue(Constants.DATABASE, txtDatabase.Text);
            RegistryUtils.SetValue(Constants.PASSWORD, txtPassword.Text);
            RegistryUtils.SetValue(Constants.USERNAME, txtUserName.Text);
            frmDialog.ShowOkDialog("Đã cấu hình xong, bạn cần khởi động lại ứng dụng", this);
            if (this.del1Param != null)
                this.del1Param(null);
            this.Close();
        }
    }
}
