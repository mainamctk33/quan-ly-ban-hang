﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using App.DataAccess;
using App.Models;
using App.Utils;
using App.Database.Customer;
using App.Database.Provider;

namespace App.FormApp.Money.Loan
{
    public partial class frmDanhSachHoaDon : DevComponents.DotNetBar.Office2007Form
    {
        private tb_Provider provider;

        List<App.Models.Debit> data = new List<App.Models.Debit>();
        private int size = 20;
        private int page = 1;
        public frmDanhSachHoaDon(tb_Provider provider)
        {
            this.provider = provider;

            InitializeComponent();
            dgrList.AutoGenerateColumns = false;
            dgrList.DataSource = data;
            ucPaging1.pageChange += Paging_pageChange;
            FormUtils.ConfigDialog(this);
            this.ControlBox = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            lbName.Text = provider.Name;
            lbAddress.Text = provider.Address;
            lbPhone.Text = provider.Phone;


        }

        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void LoadData(int page, int size)
        {
            this.page = page;
            frmLoading.RunMethod<BaseReturnFunction<List<Debit>>>(typeof(DebtInfo), "GetByProvider", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    dgrList.DataSource = result.Data;
                    data = result.Data;
                    double totalPage = result.Total * 1.0 / size;
                    int _totalPage = (int)totalPage;
                    if (totalPage > _totalPage)
                        _totalPage++;
                    ucPaging1.setData(page, _totalPage, result.Total);
                }
                else
                {
                    data = new List<App.Models.Debit>();
                    dgrList.DataSource = data;
                    ucPaging1.setData(1, 1, 0);
                }

            }, this, this.provider.ID, txtKeyword.Text, page, size);
        }

        private void frmDanhSachGiaoDich_Load(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void txtKeyword_KeyDown(object sender, KeyEventArgs e)
        {

        }


        private void dgrList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (data.Count > 0 && e.RowIndex >= 0 && e.RowIndex < data.Count)
            {
                var item = data[e.RowIndex];
                switch (e.ColumnIndex)
                {
                    case 5:
                        frmLoading.RunMethod<BaseReturnFunction<DetailBill>>(typeof(DebtInfo), "GetDetailBill", (result, ctrl) =>
                        {
                            if (result.IsTrue)
                            {
                                frmDanhSachGiaoDich frm = new frmDanhSachGiaoDich(result.Data);
                                frm.ShowDialog();
                                LoadData(page, size);
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog(result.Message);
                                ProgressClass.ShowStatus(this, result.Message);
                            }
                        }, this, false, item.BillId);
                        break;
                    case 6:
                        if (item.Money - item.Payment == 0)
                        {
                            frmDialog.ShowCancelDialog("Hóa đơn này đã được thanh toán hết");
                            return;
                        }
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn thanh toán hết nợ cho hóa đơn này"))
                        {
                            frmLoading.RunMethod<BaseReturnFunction<Boolean>>(typeof(DebtInfo), "PaymentBill", (result, ctrl) =>
                            {
                                if (result.IsTrue)
                                {
                                    LoadData(page, size);
                                    ProgressClass.ShowStatus(this, "Thanh toán nợ cho hóa đơn này thành công");
                                }
                                else
                                {
                                    frmDialog.ShowCancelDialog(result.Message);
                                    ProgressClass.ShowStatus(this, result.Message);
                                }
                            }, this, UserInfo.GetCurrentUser().UserName, item.BillId, "Thanh toán hết tiền nợ của hóa đơn");
                        }
                        break;
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(page, size);
        }

        private void txtKeyword_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void dgrList_CellMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (data.Count > 0 && e.RowIndex >= 0 && e.RowIndex < data.Count)
            {
                var item = data[e.RowIndex];
                switch (e.ColumnIndex)
                {
                    case 5:
                        frmLoading.RunMethod<BaseReturnFunction<DetailBill>>(typeof(DebtInfo), "GetDetailBill", (result, ctrl) =>
                        {
                            if (result.IsTrue)
                            {
                                frmDanhSachGiaoDich frm = new frmDanhSachGiaoDich(result.Data);
                                frm.ShowDialog();
                                LoadData(page, size);
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog(result.Message);
                                ProgressClass.ShowStatus(this, result.Message);
                            }
                        }, this, false,item.BillId);
                        break;
                    case 6:
                        if (item.Money - item.Payment == 0)
                        {
                            frmDialog.ShowCancelDialog("Hóa đơn này đã được thanh toán hết");
                            return;
                        }
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn thanh toán hết nợ cho hóa đơn này"))
                        {
                            frmLoading.RunMethod<BaseReturnFunction<Boolean>>(typeof(DebtInfo), "PaymentBill", (result, ctrl) =>
                            {
                                if (result.IsTrue)
                                {
                                    LoadData(page, size);
                                    ProgressClass.ShowStatus(this, "Thanh toán nợ cho hóa đơn này thành công");
                                }
                                else
                                {
                                    frmDialog.ShowCancelDialog(result.Message);
                                    ProgressClass.ShowStatus(this, result.Message);
                                }
                            }, this, UserInfo.GetCurrentUser().UserName, item.BillId, "Thanh toán hết tiền nợ của hóa đơn");
                        }
                        break;
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            frmThemKhoanNo frm = new frmThemKhoanNo(provider);
            frm.ShowDialog();
            LoadData(page, size);
        }
    }
}
