﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Models;
using App.DataAccess;
using App.Database.Pay;
using App.Database.Customer;
using App.FormApp.Customer;
using App.FormApp.Provider;

namespace App.FormApp.Money.Loan
{
    public partial class frmThemKhoanNo : DevComponents.DotNetBar.OfficeForm
    {
        private List<Models.Product> list;
        private Database.Provider.tb_Provider provider;

        public frmThemKhoanNo(Database.Provider.tb_Provider provider) : this()
        {
            this.provider = provider;
            btnSelectProvider.Enabled = false;
            txtFullName.Enabled = false;
            txtFullName.Text = provider.Name;
            txtPhone.Text = provider.Phone;
            txtAddress.Text = provider.Address;

        }

        public frmThemKhoanNo()
        {
            InitializeComponent();
            dgrList2.AutoGenerateColumns = false;
            list = new List<Models.Product>();
            //lbTotal.Text = sum.ToString("N0") + "đ";
            lbTotal.Text = list.Sum(x => x.ActualPrice).ToString("N0") + "đ";
            txtQuantity.Value = 1;
            txtPrice.Value = 1000;
        }

        private void txtPrice_ValueChanged(object sender, EventArgs e)
        {
            txtSum.Value = txtPrice.Value * txtQuantity.Value;
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            if (list.Count == 0)
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập sản phẩm cho phiếu nợ");
                return;
            }
            String fullname = txtFullName.Text;
            String phone = txtPhone.Text;
            String address = txtAddress.Text;
            String note = txtDesciption.Text;
            if (provider != null)
            {
                payment(provider, note);
            }
            else
            {
                var result = ProviderInfo.Create(fullname, address, phone,note, false);
                if (result.IsTrue)
                {
                    payment(result.Data, note);
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message);
                }
            }
        }

        private void payment(Database.Customer.tb_Customer customer, String note)
        {
            using (var context = new PayDbDataContext())
            {
                var bill = new tb_Bill()
                {
                    CreatedBy = UserInfo.GetCurrentUser().UserName,
                    CreatedDate = DateTime.Now,
                    CustomerId = customer.ID,
                    Note = note,
                    Price = list.Sum(x => x.Sum),
                    UpdatedDate = DateTime.Now,
                    Name = customer.FullName,
                    Address = customer.Address,
                    Phone = customer.Phone
                };
                context.tb_Bills.InsertOnSubmit(bill);
                foreach (var item in list)
                {
                    var billItem = new tb_BillItem()
                    {
                        tb_Bill = bill,
                        BillId = bill.ID,
                        ProductName = item.Name,
                        ProductId = item.ProductCode,
                        Quantily = item.QuantityInStock,
                        Sum = item.Sum,
                        Price = item.ActualPrice,
                    };
                    context.tb_BillItems.InsertOnSubmit(billItem);
                }
                context.SubmitChanges();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        private void payment(Database.Provider.tb_Provider provider, String note)
        {
            using (var context = new PayDbDataContext())
            {
                var bill = new tb_Bill()
                {
                    CreatedBy = UserInfo.GetCurrentUser().UserName,
                    CreatedDate = DateTime.Now,
                    ProviderId = provider.ID,
                    Note = note,
                    Price = list.Sum(x => x.Sum),
                    UpdatedDate = DateTime.Now,
                    Name = provider.Name,
                    Address = provider.Address,
                    Phone = provider.Phone
                };
                context.tb_Bills.InsertOnSubmit(bill);
                foreach (var item in list)
                {
                    var billItem = new tb_BillItem()
                    {
                        tb_Bill = bill,
                        BillId = bill.ID,
                        ProductName = item.Name,
                        ProductId = item.ProductCode,
                        Quantily = item.QuantityInStock,
                        Sum = item.Sum,
                        Price = item.ActualPrice,
                    };
                    context.tb_BillItems.InsertOnSubmit(billItem);
                }
                context.SubmitChanges();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnSelectProvider_Click(object sender, EventArgs e)
        {
            if (btnSelectProvider.Text == "Clear")
            {
                txtFullName.Enabled = true;
                btnSelectProvider.Text = "Chọn";
                provider = null;
                txtFullName.Text = "";
                txtPhone.Text = "";
                txtAddress.Text = "";
            }
            else
            {
                frmSelectProvider dialog = new frmSelectProvider();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.provider = dialog.Value;
                    txtFullName.Enabled = false;
                    btnSelectProvider.Text = "Clear";
                    txtFullName.Text = provider.Name;
                    txtPhone.Text = provider.Phone;
                    txtAddress.Text = provider.Address;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txtName.Text))
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập tên sản phẩm");
                return;
            }
            list.Add(new Models.Product() { Name = txtName.Text, ProductCode = txtCode.Text, Price = txtPrice.Value, QuantityInStock = txtQuantity.Value, ActualPrice = txtPrice.Value });
            dgrList2.DataSource = null;
            dgrList2.DataSource = list;
            lbTotal.Text = list.Sum(x => x.Sum).ToString("N0") + "đ";
            txtName.Text = "";
            txtPrice.Value = 1000;
            txtQuantity.Value = 1;
            txtCode.Text = "";

        }

        private void dgrList2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5 && e.RowIndex >= 0 && e.RowIndex < list.Count)
            {
                dgrList2.DataSource = null;
                list.RemoveAt(e.RowIndex);
                dgrList2.DataSource = list;
            }
        }
    }
}
