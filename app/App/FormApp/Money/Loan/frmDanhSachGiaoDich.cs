﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using App.DataAccess;
using App.Models;
using App.Utils;
using App.Database.Customer;
using App.Database.Pay;
using System.Linq;
using App.FormApp.DebtBook;

namespace App.FormApp.Money.Loan
{
    public partial class frmDanhSachGiaoDich : DevComponents.DotNetBar.Office2007Form
    {

        List<App.Models.Debit> data = new List<App.Models.Debit>();
        private int size = 20;
        private int page = 1;
        private DetailBill detailBill;
        private Database.Pay.tb_Provider provider;

        public frmDanhSachGiaoDich(DetailBill detailBill)
        {
            this.detailBill = detailBill;
            this.provider = detailBill.tb_Provider;

            InitializeComponent();
            FormUtils.FixStyleDataGridView(dgrList);
            FormUtils.FixStyleDataGridView(dgrList2);
            FormUtils.ConfigDialog(this);
            this.ControlBox = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            lbName.Text = provider.Name;
            lbAddress.Text = provider.Address;
            lbPhone.Text = provider.Phone;
            ShowDetailBill();
        }

        private void dgrList2_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0 && e.RowIndex < detailBill.listPayments.Count)
                {
                    if (e.ColumnIndex == 4)
                    {
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn xóa thanh toán này. Dữ liệu sẽ mất vĩnh viễn nếu nhấn OK"))
                        {
                            var item = detailBill.listPayments[e.RowIndex];
                            frmLoading.RunMethod<BaseReturnFunction<Boolean>>(typeof(DebtInfo), "DeletePayment", (result, ctrl) =>
                            {
                                if (result.IsTrue)
                                {
                                    dgrList2.DataSource = null;
                                    detailBill.listPayments.RemoveAt(e.RowIndex);
                                    ShowDetailBill();
                                    ProgressClass.ShowStatus(this, "Xóa thanh toán thành công");
                                }
                                else
                                {
                                    frmDialog.ShowCancelDialog(result.Message);
                                    ProgressClass.ShowStatus(this, result.Message);
                                }
                            }, this, item.ID);
                        }
                    }
                    if (e.ColumnIndex == 5)
                    {
                        frmChinhSuaThanhToan frm = new frmChinhSuaThanhToan(detailBill.listPayments[e.RowIndex], provider);
                        frm.ShowDialog();
                        ShowDetailBill();
                    }
                }
            }
            catch (Exception e2)
            {

            }
        }

        private void ShowDetailBill()
        {
            lbTongTien.Text = detailBill.tb_Bill.Price.ToString("N0") + "đ";
            double sum = detailBill.listPayments.Count > 0 ? detailBill.listPayments.Sum(x => x.Payment) : 0;
            lbDaTra.Text = sum.ToString("N0") + "đ";
            lbConLai.Text = (detailBill.tb_Bill.Price - sum).ToString("N0") + "đ";
            dgrList.DataSource = null;
            dgrList2.DataSource = null;
            dgrList.DataSource = detailBill.listProducts;
            dgrList2.DataSource = detailBill.listPayments;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            frmThemThanhToan frm = new frmThemThanhToan(detailBill.tb_Bill.ID, detailBill.tb_Bill.Price, detailBill.listPayments.Sum(x => x.Payment), detailBill.listPayments);
            frm.ShowDialog();
            dgrList2.DataSource = null;
            ShowDetailBill();
        }
    }
}
