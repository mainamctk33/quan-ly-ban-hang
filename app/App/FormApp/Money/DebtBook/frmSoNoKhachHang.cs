﻿using App.DataAccess;
using App.Database.Customer;
using App.Models;
using App.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App.FormApp.DebtBook
{
    public partial class frmSoNoKhachHang : DevComponents.DotNetBar.Office2007Form
    {
        List<App.Models.Debit> data = new List<App.Models.Debit>();
        private int size = 20;
        private int page = 1;
        public frmSoNoKhachHang()
        {
            InitializeComponent();

            dgrList.AutoGenerateColumns = false;
            dgrList.DataSource = data;
            ucPaging1.pageChange += Paging_pageChange;
            FormUtils.FixStyleDataGridView(dgrList);
        }

        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void LoadData(int page, int size)
        {
            this.page = page;
            frmLoading.RunMethod<BaseReturnFunction<List<Debit>>>(typeof(DebtInfo), "Search", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    dgrList.DataSource = result.Data;
                    data = result.Data;
                    double totalPage = result.Total * 1.0 / size;
                    int _totalPage = (int)totalPage;
                    if (totalPage > _totalPage)
                        _totalPage++;
                    ucPaging1.setData(page, _totalPage, result.Total);
                }
                else
                {
                    data = new List<App.Models.Debit>();
                    dgrList.DataSource = data;
                    ucPaging1.setData(1, 1, 0);
                }

            }, this, txtKeyword.Text, page, size);
        }

        private void frmSoNoKhachHang_Load(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void txtKeyword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                LoadData(1, size);

        }

        private void btnSearch(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void dgrList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Debit debit;
            if (e.RowIndex != -1)
            {
                switch (e.ColumnIndex)
                {
                    case 5:
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn thanh toán hết nợ cho khách hàng này", null))
                        {
                            debit = data[e.RowIndex];
                            frmLoading.RunMethod<BaseReturnFunction<Boolean>>(typeof(DebtInfo), "CustomerPaymentAll", (result, ctrl) =>
                            {
                                if (result.IsTrue)
                                {
                                    LoadData(1, size);
                                }
                            }, this, UserInfo.GetCurrentUser().UserName, debit.ID, "Thanh toán nợ ngày " + DateTime.Now.ToString("dd/MM/yyyy"));
                        }
                        break;
                    case 6:
                        debit = data[e.RowIndex];

                        frmLoading.RunMethod<BaseReturnFunction<tb_Customer>>(typeof(CustomerInfo), "GetById1", (result, ctrl) =>
                        {
                            if (result.IsTrue)
                            {
                                frmDanhSachHoaDon ds = new frmDanhSachHoaDon(result.Data);
                                ds.ShowDialog();
                                LoadData(page, size);
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog("Không tìm thấy khách hàng này");
                                ProgressClass.ShowStatus(this, "Không tìm thấy khách hàng này");
                            }
                        }, this, false, debit.ID);
                        break;
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            frmThemKhoanNo frm = new frmThemKhoanNo();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                btnSearch1.PerformClick();
            }

        }
    }
}
