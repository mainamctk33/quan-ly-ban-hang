﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Models;
using App.DataAccess;
using App.Database.Pay;
using App.Database.Customer;
using App.FormApp.Customer;

namespace App.FormApp.DebtBook
{
    public partial class frmThemThanhToan : DevComponents.DotNetBar.OfficeForm
    {
        private List<Models.Product> list;
        private int billId;
        private Database.Customer.tb_Customer customer;
        private List<Debit> listPayments;

        public frmThemThanhToan(int billId, double money, double payment, List<Debit> listPayments)
        {
            InitializeComponent();
            this.billId = billId;
            dbSoTien.MaxValue = money - payment;
            lbTongTien.Text = money.ToString("N0") + " đ";
            lbDaTra.Text = payment.ToString("N0") + " đ";
            this.listPayments = listPayments;
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            if (dbSoTien.Value == 0)
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập số tiền cần thanh toán");
                ProgressClass.ShowStatus(this, "Vui lòng nhập số tiền cần thanh toán");
                return;
            }
            if (String.IsNullOrWhiteSpace(txtDesciption.Text))
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập nội dung thanh toán");
                ProgressClass.ShowStatus(this, "Vui lòng nhập nội dung thanh toán");
                return;
            }
            frmLoading.RunMethod<BaseReturnFunction<Debit>>(typeof(DebtInfo), "AddPayment", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    listPayments.Add(result.Data);
                    ProgressClass.ShowStatus(this, "Thêm thanh toán thành công");
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message);
                    ProgressClass.ShowStatus(this, result.Message);
                }
            }, this, UserInfo.GetCurrentUser().UserName, billId, dbSoTien.Value, txtDesciption.Text);
        }

    }
}
