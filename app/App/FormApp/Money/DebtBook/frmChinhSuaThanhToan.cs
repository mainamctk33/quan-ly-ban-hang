﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Models;
using App.DataAccess;
using App.Database.Pay;
using App.Database.Customer;
using App.FormApp.Customer;

namespace App.FormApp.DebtBook
{
    public partial class frmChinhSuaThanhToan : DevComponents.DotNetBar.OfficeForm
    {
        private List<Models.Product> list;
        private int billId;
        private Database.Pay.tb_Customer customer;
        private Debit debit;

        public frmChinhSuaThanhToan(Debit debit, Database.Pay.tb_Customer customer)
        {
            InitializeComponent();
            this.debit = debit;
            this.customer = customer;
            lbName.Text = customer.FullName;
            lbAddress.Text = customer.Address;
            lbPhone.Text = customer.Phone;
            chkThanThiet.Checked = customer.isFamiliar;
            lbNguoiLap.Text = debit.UserFullName;
            dtpCreatedDate.Value = debit.CreatedDate;
            lbSoTien.Text = debit.MoneyString;
            dbSoTien.Value = debit.Money;
            txtDesciption.Text = debit.Note;
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            if (dbSoTien.Value == 0)
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập số tiền cần thanh toán");
                ProgressClass.ShowStatus(this, "Vui lòng nhập số tiền cần thanh toán");
                return;
            }
            if (String.IsNullOrWhiteSpace(txtDesciption.Text))
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập nội dung thanh toán");
                ProgressClass.ShowStatus(this, "Vui lòng nhập nội dung thanh toán");
                return;
            }
            frmLoading.RunMethod<BaseReturnFunction<Debit>>(typeof(DebtInfo), "EditPayment", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    ProgressClass.ShowStatus(this, result.Message);
                    debit.Money = debit.Payment= dbSoTien.Value;
                    debit.Note = txtDesciption.Text;
                    debit.CreatedDate = dtpCreatedDate.Value;
                    debit.UserFullName = UserInfo.GetCurrentUser().FullName;
                    this.Close();
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message);
                    ProgressClass.ShowStatus(this, result.Message);
                }
            }, this, debit.ID, UserInfo.GetCurrentUser().UserName, dtpCreatedDate.Value, dbSoTien.Value, txtDesciption.Text);
        }

    }
}
