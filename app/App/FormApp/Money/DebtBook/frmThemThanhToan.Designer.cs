﻿namespace App.FormApp.DebtBook
{
    partial class frmThemThanhToan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.lbConLai = new System.Windows.Forms.Label();
            this.lbDaTra = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTongTien = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDesciption = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnPayment = new DevComponents.DotNetBar.ButtonX();
            this.dbSoTien = new DevComponents.Editors.DoubleInput();
            this.groupPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbSoTien)).BeginInit();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2013;
            this.groupPanel1.Controls.Add(this.dbSoTien);
            this.groupPanel1.Controls.Add(this.lbConLai);
            this.groupPanel1.Controls.Add(this.lbDaTra);
            this.groupPanel1.Controls.Add(this.label5);
            this.groupPanel1.Controls.Add(this.label6);
            this.groupPanel1.Controls.Add(this.label3);
            this.groupPanel1.Controls.Add(this.label2);
            this.groupPanel1.Controls.Add(this.lbTongTien);
            this.groupPanel1.Controls.Add(this.label1);
            this.groupPanel1.Controls.Add(this.label4);
            this.groupPanel1.Controls.Add(this.txtDesciption);
            this.groupPanel1.DisabledBackColor = System.Drawing.Color.Empty;
            this.groupPanel1.Location = new System.Drawing.Point(12, 12);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(435, 189);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.groupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.groupPanel1.TabIndex = 21;
            this.groupPanel1.Text = "Thông tin thanh toán";
            // 
            // lbConLai
            // 
            this.lbConLai.AutoSize = true;
            this.lbConLai.BackColor = System.Drawing.Color.Transparent;
            this.lbConLai.ForeColor = System.Drawing.Color.Black;
            this.lbConLai.Location = new System.Drawing.Point(64, 57);
            this.lbConLai.Name = "lbConLai";
            this.lbConLai.Size = new System.Drawing.Size(45, 13);
            this.lbConLai.TabIndex = 16;
            this.lbConLai.Text = "lbDaTra";
            // 
            // lbDaTra
            // 
            this.lbDaTra.AutoSize = true;
            this.lbDaTra.BackColor = System.Drawing.Color.Transparent;
            this.lbDaTra.ForeColor = System.Drawing.Color.Black;
            this.lbDaTra.Location = new System.Drawing.Point(64, 34);
            this.lbDaTra.Name = "lbDaTra";
            this.lbDaTra.Size = new System.Drawing.Size(45, 13);
            this.lbDaTra.TabIndex = 16;
            this.lbDaTra.Text = "lbDaTra";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Số tiền thanh toán:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(210, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "đ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(3, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Còn lại:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Đã trả:";
            // 
            // lbTongTien
            // 
            this.lbTongTien.AutoSize = true;
            this.lbTongTien.BackColor = System.Drawing.Color.Transparent;
            this.lbTongTien.ForeColor = System.Drawing.Color.Black;
            this.lbTongTien.Location = new System.Drawing.Point(64, 12);
            this.lbTongTien.Name = "lbTongTien";
            this.lbTongTien.Size = new System.Drawing.Size(55, 13);
            this.lbTongTien.TabIndex = 16;
            this.lbTongTien.Text = "Tổng tiền:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Tổng tiền:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Nội dung thanh toán";
            // 
            // txtDesciption
            // 
            this.txtDesciption.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtDesciption.Border.Class = "TextBoxBorder";
            this.txtDesciption.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtDesciption.DisabledBackColor = System.Drawing.Color.White;
            this.txtDesciption.ForeColor = System.Drawing.Color.Black;
            this.txtDesciption.Location = new System.Drawing.Point(6, 135);
            this.txtDesciption.Name = "txtDesciption";
            this.txtDesciption.PreventEnterBeep = true;
            this.txtDesciption.Size = new System.Drawing.Size(417, 20);
            this.txtDesciption.TabIndex = 15;
            this.txtDesciption.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // btnPayment
            // 
            this.btnPayment.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnPayment.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnPayment.Image = global::App.Properties.Resources.invoice;
            this.btnPayment.ImageFixedSize = new System.Drawing.Size(20, 20);
            this.btnPayment.Location = new System.Drawing.Point(336, 207);
            this.btnPayment.Name = "btnPayment";
            this.btnPayment.Size = new System.Drawing.Size(111, 36);
            this.btnPayment.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnPayment.TabIndex = 2;
            this.btnPayment.Text = "Tạo thanh toán";
            this.btnPayment.Click += new System.EventHandler(this.btnPayment_Click);
            // 
            // dbSoTien
            // 
            // 
            // 
            // 
            this.dbSoTien.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dbSoTien.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dbSoTien.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.dbSoTien.DisplayFormat = "N0";
            this.dbSoTien.Increment = 1D;
            this.dbSoTien.Location = new System.Drawing.Point(106, 81);
            this.dbSoTien.Name = "dbSoTien";
            this.dbSoTien.ShowUpDown = true;
            this.dbSoTien.Size = new System.Drawing.Size(98, 20);
            this.dbSoTien.TabIndex = 20;
            // 
            // frmThemThanhToan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 250);
            this.Controls.Add(this.btnPayment);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(473, 289);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(473, 289);
            this.Name = "frmThemThanhToan";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "THÊM THANH TOÁN";
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dbSoTien)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.ButtonX btnPayment;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtDesciption;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private System.Windows.Forms.Label lbConLai;
        private System.Windows.Forms.Label lbDaTra;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbTongTien;
        private System.Windows.Forms.Label label1;
        private DevComponents.Editors.DoubleInput dbSoTien;
    }
}