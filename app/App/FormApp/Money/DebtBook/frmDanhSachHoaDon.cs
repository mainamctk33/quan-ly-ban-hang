﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using App.DataAccess;
using App.Models;
using App.Utils;
using App.Database.Customer;

namespace App.FormApp.DebtBook
{
    public partial class frmDanhSachHoaDon : DevComponents.DotNetBar.Office2007Form
    {
        private tb_Customer customer;

        List<App.Models.Debit> data = new List<App.Models.Debit>();
        private int size = 20;
        private int page = 1;
        public frmDanhSachHoaDon(tb_Customer customer)
        {
            this.customer = customer;

            InitializeComponent();
            dgrList.AutoGenerateColumns = false;
            dgrList.DataSource = data;
            ucPaging1.pageChange += Paging_pageChange;
            FormUtils.ConfigDialog(this);
            this.ControlBox = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            lbName.Text = customer.FullName;
            lbAddress.Text = customer.Address;
            lbPhone.Text = customer.Phone;
            chkThanThiet.Checked = customer.isFamiliar;


        }

        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void LoadData(int page, int size)
        {
            this.page = page;
            frmLoading.RunMethod<BaseReturnFunction<List<Debit>>>(typeof(DebtInfo), "GetByCustomer", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    dgrList.DataSource = result.Data;
                    data = result.Data;
                    double totalPage = result.Total * 1.0 / size;
                    int _totalPage = (int)totalPage;
                    if (totalPage > _totalPage)
                        _totalPage++;
                    ucPaging1.setData(page, _totalPage, result.Total);
                }
                else
                {
                    data = new List<App.Models.Debit>();
                    dgrList.DataSource = data;
                    ucPaging1.setData(1, 1, 0);
                }

            }, this, this.customer.ID, txtKeyword.Text, page, size);
        }

        private void frmDanhSachGiaoDich_Load(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            LoadData(1, size);
        }

        private void txtKeyword_KeyDown(object sender, KeyEventArgs e)
        {

        }


        private void dgrList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Debit debit;
            if (e.RowIndex != -1)
            {
                switch (e.ColumnIndex)
                {
                    case 5:
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn thanh toán hết nợ cho khách hàng này", null))
                        {
                            debit = data[e.RowIndex];
                            frmLoading.RunMethod<BaseReturnFunction<Boolean>>(typeof(DebtInfo), "CustomerPaymentAll", (result, ctrl) =>
                            {
                                if (result.IsTrue)
                                {
                                    LoadData(1, size);
                                }
                            }, this, UserInfo.GetCurrentUser().UserName, debit.ID, "Thanh toán nợ ngày " + DateTime.Now.ToString("dd/MM/yyyy"));
                        }
                        break;
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(page, size);
        }

        private void txtKeyword_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSearch.PerformClick();
            }
        }

        private void dgrList_CellMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (data.Count > 0 && e.RowIndex >= 0 && e.RowIndex < data.Count)
            {
                var item = data[e.RowIndex];
                switch (e.ColumnIndex)
                {
                    case 5:
                        frmLoading.RunMethod<BaseReturnFunction<DetailBill>>(typeof(DebtInfo), "GetDetailBill", (result, ctrl) =>
                        {
                            if (result.IsTrue)
                            {
                                frmDanhSachGiaoDich frm = new frmDanhSachGiaoDich(result.Data);
                                frm.ShowDialog();
                                LoadData(page, size);
                            }
                            else
                            {
                                frmDialog.ShowCancelDialog(result.Message);
                                ProgressClass.ShowStatus(this, result.Message);
                            }
                        }, this, false,item.BillId);
                        break;
                    case 6:
                        if (item.Money - item.Payment == 0)
                        {
                            frmDialog.ShowCancelDialog("Hóa đơn này đã được thanh toán hết");
                            return;
                        }
                        if (frmDialog.ShowOkCancelDialog("Xác nhận", "Bạn có muốn thanh toán hết nợ cho hóa đơn này"))
                        {
                            frmLoading.RunMethod<BaseReturnFunction<Boolean>>(typeof(DebtInfo), "PaymentBill", (result, ctrl) =>
                            {
                                if (result.IsTrue)
                                {
                                    LoadData(page, size);
                                    ProgressClass.ShowStatus(this, "Thanh toán nợ cho hóa đơn này thành công");
                                }
                                else
                                {
                                    frmDialog.ShowCancelDialog(result.Message);
                                    ProgressClass.ShowStatus(this, result.Message);
                                }
                            }, this, UserInfo.GetCurrentUser().UserName, item.BillId, "Thanh toán hết tiền nợ của hóa đơn");
                        }
                        break;
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            frmThemKhoanNo frm = new frmThemKhoanNo(customer);
            frm.ShowDialog();
            LoadData(page, size);
        }
    }
}
