﻿using App.DataAccess;
using App.Database.Customer;
using App.FormApp.Customer;
using App.Models;
using App.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App.FormApp.About
{
    public partial class frmAbout : DevComponents.DotNetBar.Office2007Form
    {
        List<User> data = new List<User>();
        private int size = 20;
        private int page = 1;

        public frmAbout()
        {
            InitializeComponent();
            FormUtils.ConfigDialog(this);
            this.ControlBox = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }        
    }
}
