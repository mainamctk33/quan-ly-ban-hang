﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Models;
using App.DataAccess;
using App.Database.Pay;
using App.Database.Customer;
using App.FormApp.Customer;

namespace App.FormApp.Sell
{
    public partial class frmPayment : DevComponents.DotNetBar.OfficeForm
    {
        private List<Models.Product> list;
        private double sum;
        private double debt;
        private double _return;
        private Database.Customer.tb_Customer customer;

        public frmPayment(List<Models.Product> list)
        {
            this.list = list;
            InitializeComponent();
            dgrList2.AutoGenerateColumns = false;
            dgrList2.DataSource = list;
            sum = list.Sum(x => x.Sum);
            lbTotal.Text = sum.ToString("N0") + "đ";
            debt = sum > dbMoney.Value ? sum - dbMoney.Value : 0;
            lbDebt.Text = debt.ToString("N0") + " đ";
            _return = dbMoney.Value > sum ? (dbMoney.Value - sum) : 0;
            lbReturn.Text = _return.ToString("N0") + " đ";
        }

        private void dbMoney_ValueChanged(object sender, EventArgs e)
        {
            double debt = sum > dbMoney.Value ? sum - dbMoney.Value : 0;
            lbDebt.Text = debt.ToString("N0") + " đ";
            double _return = dbMoney.Value > sum ? (dbMoney.Value - sum) : 0;
            lbReturn.Text = _return.ToString("N0") + " đ";
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            String fullname = txtFullName.Text;
            String phone = txtPhone.Text;
            String address = txtAddress.Text;
            String note = txtDesciption.Text;
            var money = dbMoney.Value > sum ? sum : dbMoney.Value;
            if (customer != null)
            {
                payment(customer, money, note);
            }
            else
            {
                var result = CustomerInfo.Create(fullname, address, phone, true, false);
                if (result.IsTrue)
                {
                    payment(result.Data, money, note);
                }
                else
                {
                    frmDialog.ShowCancelDialog(result.Message);
                }
            }
        }

        private void payment(Database.Customer.tb_Customer customer, double money, String note)
        {
            using (var context = new PayDbDataContext())
            {
                var bill = new tb_Bill()
                {
                    CreatedBy = UserInfo.GetCurrentUser().UserName,
                    CreatedDate = DateTime.Now,
                    CustomerId = customer.ID,
                    Note = note,
                    Price = sum,
                    UpdatedDate = DateTime.Now,
                    Name = customer.FullName,
                    Address = customer.Address,
                    Phone = customer.Phone
                };
                context.tb_Bills.InsertOnSubmit(bill);
                foreach (var item in list)
                {
                    var billItem = new tb_BillItem()
                    {
                        tb_Bill = bill,
                        BillId = bill.ID,
                        ProductName = item.Name,
                        ProductId = item.ProductCode,
                        Quantily = item.QuantityInStock,
                        Sum = item.Sum,
                        Price = item.ActualPrice,
                    };
                    context.tb_BillItems.InsertOnSubmit(billItem);
                }
                var pay = new tb_Pay()
                {
                    BillId = bill.ID,
                    tb_Bill = bill,
                    CreatedBy = UserInfo.GetCurrentUser().UserName,
                    Note = note,
                    Value = money,
                    CreatedDate = DateTime.Now
                };
                context.tb_Pays.InsertOnSubmit(pay);
                context.SubmitChanges();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnSelectCustomer_Click(object sender, EventArgs e)
        {
            if (btnSelectCustomer.Text == "Clear")
            {
                txtFullName.Enabled = true;
                btnSelectCustomer.Text = "Chọn";
                customer = null;
                txtFullName.Text = "";
                txtPhone.Text = "";
                txtAddress.Text = "";
            }
            else
            {
                frmSelectCustomer dialog = new frmSelectCustomer();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.customer = dialog.Value;
                    txtFullName.Enabled = false;
                    btnSelectCustomer.Text = "Clear";
                    txtFullName.Text = customer.FullName;
                    txtPhone.Text = customer.Phone;
                    txtAddress.Text = customer.Address;
                }
            }
        }
    }
}
