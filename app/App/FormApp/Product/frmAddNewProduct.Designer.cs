﻿namespace App.FormApp.Product
{
    partial class frmAddNewProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.txtName = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.chkActive = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cbProductType = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lb1 = new System.Windows.Forms.Label();
            this.btnAddProductType = new DevComponents.DotNetBar.ButtonX();
            this.label2 = new System.Windows.Forms.Label();
            this.cbProvider = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.btnAddProvider = new DevComponents.DotNetBar.ButtonX();
            this.dbPrice = new DevComponents.Editors.DoubleInput();
            this.label3 = new System.Windows.Forms.Label();
            this.grPromotion = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.dtpApplyTo = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.dtpApplyFrom = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dbNewPrice = new DevComponents.Editors.DoubleInput();
            this.chkApplyNewPrice = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.numQuantityInStock = new DevComponents.Editors.IntegerInput();
            this.label7 = new System.Windows.Forms.Label();
            this.rtDescription = new DevComponents.DotNetBar.Controls.RichTextBoxEx();
            this.label8 = new System.Windows.Forms.Label();
            this.txtProductCode = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxEx1 = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem0 = new DevComponents.Editors.ComboItem();
            ((System.ComponentModel.ISupportInitialize)(this.dbPrice)).BeginInit();
            this.grPromotion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpApplyTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpApplyFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbNewPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantityInStock)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.Location = new System.Drawing.Point(517, 273);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(79, 23);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnOk.Location = new System.Drawing.Point(432, 273);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(79, 23);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "Thêm";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtName.Border.Class = "TextBoxBorder";
            this.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtName.DisabledBackColor = System.Drawing.Color.White;
            this.txtName.ForeColor = System.Drawing.Color.Black;
            this.txtName.Location = new System.Drawing.Point(321, 39);
            this.txtName.Name = "txtName";
            this.txtName.PreventEnterBeep = true;
            this.txtName.Size = new System.Drawing.Size(277, 20);
            this.txtName.TabIndex = 0;
            this.txtName.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(613, 1);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(318, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Tên sản phẩm";
            // 
            // chkActive
            // 
            this.chkActive.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkActive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.CheckValue = "Y";
            this.chkActive.ForeColor = System.Drawing.Color.Black;
            this.chkActive.Location = new System.Drawing.Point(15, 249);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(133, 23);
            this.chkActive.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkActive.TabIndex = 7;
            this.chkActive.Text = "Hiển thị";
            // 
            // cbProductType
            // 
            this.cbProductType.DisplayMember = "Text";
            this.cbProductType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProductType.ForeColor = System.Drawing.Color.Black;
            this.cbProductType.FormattingEnabled = true;
            this.cbProductType.ItemHeight = 15;
            this.cbProductType.Location = new System.Drawing.Point(15, 86);
            this.cbProductType.Name = "cbProductType";
            this.cbProductType.Size = new System.Drawing.Size(251, 21);
            this.cbProductType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbProductType.TabIndex = 1;
            // 
            // lb1
            // 
            this.lb1.AutoSize = true;
            this.lb1.BackColor = System.Drawing.Color.Transparent;
            this.lb1.ForeColor = System.Drawing.Color.Black;
            this.lb1.Location = new System.Drawing.Point(12, 70);
            this.lb1.Name = "lb1";
            this.lb1.Size = new System.Drawing.Size(76, 13);
            this.lb1.TabIndex = 12;
            this.lb1.Text = "Loại sản phẩm";
            // 
            // btnAddProductType
            // 
            this.btnAddProductType.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddProductType.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddProductType.Location = new System.Drawing.Point(272, 87);
            this.btnAddProductType.Name = "btnAddProductType";
            this.btnAddProductType.Size = new System.Drawing.Size(20, 20);
            this.btnAddProductType.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddProductType.TabIndex = 2;
            this.btnAddProductType.Text = "+";
            this.btnAddProductType.Click += new System.EventHandler(this.btnAddProductType_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(318, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Nhà cung cấp";
            // 
            // cbProvider
            // 
            this.cbProvider.DisplayMember = "Text";
            this.cbProvider.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProvider.ForeColor = System.Drawing.Color.Black;
            this.cbProvider.FormattingEnabled = true;
            this.cbProvider.ItemHeight = 15;
            this.cbProvider.Location = new System.Drawing.Point(321, 86);
            this.cbProvider.Name = "cbProvider";
            this.cbProvider.Size = new System.Drawing.Size(251, 21);
            this.cbProvider.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbProvider.TabIndex = 3;
            // 
            // btnAddProvider
            // 
            this.btnAddProvider.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddProvider.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnAddProvider.Location = new System.Drawing.Point(578, 86);
            this.btnAddProvider.Name = "btnAddProvider";
            this.btnAddProvider.Size = new System.Drawing.Size(20, 20);
            this.btnAddProvider.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnAddProvider.TabIndex = 4;
            this.btnAddProvider.Text = "+";
            this.btnAddProvider.Click += new System.EventHandler(this.btnAddProvider_Click);
            // 
            // dbPrice
            // 
            this.dbPrice.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.dbPrice.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dbPrice.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dbPrice.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.dbPrice.DisplayFormat = "N0";
            this.dbPrice.ForeColor = System.Drawing.Color.Black;
            this.dbPrice.Increment = 1000D;
            this.dbPrice.Location = new System.Drawing.Point(15, 135);
            this.dbPrice.Name = "dbPrice";
            this.dbPrice.ShowUpDown = true;
            this.dbPrice.Size = new System.Drawing.Size(131, 20);
            this.dbPrice.TabIndex = 5;
            this.dbPrice.ValueChanged += new System.EventHandler(this.dbPrice_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Giá";
            // 
            // grPromotion
            // 
            this.grPromotion.BackColor = System.Drawing.Color.Transparent;
            this.grPromotion.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2013;
            this.grPromotion.Controls.Add(this.dtpApplyTo);
            this.grPromotion.Controls.Add(this.dtpApplyFrom);
            this.grPromotion.Controls.Add(this.label6);
            this.grPromotion.Controls.Add(this.label5);
            this.grPromotion.Controls.Add(this.label4);
            this.grPromotion.Controls.Add(this.chkApplyNewPrice);
            this.grPromotion.DisabledBackColor = System.Drawing.Color.Empty;
            this.grPromotion.IsShadowEnabled = true;
            this.grPromotion.Location = new System.Drawing.Point(177, 5);
            this.grPromotion.Name = "grPromotion";
            this.grPromotion.Size = new System.Drawing.Size(117, 28);
            // 
            // 
            // 
            this.grPromotion.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grPromotion.Style.BackColorGradientAngle = 90;
            this.grPromotion.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grPromotion.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grPromotion.Style.BorderBottomWidth = 1;
            this.grPromotion.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grPromotion.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grPromotion.Style.BorderLeftWidth = 1;
            this.grPromotion.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grPromotion.Style.BorderRightWidth = 1;
            this.grPromotion.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grPromotion.Style.BorderTopWidth = 1;
            this.grPromotion.Style.CornerDiameter = 4;
            this.grPromotion.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grPromotion.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grPromotion.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grPromotion.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grPromotion.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grPromotion.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grPromotion.TabIndex = 8;
            this.grPromotion.Text = "Thông tin khuyến mại";
            this.grPromotion.Visible = false;
            // 
            // dtpApplyTo
            // 
            this.dtpApplyTo.AllowEmptyState = false;
            this.dtpApplyTo.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.dtpApplyTo.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpApplyTo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyTo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpApplyTo.ButtonDropDown.Visible = true;
            this.dtpApplyTo.CustomFormat = "dd/MM/yyyy";
            this.dtpApplyTo.Enabled = false;
            this.dtpApplyTo.ForeColor = System.Drawing.Color.Black;
            this.dtpApplyTo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpApplyTo.IsInputReadOnly = true;
            this.dtpApplyTo.IsPopupCalendarOpen = false;
            this.dtpApplyTo.Location = new System.Drawing.Point(6, 138);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtpApplyTo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyTo.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtpApplyTo.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpApplyTo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpApplyTo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpApplyTo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpApplyTo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpApplyTo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpApplyTo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpApplyTo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyTo.MonthCalendar.DisplayMonth = new System.DateTime(2018, 6, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtpApplyTo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpApplyTo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpApplyTo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpApplyTo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyTo.MonthCalendar.TodayButtonVisible = true;
            this.dtpApplyTo.Name = "dtpApplyTo";
            this.dtpApplyTo.Size = new System.Drawing.Size(277, 20);
            this.dtpApplyTo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpApplyTo.TabIndex = 2;
            this.dtpApplyTo.Value = new System.DateTime(2018, 6, 29, 22, 37, 40, 191);
            // 
            // dtpApplyFrom
            // 
            this.dtpApplyFrom.AllowEmptyState = false;
            this.dtpApplyFrom.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.dtpApplyFrom.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpApplyFrom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyFrom.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpApplyFrom.ButtonDropDown.Visible = true;
            this.dtpApplyFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpApplyFrom.Enabled = false;
            this.dtpApplyFrom.ForeColor = System.Drawing.Color.Black;
            this.dtpApplyFrom.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpApplyFrom.IsInputReadOnly = true;
            this.dtpApplyFrom.IsPopupCalendarOpen = false;
            this.dtpApplyFrom.Location = new System.Drawing.Point(6, 89);
            // 
            // 
            // 
            // 
            // 
            // 
            this.dtpApplyFrom.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyFrom.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 1);
            this.dtpApplyFrom.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpApplyFrom.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpApplyFrom.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpApplyFrom.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpApplyFrom.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpApplyFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpApplyFrom.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpApplyFrom.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyFrom.MonthCalendar.DisplayMonth = new System.DateTime(2018, 6, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtpApplyFrom.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpApplyFrom.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpApplyFrom.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpApplyFrom.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpApplyFrom.MonthCalendar.TodayButtonVisible = true;
            this.dtpApplyFrom.Name = "dtpApplyFrom";
            this.dtpApplyFrom.Size = new System.Drawing.Size(277, 20);
            this.dtpApplyFrom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpApplyFrom.TabIndex = 1;
            this.dtpApplyFrom.Value = new System.DateTime(2018, 6, 29, 22, 37, 40, 257);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(3, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Đến ngày";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Áp dụng từ ngày";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(3, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Giá mới";
            // 
            // dbNewPrice
            // 
            this.dbNewPrice.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.dbNewPrice.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dbNewPrice.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dbNewPrice.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.dbNewPrice.DisplayFormat = "N0";
            this.dbNewPrice.Enabled = false;
            this.dbNewPrice.ForeColor = System.Drawing.Color.Black;
            this.dbNewPrice.Increment = 1000D;
            this.dbNewPrice.Location = new System.Drawing.Point(470, 135);
            this.dbNewPrice.Name = "dbNewPrice";
            this.dbNewPrice.ShowUpDown = true;
            this.dbNewPrice.Size = new System.Drawing.Size(128, 20);
            this.dbNewPrice.TabIndex = 0;
            // 
            // chkApplyNewPrice
            // 
            this.chkApplyNewPrice.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.chkApplyNewPrice.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.chkApplyNewPrice.ForeColor = System.Drawing.Color.Black;
            this.chkApplyNewPrice.Location = new System.Drawing.Point(141, 112);
            this.chkApplyNewPrice.Name = "chkApplyNewPrice";
            this.chkApplyNewPrice.Size = new System.Drawing.Size(122, 23);
            this.chkApplyNewPrice.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.chkApplyNewPrice.TabIndex = 9;
            this.chkApplyNewPrice.Text = "Áp dụng giá mới";
            this.chkApplyNewPrice.CheckedChanged += new System.EventHandler(this.chkApplyNewPrice_CheckedChanged);
            // 
            // numQuantityInStock
            // 
            this.numQuantityInStock.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.numQuantityInStock.BackgroundStyle.Class = "DateTimeInputBackground";
            this.numQuantityInStock.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.numQuantityInStock.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2;
            this.numQuantityInStock.ForeColor = System.Drawing.Color.Black;
            this.numQuantityInStock.Location = new System.Drawing.Point(154, 135);
            this.numQuantityInStock.Name = "numQuantityInStock";
            this.numQuantityInStock.ShowUpDown = true;
            this.numQuantityInStock.Size = new System.Drawing.Size(140, 20);
            this.numQuantityInStock.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(152, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Còn lại";
            // 
            // rtDescription
            // 
            // 
            // 
            // 
            this.rtDescription.BackgroundStyle.Class = "RichTextBoxBorder";
            this.rtDescription.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rtDescription.ForeColor = System.Drawing.Color.Beige;
            this.rtDescription.Location = new System.Drawing.Point(15, 184);
            this.rtDescription.Name = "rtDescription";
            this.rtDescription.Rtf = "{\\rtf1\\ansi\\ansicpg932\\deff0\\deflang1033\\deflangfe1041{\\fonttbl{\\f0\\fnil\\fcharset" +
    "0 Microsoft Sans Serif;}}\r\n{\\colortbl ;\\red245\\green245\\blue220;}\r\n\\viewkind4\\uc" +
    "1\\pard\\cf1\\lang1041\\f0\\fs17\\par\r\n}\r\n";
            this.rtDescription.Size = new System.Drawing.Size(583, 49);
            this.rtDescription.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(12, 168);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Mô tả";
            // 
            // txtProductCode
            // 
            this.txtProductCode.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtProductCode.Border.Class = "TextBoxBorder";
            this.txtProductCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtProductCode.DisabledBackColor = System.Drawing.Color.White;
            this.txtProductCode.ForeColor = System.Drawing.Color.Black;
            this.txtProductCode.Location = new System.Drawing.Point(15, 39);
            this.txtProductCode.Name = "txtProductCode";
            this.txtProductCode.PreventEnterBeep = true;
            this.txtProductCode.Size = new System.Drawing.Size(277, 20);
            this.txtProductCode.TabIndex = 0;
            this.txtProductCode.WatermarkBehavior = DevComponents.DotNetBar.eWatermarkBehavior.HideNonEmpty;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(12, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Mã sản phẩm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(318, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "Giảm giá";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(467, 119);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Giá mới";
            // 
            // comboBoxEx1
            // 
            this.comboBoxEx1.DisplayMember = "Text";
            this.comboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxEx1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEx1.ForeColor = System.Drawing.Color.Black;
            this.comboBoxEx1.FormattingEnabled = true;
            this.comboBoxEx1.ItemHeight = 15;
            this.comboBoxEx1.Items.AddRange(new object[] {
            this.comboItem0,
            this.comboItem1,
            this.comboItem2,
            this.comboItem3,
            this.comboItem4,
            this.comboItem5,
            this.comboItem6});
            this.comboBoxEx1.Location = new System.Drawing.Point(321, 134);
            this.comboBoxEx1.Name = "comboBoxEx1";
            this.comboBoxEx1.Size = new System.Drawing.Size(143, 21);
            this.comboBoxEx1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.comboBoxEx1.TabIndex = 3;
            this.comboBoxEx1.SelectedIndexChanged += new System.EventHandler(this.comboBoxEx1_SelectedIndexChanged);
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "5 %";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "10 %";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "15 %";
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "20 %";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "25 %";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "30 %";
            // 
            // comboItem0
            // 
            this.comboItem0.Text = "0 %";
            // 
            // frmAddNewProduct
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 302);
            this.ControlBox = false;
            this.Controls.Add(this.rtDescription);
            this.Controls.Add(this.numQuantityInStock);
            this.Controls.Add(this.grPromotion);
            this.Controls.Add(this.dbPrice);
            this.Controls.Add(this.btnAddProvider);
            this.Controls.Add(this.dbNewPrice);
            this.Controls.Add(this.btnAddProductType);
            this.Controls.Add(this.comboBoxEx1);
            this.Controls.Add(this.cbProvider);
            this.Controls.Add(this.cbProductType);
            this.Controls.Add(this.chkActive);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lb1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtProductCode);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtName);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmAddNewProduct";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thêm mới sản phẩm";
            this.Load += new System.EventHandler(this.frmAddNewProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dbPrice)).EndInit();
            this.grPromotion.ResumeLayout(false);
            this.grPromotion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpApplyTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpApplyFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbNewPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQuantityInStock)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnOk;
        private DevComponents.DotNetBar.Controls.TextBoxX txtName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkActive;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbProductType;
        private System.Windows.Forms.Label lb1;
        private DevComponents.DotNetBar.ButtonX btnAddProductType;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbProvider;
        private DevComponents.DotNetBar.ButtonX btnAddProvider;
        private DevComponents.Editors.DoubleInput dbPrice;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.GroupPanel grPromotion;
        private DevComponents.Editors.DoubleInput dbNewPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpApplyTo;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpApplyFrom;
        private System.Windows.Forms.Label label6;
        private DevComponents.Editors.IntegerInput numQuantityInStock;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.Controls.CheckBoxX chkApplyNewPrice;
        private DevComponents.DotNetBar.Controls.RichTextBoxEx rtDescription;
        private System.Windows.Forms.Label label8;
        private DevComponents.DotNetBar.Controls.TextBoxX txtProductCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private DevComponents.DotNetBar.Controls.ComboBoxEx comboBoxEx1;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem0;
    }
}