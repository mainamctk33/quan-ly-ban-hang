﻿using App.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Database.Customer;
using App.Database.ProductType;
using System.Dynamic;
using App.Database.Product;
using App.FormApp.Provider;
using App.FormApp.ProductType;
using App.Utils;
using System.Reflection;

namespace App.FormApp.Product
{
    public delegate void delUpdate(App.Models.Product value);

    public partial class frmAddNewProduct : DevComponents.DotNetBar.Office2007Form
    {
        List<App.Database.ProductType.tb_ProductType> listProductType = new List<Database.ProductType.tb_ProductType>();
        List<App.Database.Provider.tb_Provider> listProvider = new List<Database.Provider.tb_Provider>();

        private delUpdate _delUpdate;
        private App.Models.Product tb_Product;
        private int productTypeId;
        private int providerId;
        List<int> pers = new List<int>() { 0, 5, 10, 15, 20, 25, 40 };
        public frmAddNewProduct(delUpdate _delUpdate)
        {
            InitializeComponent();
            this._delUpdate = _delUpdate;
            FormUtils.ConfigDialog(this);
        }

        public frmAddNewProduct(App.Models.Product product, delUpdate _delUpdate) : this(_delUpdate)
        {
            txtProductCode.Text = product.ProductCode;
            txtProductCode.Enabled = false;
            this.tb_Product = product;
            this.Text = "Chỉnh sửa thông tin sản phẩm";
            this.btnOk.Text = "Lưu";
            this.txtName.Text = product.Name;
            this.chkActive.Checked = product.Active;
            this.dbPrice.Value = product.Price;
            //chkApplyNewPrice.Checked = product.ApplyNewPrice;
            this.productTypeId = product.ProductTypeId;
            this.providerId = product.ProviderId;
            this.numQuantityInStock.Value = product.QuantityInStock;

            if (!product.NewPrice.HasValue)
            {
                comboBoxEx1.SelectedIndex = 1;
            }
            else
            {
                var value = product.NewPrice.Value;
                var per = 100- value * 100 / product.Price;
                int i = 0;
                for (i = 0; i < pers.Count; i++)
                {
                    if (pers[i] <= per)
                    {
                        comboBoxEx1.SelectedIndex = i;
                    }
                }
            }

            //if (!product.ApplyNewPrice)
            //{
            //    dbNewPrice.Text = "";
            //}
            //else
            //{
            //    dbNewPrice.Value = product.NewPrice.HasValue ? product.NewPrice.Value : 0;
            //}
            //this.dtpApplyFrom.Value = product.NewPriceFromDate.HasValue ? product.NewPriceFromDate.Value : DateTime.Now;
            //this.dtpApplyTo.Value = product.NewPriceToDate.HasValue ? product.NewPriceToDate.Value : DateTime.Now;
            rtDescription.Text = product.Description;
            FormUtils.ConfigDialog(this);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtProductCode.Text == "")
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập mã sản phẩm", this);
                return;
            }

            if (dbPrice.Text == "")
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập giá sản phẩm", this);
                return;
            }
            if (numQuantityInStock.Text == "")
            {
                frmDialog.ShowCancelDialog("Số sản phẩm còn trong kho", this);
                return;
            }
            if (cbProductType.SelectedValue == null)
            {
                frmDialog.ShowCancelDialog("Vui lòng chọn loại sản phẩm", this);
                return;
            }
            if (cbProvider.SelectedValue == null)
            {
                frmDialog.ShowCancelDialog("Vui lòng chọn nhà cung cấp", this);
                return;
            }

            if (tb_Product != null)
            {
                frmLoading.RunMethod<BaseReturnFunction<App.Models.Product>>(typeof(ProductInfo), "Update", (result, ctrl) =>
                 {
                     if (result.IsTrue)
                     {
                         if (_delUpdate != null)
                             _delUpdate(result.Data);
                         this.Close();
                     }
                     else
                     {
                         frmDialog.ShowCancelDialog(result.Message, this);
                     }
                 }, this, tb_Product.ProductCode, UserInfo.GetCurrentUser().UserName, txtName.Text, ConvertUtils.ToInt(cbProductType.SelectedValue), ConvertUtils.ToInt(cbProvider.SelectedValue), numQuantityInStock.Value, dbPrice.Value, dbNewPrice.Value, DateTime.Now, DateTime.MaxValue, true, rtDescription.Text, chkActive.Checked);
            }
            else
            {
                frmLoading.RunMethod<BaseReturnFunction<App.Models.Product>>(typeof(ProductInfo), "Create", (result, ctrl) =>
                {
                    if (result.IsTrue)
                    {
                        if (_delUpdate != null)
                            _delUpdate(result.Data);
                        this.Close();
                    }
                    else
                    {
                        frmDialog.ShowCancelDialog(result.Message, this);
                    }
                }, this, txtProductCode.Text, UserInfo.GetCurrentUser().UserName, txtName.Text, ConvertUtils.ToInt(cbProductType.SelectedValue), ConvertUtils.ToInt(cbProvider.SelectedValue), numQuantityInStock.Value, dbPrice.Value, dbNewPrice.Value, DateTime.Now, DateTime.MaxValue, true, rtDescription.Text, chkActive.Checked);
            }
        }

        private void frmAddNewProduct_Load(object sender, EventArgs e)
        {
            frmLoading.Run(() =>
            {
                dynamic c = new ExpandoObject();
                var a = ProductTypeInfo.GetAll();
                if (a.IsTrue)
                    c.ProductType = a.Data;
                else
                    c.ProductType = new List<App.Database.ProductType.tb_ProductType>();
                var b = ProviderInfo.GetAll();
                if (b.IsTrue)
                    c.Provider = b.Data;
                else
                    c.Provider = new List<App.Database.Provider.tb_Provider>();
                return c;
            }, (a, b) =>
            {
                if (a != null)
                {
                    listProductType = a.ProductType;
                    listProvider = a.Provider;
                    setDataSourceProductType(listProductType);
                    setDataSourceProviderType(listProvider);
                    cbProductType.SelectedValue = productTypeId;
                    cbProvider.SelectedValue = providerId;
                }

            }, this);
        }
        private void setDataSourceProductType(Object data)
        {
            cbProductType.DataSource = null;
            cbProductType.DataSource = data;
            cbProductType.DisplayMember = "Name";
            cbProductType.ValueMember = "ID";
        }
        private void setDataSourceProviderType(Object data)
        {
            cbProvider.DataSource = null;
            cbProvider.DataSource = data;
            cbProvider.DisplayMember = "Name";
            cbProvider.ValueMember = "ID";
        }

        private void btnAddProvider_Click(object sender, EventArgs e)
        {
            frmAddNewProvider frm = new frmAddNewProvider((a) =>
            {
                if (a.Active)
                {
                    listProvider.Insert(0, a);
                    setDataSourceProviderType(listProvider);
                }
                ProgressClass.ShowStatus(this, "Thêm mới nhà cung cấp thành công");
                //if (_frmProvider != null)
                //{
                //    _frmProvider.addNew(a);
                //}
            });
            frm.ShowDialog();
        }

        private void btnAddProductType_Click(object sender, EventArgs e)
        {
            frmAddNewProductType frm = new frmAddNewProductType((a) =>
            {
                if (a.Active)
                {
                    listProductType.Insert(0, a);
                    setDataSourceProductType(listProductType);
                }
                ProgressClass.ShowStatus(this, "Thêm mới loại sản phẩm thành công");
                //if (_frmMgrProductType != null)
                //{
                //    _frmMgrProductType.addNew(a);
                //}
            });
            frm.ShowDialog();
        }

        private void chkApplyNewPrice_CheckedChanged(object sender, EventArgs e)
        {
            dbNewPrice.Enabled = chkApplyNewPrice.Checked;
            dtpApplyFrom.Enabled = chkApplyNewPrice.Checked;
            dtpApplyTo.Enabled = chkApplyNewPrice.Checked;
        }

        private void dbPrice_ValueChanged(object sender, EventArgs e)
        {
            calculatorNewPrice();
        }

        private void comboBoxEx1_SelectedIndexChanged(object sender, EventArgs e)
        {
            calculatorNewPrice();
        }

        void calculatorNewPrice()
        {
            int value = 0;
            try
            {

                value = pers[comboBoxEx1.SelectedIndex];
            }
            catch (Exception)
            {
                value = 0;
            }
            dbNewPrice.Value = (100 - value) * dbPrice.Value / 100;
        }
    }
}
