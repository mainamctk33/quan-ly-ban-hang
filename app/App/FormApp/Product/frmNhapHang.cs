﻿using App.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Models;
using System.Web.Script.Serialization;
using System.Threading;
using App.Database.Provider;

namespace App.FormApp.Product
{
    public partial class frmNhapHang : DevComponents.DotNetBar.OfficeForm
    {
        public int page = 1;
        private List<Models.Product> data = new List<Models.Product>();
        private readonly int size = 20;
        private List<Models.Product> selected = new List<Models.Product>();
        private tb_Provider provider;

        public frmNhapHang(tb_Provider provider)
        {
            InitializeComponent();
            this.provider = provider;
            lbName.Text = this.provider.Name;
            lbAddress.Text = this.provider.Address;
            lbPhone.Text = this.provider.Phone;

            dgrList1.AutoGenerateColumns = false;
            dgrList2.AutoGenerateColumns = false;
            dgrList2.DataSource = selected;
            paging.pageChange += Paging_pageChange;
        }

        private void Paging_pageChange(object sender, EventArgs e)
        {
            int page = (int)sender;
            LoadData(page, size);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(page, size);
        }
        private void LoadData(int page, int size)
        {
            frmLoading.RunMethod<BaseReturnFunction<List<Models.Product>>>(typeof(ProductInfo), "SearchByProvider", (result, ctrl) =>
            {
                if (result.IsTrue)
                {
                    dgrList1.DataSource = result.Data;
                    dgrList2.DataSource = null;
                    dgrList2.DataSource = selected;
                    data = result.Data;
                    double totalPage = result.Total * 1.0 / size;
                    int _totalPage = (int)totalPage;
                    if (totalPage > _totalPage)
                        _totalPage++;
                    paging.setData(page, _totalPage, result.Total);
                }
                else
                {
                    data = new List<Models.Product>();
                    dgrList1.DataSource = data;
                    paging.setData(1, 1, 0);
                }

            }, this, provider.ID, txtKeyword.Text, page, size);
        }


        private void txtKeyword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadData(1, size);
            }
        }

        private void frmFormSell2_SizeChanged(object sender, EventArgs e)
        {
            panel1.Width = this.Width / 2;
        }

        private void frmFormSell2_Load(object sender, EventArgs e)
        {
            this.Top = 0; Left = 0;
            this.Location = Screen.PrimaryScreen.Bounds.Location;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.MaximumSize = this.Size;
            LoadData(page, size);
        }

        private void frmFormSell2_LocationChanged(object sender, EventArgs e)
        {
            this.Top = 0;
            this.Left = 0;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgrList1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
                if (e.ColumnIndex == 3)
                {
                    if (select(data[e.RowIndex]))
                    {
                        dgrList2.DataSource = null;
                        dgrList2.DataSource = selected.ToList();
                        //dgrList2.DataSource = selected;
                        //dgrList2.Update();
                        //dgrList2.Refresh();
                        dgrList1.DataSource = null;
                        dgrList1.DataSource = data;
                        sum();
                    }
                }
        }

        void sum()
        {
            double s = selected.Sum(x => x.Sum);
            txtPrice.Text = s.ToString("N0") + " d";
        }
        bool select(Models.Product product)
        {            
            var temp = selected.FirstOrDefault(x => x.ProductCode == product.ProductCode);
            if (temp != null)
            {
                temp.QuantityInStock++;
            }
            else
            {
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(product);

                Models.Product _product = serializer.Deserialize<Models.Product>(serializedResult);
                _product.QuantityInStock = 1;
                selected.Add(_product);
            }
            return true;
        }

        private void dgrList2_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var item = selected[e.RowIndex];
                switch (e.ColumnIndex)
                {
                    case 2:
                        if (reduce(item))
                        {
                            sum();
                            dgrList1.DataSource = null;
                            dgrList1.DataSource = data;
                            dgrList2.DataSource = null;
                            dgrList2.DataSource = selected.ToList();
                        }
                        break;
                    case 3:
                        frmNhapSoLuong frm = new frmNhapSoLuong(1,item.QuantityInStock,int.MaxValue);
                        if(frm.ShowDialog()==DialogResult.OK)
                        {
                            item.QuantityInStock = frm.Value;
                            sum();
                            dgrList1.DataSource = null;
                            dgrList1.DataSource = data;
                            dgrList2.DataSource = null;
                            dgrList2.DataSource = selected.ToList();
                        }
                        break;
                    case 4:
                        if (increate(item))
                        {
                            sum();
                            dgrList1.DataSource = null;
                            dgrList1.DataSource = data;
                            dgrList2.DataSource = null;
                            dgrList2.DataSource = selected.ToList();
                        }
                        break;
                    case 7:
                        if (remove(item))
                        {
                            sum();
                            dgrList1.DataSource = null;
                            dgrList1.DataSource = data;
                            dgrList2.DataSource = null;
                            dgrList2.DataSource = selected.ToList();
                        }
                        break;
                }

            }
        }

        private bool remove(Models.Product item)
        {
            selected.Remove(item);
            return true;
        }

        private bool increate(Models.Product item)
        {
            item.QuantityInStock++;
            var item2 = data.SingleOrDefault(x => x.ProductCode == item.ProductCode);
            if (item2 != null)
                item2.QuantityInStock++;
            
            return true;

        }

        private bool reduce(Models.Product item)
        {
            if (item.QuantityInStock > 0)
            {
                item.QuantityInStock--;
                var item2 = data.SingleOrDefault(x => x.ProductCode == item.ProductCode);
                if (item2 != null)
                {
                    item2.QuantityInStock--;
                    if (item2.QuantityInStock < 0)
                        item.QuantityInStock = 0;
                }
                return true;
            }
            return false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            if ((selected.Count == 0))
            {
                frmDialog.ShowCancelDialog("Chọn sản phẩm cần thanh toán");
                return;
            }
            frmPayment payment = new frmPayment(provider,selected);
            if (payment.ShowDialog() == DialogResult.OK)
            {
                frmDialog.ShowOkDialog("Thanh toán thành công", this);
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }

}
