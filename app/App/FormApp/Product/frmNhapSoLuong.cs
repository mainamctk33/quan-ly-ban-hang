﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Models;
using App.DataAccess;
using App.Database.Pay;
using App.Database.Customer;
using App.FormApp.Customer;
using App.FormApp.Provider;
using App.Utils;

namespace App.FormApp
{
    public partial class frmNhapSoLuong : DevComponents.DotNetBar.Office2007Form
    {
        private List<Models.Product> list;
        private Database.Provider.tb_Provider provider;

        public int Value
        {
            get
            {
                return txtValue.Value;
            }
        }
        public frmNhapSoLuong(int min, int value, int max) 
        {
            InitializeComponent();
            txtValue.MinValue = min;
            txtValue.MaxValue = max;
            txtValue.Value = value;
            txtValue.ValueObject = value;
            FormUtils.ConfigDialog(this);
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
