﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App.Models;
using App.DataAccess;
using App.Database.Pay;
using App.Database.Customer;
using App.FormApp.Customer;

namespace App.FormApp.Product
{
    public partial class frmPayment : DevComponents.DotNetBar.OfficeForm
    {
        private Database.Provider.tb_Provider provider;
        private List<Models.Product> list;
        private double sum;
        private double debt;
        private double _return;

        public frmPayment(Database.Provider.tb_Provider provider, List<Models.Product> list)
        {
            this.provider = provider;
            this.list = list;
            InitializeComponent();
            lbName.Text = this.provider.Name;
            lbAddress.Text = this.provider.Address;
            lbPhone.Text = this.provider.Phone;

            dgrList2.AutoGenerateColumns = false;
            dgrList2.DataSource = list;
            sum = list.Sum(x => x.Sum);
            lbTotal.Text = sum.ToString("N0") + "đ";
            debt = sum > dbMoney.Value ? sum - dbMoney.Value : 0;
            lbDebt.Text = debt.ToString("N0") + " đ";
        }

        private void dbMoney_ValueChanged(object sender, EventArgs e)
        {
            if (dbMoney.Value > sum)
                dbMoney.Value = sum;
            double debt = sum > dbMoney.Value ? sum - dbMoney.Value : 0;
            lbDebt.Text = debt.ToString("N0") + " đ";

        }

        private void btnPayment_Click(object sender, EventArgs e)
        {
            var money = dbMoney.Value > sum ? sum : dbMoney.Value;
            if (String.IsNullOrWhiteSpace(txtNote.Text))
            {
                frmDialog.ShowCancelDialog("Vui lòng nhập nội dung hóa đơn");
                return;
            }
            payment(provider, money, txtNote.Text);
        }

        private void payment(Database.Provider.tb_Provider provider, double money, String note)
        {
            using (var context = new PayDbDataContext())
            {
                var bill = new tb_Bill()
                {
                    CreatedBy = UserInfo.GetCurrentUser().UserName,
                    CreatedDate = DateTime.Now,
                    ProviderId = provider.ID,
                    Note = note,
                    Price = sum,
                    UpdatedDate = DateTime.Now,
                    Name = provider.Name,
                    Address = provider.Address,
                    Phone = provider.Phone
                };
                context.tb_Bills.InsertOnSubmit(bill);
                foreach (var item in list)
                {
                    var product = context.tb_Products.SingleOrDefault(x => x.ProductCode == item.ProductCode);
                    if (product != null)
                        product.QuantityInStock += item.QuantityInStock;

                    var billItem = new tb_BillItem()
                    {
                        tb_Bill = bill,
                        BillId = bill.ID,
                        ProductName = item.Name,
                        ProductId = item.ProductCode,
                        Quantily = item.QuantityInStock,
                        Sum = item.Sum,
                        Price = item.ActualPrice,
                    };
                    context.tb_BillItems.InsertOnSubmit(billItem);
                }
                var pay = new tb_Pay()
                {
                    BillId = bill.ID,
                    tb_Bill = bill,
                    CreatedBy = UserInfo.GetCurrentUser().UserName,
                    Note = note,
                    Value = money,
                    CreatedDate = DateTime.Now
                };
                context.tb_Pays.InsertOnSubmit(pay);
                context.SubmitChanges();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
