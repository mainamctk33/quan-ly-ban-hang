﻿namespace App
{
    partial class frmDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvMessage = new DevComponents.DotNetBar.LabelX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOk = new DevComponents.DotNetBar.ButtonX();
            this.lnCancel = new System.Windows.Forms.Panel();
            this.btnCancel = new DevComponents.DotNetBar.ButtonX();
            this.panel1.SuspendLayout();
            this.lnCancel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvMessage
            // 
            this.tvMessage.BackColor = System.Drawing.Color.Transparent;
            // 
            // 
            // 
            this.tvMessage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.tvMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.tvMessage.ForeColor = System.Drawing.Color.Black;
            this.tvMessage.Location = new System.Drawing.Point(0, 0);
            this.tvMessage.Name = "tvMessage";
            this.tvMessage.Size = new System.Drawing.Size(365, 67);
            this.tvMessage.TabIndex = 0;
            this.tvMessage.TextAlignment = System.Drawing.StringAlignment.Center;
            this.tvMessage.WordWrap = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.lnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, 67);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(365, 38);
            this.panel1.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnOk.Location = new System.Drawing.Point(184, 5);
            this.btnOk.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(85, 28);
            this.btnOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Chấp nhận";
            // 
            // lnCancel
            // 
            this.lnCancel.Controls.Add(this.btnCancel);
            this.lnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.lnCancel.ForeColor = System.Drawing.Color.Black;
            this.lnCancel.Location = new System.Drawing.Point(269, 5);
            this.lnCancel.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
            this.lnCancel.Name = "lnCancel";
            this.lnCancel.Size = new System.Drawing.Size(91, 28);
            this.lnCancel.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancel.Location = new System.Drawing.Point(6, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(15, 3, 3, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 28);
            this.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Hủy bỏ";
            // 
            // frmDialog
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(365, 101);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tvMessage);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.TopMost = true;
            this.panel1.ResumeLayout(false);
            this.lnCancel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX tvMessage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel lnCancel;
        private DevComponents.DotNetBar.ButtonX btnCancel;
        private DevComponents.DotNetBar.ButtonX btnOk;
    }
}