﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Dynamic;

namespace App.Utils
{
    public class ConvertUtils
    {
        public static DateTime TimeMilisecondToDateTime(double timeMilisecond)
        {
            try
            {
                DateTime dtDateTimes = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                return dtDateTimes.AddMilliseconds(timeMilisecond).ToLocalTime();
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
        }
        public static DateTime TimeSecondToDateTime(double timeMilisecond)
        {
            try
            {
                return TimeMilisecondToDateTime(timeMilisecond * 1000);
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
        }

        public static T toObject<T>(dynamic content)
        {
            if (content == null)
                return JsonConvert.DeserializeObject<T>("");
            return JsonConvert.DeserializeObject<T>(content);
        }

        public static int MilisecondToAge(double second)
        {
            return DateTimeToAge(ConvertUtils.TimeMilisecondToDateTime(second));
        }

        public static int DateTimeToAge(DateTime dateTime)
        {
            int age = DateTime.Now.Year - dateTime.Year;
            if (age <= 0) return 1;
            return age + 1;
        }

        public static double DateTimeToTimeMilisecond(DateTime dateTime)
        {
            return Math.Round(dateTime.ToUniversalTime().Subtract(
                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                ).TotalMilliseconds, 0);
        }
        public static double DateTimeToTimeSecond(DateTime dateTime)
        {
            return Math.Round(dateTime.ToUniversalTime().Subtract(
                new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                ).TotalSeconds);
        }



        public static int ToInt(object obj, int defaultValue)
        {
            try
            {
                return Convert.ToInt32(obj.ToString());
            }
            catch (Exception e)
            {
                return defaultValue;
            }
        }

        public static int ToInt(object obj)
        {
            return ToInt(obj, 0);
        }
        public static double ToDouble(object obj)
        {
            try
            {
                return Convert.ToDouble(obj.ToString());
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        internal static decimal ToDecimal(object value)
        {
            return ToDecimal(value, 0);
        }
        internal static decimal ToDecimal(object value, decimal defaultValue)
        {
            try
            {
                return Convert.ToDecimal(value);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        public static String Serialize(Object _object)
        {
            return JsonConvert.SerializeObject(_object);
        }
        
        public static dynamic ToDynamic(String anonymousObject)
        {
            return JsonConvert.DeserializeObject<dynamic>(anonymousObject);
        }

        public static string ToString(dynamic dataJsonParams, string v)
        {
            try
            {
                return dataJsonParams[v].ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static DateTime ToDate(string v)
        {
            return Convert.ToDateTime(v);
        }
        public static DateTime ToDate(string date, String format)
        {
            return DateTime.ParseExact(date, format, System.Globalization.CultureInfo.InvariantCulture);
        }

        public static T ToEnum<T>(object val) where T : struct
        {
            if (val is Int32)
                return (T)val;
            if (val is String)
                return (T)Enum.Parse(typeof(T), val.ToString());
            throw new Exception("value can't parse to enum");
        }

        public static List<T> ToList<T>(T t) where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }

    }
}