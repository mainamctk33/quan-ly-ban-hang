﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Utils
{
    class RegistryUtils
    {
        public static RegistryKey OpenKey()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);
            var temp = key.OpenSubKey("QLBH", true);
            if (temp != null)
                return temp;
            return key.CreateSubKey("QLBH", true);
        }
        public static RegistryKey OpenKey(String keyString)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);
            var temp = key.OpenSubKey(keyString, true);
            if (temp != null)
                return temp;
            return key.CreateSubKey(keyString, true);
        }
        public static RegistryKey OpenKey(RegistryKey registry, String keyString)
        {
            var temp = registry.OpenSubKey(keyString, true);
            if (temp != null)
                return temp;
            return registry.CreateSubKey(keyString, true);
        }
        public static void SetValue(String key, object value)
        {
            var temp = OpenKey();
            temp.SetValue(key, value);
        }
        public static int GetValue(String key, int defaultValue)
        {
            try
            {
                var temp = OpenKey();
                if (temp != null)
                {
                    var value = temp.GetValue(key);
                    return ConvertUtils.ToInt(value, defaultValue);
                }
            }
            catch (Exception)
            {

            }
            return defaultValue;
        }
        public static int GetValue(String key)
        {
            return GetValue(key, 0);
        }

        public static string GetStringValue(String key)
        {
            try
            {
                var temp = OpenKey();
                if (temp != null)
                {
                    var value = temp.GetValue(key);
                    return value.ToString();
                }
            }
            catch (Exception)
            {

            }
            return null;
        }

    }
}
