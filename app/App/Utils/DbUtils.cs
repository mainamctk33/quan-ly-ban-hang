﻿using App.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Utils
{
    public class DbUtils
    {
        public static String GetConnectString()
        {
            String server = RegistryUtils.GetStringValue(Constants.SERVER);
            String database = RegistryUtils.GetStringValue(Constants.DATABASE);
            String password = RegistryUtils.GetStringValue(Constants.PASSWORD);
            String username = RegistryUtils.GetStringValue(Constants.USERNAME);
            if(String.IsNullOrWhiteSpace(password))
            {
                return String.Format("Data Source={0};Initial Catalog={1};Integrated Security=True;", server, database, username, password);
            }
            return String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}", server, database, username, password);
        }
    }
}
